create table app_user
(
  id bigserial not null
    constraint app_app_user_pkey
    primary key,
  first_name varchar(255),
  surname varchar(255),
  login_name varchar(255)
    constraint ukb2p5uxytyfufef7vm8snay5ee
    unique,
  email varchar(100) not null
    constraint uk1j9d9a06i600gd43uu3km82jw
    unique,
  status_id integer not null,
  password varchar(255)
)
;

create unique index app_app_user_login_uindex
  on app_user (login_name)
;

create unique index app_app_user_email_uindex
  on app_user (email)
;

create table gallery
(
  id bigserial not null
    constraint table_name_pkey
    primary key,
  description text,
  sitename_id integer,
  affiliate_program_id integer not null,
  title text,
  protocol_id integer,
  domain_id integer not null,
  uri text not null,
  date timestamp
)
;

create unique index gallery_uri_uindex
  on gallery (uri)
;

create table login_info
(
  id bigserial not null
    constraint login_info_pkey
    primary key,
  last_date timestamp,
  login varchar(100) not null,
  password varchar(255) not null,
  start_date timestamp,
  affiliate_program_id integer not null,
  app_user_id integer not null
    constraint login_info_app_app_user_id_fk
    references app_user
    on delete cascade,
  external_affiliate_login_id varchar(50),
  password2 varchar(255),
  constraint uk9555pqs4xxnqgfsnebu08uexf
  unique (app_user_id, affiliate_program_id, login)
)
;

create unique index login_info_id_uindex
  on login_info (id)
;

create unique index login_info_app_user_id_affiliate_program_id_login_uindex
  on login_info (app_user_id, affiliate_program_id, login)
;

create index login_info_affiliate_program_id_index
  on login_info (affiliate_program_id)
;

create table payment
(
  id bigserial not null
    constraint payments_pkey
    primary key,
  amount integer,
  method varchar(50),
  payment_sent timestamp,
  status varchar(50),
  affiliate_program_id integer,
  logininfo_id integer
    constraint payments_login_info_id_fk
    references login_info
    on delete cascade,
  app_user_id integer not null
    constraint payments_app_app_user_id_fk
    references app_user,
  constraint uk3kjvtt37vahwllxpxvsqmero4
  unique (app_user_id, affiliate_program_id, logininfo_id)
)
;

create unique index payments_app_user_id_affiliate_program_id_logininfo_id_uindex
  on payment (app_user_id, affiliate_program_id, logininfo_id)
;

create table protocol
(
  id bigserial not null
    constraint protocol_pkey
    primary key,
  protocol_name varchar(10)
    constraint ukkq6djdbk78pb0rx13e1dqa02s
    unique
)
;

create unique index protocol_protocol_name_uindex
  on protocol (protocol_name)
;

create table sale_program
(
  id bigserial not null
    constraint sale_programs_pkey
    primary key,
  external_id varchar(20),
  program_name varchar(50),
  affiliate_program_id integer not null
)
;

create table site_for_galleries
(
  id bigserial not null
    constraint site_for_galleries_pkey
    primary key,
  external_id varchar(20),
  site_name varchar(50),
  affiliate_program_id integer not null,
  protocol_id integer
    constraint site_for_galleries_protocol_id_fk
    references protocol,
  domain_id integer,
  uri varchar(255),
  constraint uk9l02d1iqjutuhsmlp5ff125pj
  unique (domain_id, affiliate_program_id)
)
;

create unique index site_for_galleries_domain_id_affiliate_program_id_uindex
  on site_for_galleries (domain_id, affiliate_program_id)
;

create table site_from_aff_program
(
  id bigserial not null
    constraint sites_from_aff_programs_pkey
    primary key,
  external_site_id varchar(20),
  site_name varchar(100) not null,
  affiliate_program_id integer not null,
  domain_id integer,
  uri_part_before_param varchar(100),
  uri_part_after_param varchar(100),
  constraint uklsjuut2r6c87f3kakjelh3cka
  unique (affiliate_program_id, external_site_id)
)
;

create unique index sites_from_aff_programs_app_user_id_affiliate_program_id_login_
  on site_from_aff_program (affiliate_program_id, external_site_id)
;

create index site_from_aff_program_affiliate_program_id_index
  on site_from_aff_program (affiliate_program_id)
;

create index idx_site_from_aff_program
  on site_from_aff_program (domain_id)
;

create table app_user_profile
(
  id bigserial not null
    constraint app_user_profile_pkey
    primary key,
  type varchar(15) not null
    constraint ukbj8mv97pise3su1isk73xftev
    unique
)
;

create unique index app_user_profile_type_uindex
  on app_user_profile (type)
;

create table domain
(
  id bigserial not null
    constraint domains_pkey
    primary key,
  domain_name varchar(50) not null
    constraint uk3cnnj1fsouocujy0djd7p1sn6
    unique
)
;

create unique index domains_id_uindex
  on domain (id)
;

create unique index domains_domain_name_uindex
  on domain (domain_name)
;

alter table site_for_galleries
  add constraint site_for_galleries_domains_id_fk
foreign key (domain_id) references domain
;

alter table site_from_aff_program
  add constraint fk_site_from_aff_program
foreign key (domain_id) references domain
on update restrict on delete restrict
;

create table autorization_history
(
  id bigserial not null
    constraint autorization_history_pkey
    primary key,
  app_user_id integer not null
    constraint autorization_history_app_app_user_id_fk
    references app_user
    on delete cascade,
  date_login timestamp not null,
  ip_address varchar(15),
  browser_agent varchar(255)
)
;

create unique index autorization_history_id_uindex
  on autorization_history (id)
;

create table status
(
  id bigserial not null
    constraint status_pkey
    primary key,
  status varchar(50) not null
    constraint ukkxaj0dvn13fwjuimg3y2j0oa2
    unique
)
;

create unique index status_id_uindex
  on status (id)
;

create unique index status_status_uindex
  on status (status)
;

alter table app_user
  add constraint app_app_user_status_id_fk
foreign key (status_id) references status
;

create table general_stat
(
  stats_date timestamp not null,
  raw_click bigint,
  rebill_count bigint,
  rebill_money bigint,
  refund_chargeback_count bigint,
  refund_chargeback_money bigint,
  signup_count bigint,
  signup_money bigint,
  total_money bigint,
  unique_clicks bigint,
  app_user_id bigint not null
    constraint general_stats_app_app_user_id_fk
    references app_user
    on delete cascade,
  site_from_aps_id bigint not null
    constraint general_stats_site_from_aff_program_id_fk
    references site_from_aff_program,
  logininfo_id bigint not null
    constraint general_stats_login_info_id_fk
    references login_info
    on delete cascade,
  affiliate_program_id bigint not null,
  id bigserial not null
    constraint general_stat_id_pk
    primary key,
  constraint ukqwoo1x8u8y5n6btv0m6pypp88
  unique (stats_date, app_user_id, site_from_aps_id, logininfo_id, affiliate_program_id)
)
;

create unique index general_stat_stats_date_app_user_id_site_from_aps_id_logininfo_
  on general_stat (stats_date, app_user_id, site_from_aps_id, logininfo_id, affiliate_program_id)
;

create unique index general_stat_id_uindex
  on general_stat (id)
;

create index general_stat_app_user_id_index
  on general_stat (app_user_id)
;

create index general_stat_site_from_aps_id_index
  on general_stat (site_from_aps_id)
;

create index general_stat_logininfo_id_index
  on general_stat (logininfo_id)
;

create index general_stat_affiliate_program_id_index
  on general_stat (affiliate_program_id)
;

create table persistent_logins
(
  series varchar(64) not null
    constraint persistent_logins_series_pk
    primary key,
  last_used timestamp,
  token varchar(64) not null,
  username varchar(64) not null
)
;

create table affiliate_program
(
  id bigserial not null
    constraint affiliate_program_pkey
    primary key,
  type varchar(50) not null,
  name varchar(100) not null,
  uri varchar(255),
  protocol_id integer not null
    constraint affiliate_programs_protocol_id_fk
    references protocol,
  domain_id integer
    constraint fk_affiliate_program
    references domain
    on update restrict on delete restrict,
  default_sale_program_id bigint,
  css_selector_id bigint,
  javascript_enable boolean
)
;

create unique index affiliate_program_id_uindex
  on affiliate_program (id)
;

create index idx_affiliate_program
  on affiliate_program (domain_id)
;

create index idx_affiliate_program_0
  on affiliate_program (default_sale_program_id)
;

alter table login_info
  add constraint login_info_affiliate_program_id_fk
foreign key (affiliate_program_id) references affiliate_program
;

alter table payment
  add constraint payment_affiliate_program_id_fk
foreign key (affiliate_program_id) references affiliate_program
;

alter table sale_program
  add constraint sale_program_affiliate_program_id_fk
foreign key (affiliate_program_id) references affiliate_program
;

alter table site_for_galleries
  add constraint site_for_galleries_affiliate_program_id_fk
foreign key (affiliate_program_id) references affiliate_program
;

alter table site_from_aff_program
  add constraint site_from_aff_program_affiliate_program_id_fk
foreign key (affiliate_program_id) references affiliate_program
;

alter table general_stat
  add constraint general_stat_affiliate_program_id_fk
foreign key (affiliate_program_id) references affiliate_program
;

create table lastdate_download_stat
(
  id bigserial not null
    constraint pk_lastdate_download_stats
    primary key,
  site_id integer not null
    constraint fk_lastdate_download_stats
    references site_from_aff_program
    on delete cascade,
  login_id integer not null
    constraint fk_lastdate_download_stats_0
    references login_info
    on delete cascade,
  last_date timestamp not null,
  status_id integer
    constraint lastdate_download_stat_status_id_fk
    references status
    on delete cascade,
  appuser_id integer not null
    constraint lastdate_download_stat_app_user_id_fk
    references app_user
    on delete cascade,
  constraint idx_lastdate_download_stats_1
  unique (site_id, login_id)
)
;

create unique index lastdate_download_stat_site_id_login_id_uindex
  on lastdate_download_stat (site_id, login_id)
;

create index idx_lastdate_download_stats
  on lastdate_download_stat (site_id)
;

create index idx_lastdate_download_stats_0
  on lastdate_download_stat (login_id)
;

create table account_balance
(
  id serial not null
    constraint pk_account_balans
    primary key,
  amount bigint,
  date_amount timestamp,
  login_id bigint not null
    constraint account_balance_login_info_id_fk
    references login_info
    on delete cascade
)
;

create index account_balance_date_amount_index
  on account_balance (date_amount)
;

create table tag
(
  tagname varchar(30) not null
    constraint ukpqr1ll1n2j2ganqvhivmiodss
    unique,
  id serial not null
    constraint tag_id_pk
    primary key
)
;

create unique index tag_tagname_uindex
  on tag (tagname)
;

create unique index tag_id_uindex
  on tag (id)
;

create table porn_model
(
  id bigserial not null
    constraint porn_model_pkey
    primary key,
  name varchar(50) not null,
  description text,
  affiliate_program_id integer
    constraint porn_model_affiliate_program_id_fk
    references affiliate_program,
  uri_to_model_page text,
  birthplace varchar(50),
  protocol_id integer
    constraint porn_model_protocol_id_fk
    references protocol,
  domain_id bigint
    constraint porn_model_domain_id_fk
    references domain,
  link_to_portfolio_image text,
  constraint uka3305eiuwq4cvb6e07ffwowdi
  unique (name, affiliate_program_id)
)
;

create unique index porn_model_id_uindex
  on porn_model (id)
;

create unique index porn_model_name_affiliate_program_id_uindex
  on porn_model (name, affiliate_program_id)
;

create table link_to_content
(
  id bigserial not null
    constraint image_pkey
    primary key,
  image_link_uri text not null,
  protocol_id integer not null
    constraint image_protocol_id_fk
    references protocol,
  domain_id integer not null
    constraint image_domain_id_fk
    references domain,
  content_type_id integer not null,
  constraint uke0noi281y6cistxpbneq7jwf3
  unique (protocol_id, domain_id, image_link_uri)
)
;

create unique index image_id_uindex
  on link_to_content (id)
;

create unique index image_protocol_id_domain_id_image_link_uri_uindex
  on link_to_content (protocol_id, domain_id, image_link_uri)
;

create table porn_model_gallery
(
  porn_model_id integer not null
    constraint porn_model_gallery_porn_model_id_fk
    references porn_model,
  gallery_id integer not null
    constraint porn_model_gallery_gallery_id_fk
    references gallery,
  constraint porn_model_gallery_porn_model_id_gallery_id_pk
  primary key (porn_model_id, gallery_id)
)
;

create table gallery_tag
(
  gallery_id integer not null
    constraint galerry_tag_gallery_id_fk
    references gallery,
  tag_id integer not null
    constraint galerry_tag_tag_id_fk
    references tag,
  constraint gallery_tag_gallery_id_tag_id_pk
  primary key (gallery_id, tag_id)
)
;

create table content_type
(
  id bigserial not null
    constraint content_type_pkey
    primary key,
  content_type varchar(30)
    constraint ukl38bowuljpndhwnnc3f6ixjep
    unique
)
;

create unique index content_type_id_uindex
  on content_type (id)
;

create unique index content_type_content_type_uindex
  on content_type (content_type)
;

alter table link_to_content
  add constraint link_to_content_content_type_id_fk
foreign key (content_type_id) references content_type
;

create table gallery_link_to_content
(
  gallery_id bigserial not null,
  link_to_content_id bigserial not null
    constraint gallery_link_to_content_link_to_content_id_fk
    references link_to_content,
  constraint gallery_link_to_content_gallery_id_link_to_content_id_pk
  primary key (gallery_id, link_to_content_id)
)
;

create index gallery_link_to_content_gallery_id_index
  on gallery_link_to_content (gallery_id)
;

create index gallery_link_to_content_link_to_content_id_index
  on gallery_link_to_content (link_to_content_id)
;

create table service_collection_runs_threads
(
  id bigserial not null
    constraint service_collection_runs_threads_pkey
    primary key,
  app_user_id bigint
    constraint service_collection_runs_threads_app_app_user_id_fk
    references app_user,
  login_id bigint
    constraint service_collection_runs_threads_login_info_id_fk
    references login_info
    on delete cascade,
  affiliate_program_id bigint
    constraint service_collection_runs_threads_affiliate_program_id_fk
    references affiliate_program,
  site_from_ap_id bigint
    constraint service_collection_runs_threads_site_from_aff_program_id_fk
    references site_from_aff_program
)
;

create unique index service_collection_runs_threads_id_uindex
  on service_collection_runs_threads (id)
;

create table app_user_app_user_profile
(
  app_user_id bigint not null
    constraint app_user_app_user_profile_app_user_id_fk
    references app_user
    on delete cascade,
  app_user_profile_id bigint not null
    constraint app_user_app_user_profile_app_user_profile_id_fk
    references app_user_profile,
  constraint app_user_app_user_profile_app_user_id_app_user_profile_id_pk
  primary key (app_user_id, app_user_profile_id)
)
;

create table css_selector
(
  id bigserial not null
    constraint css_selecror_pkey
    primary key,
  waiting_this_template varchar(255) not null,
  form_tag varchar(255) not null,
  user_field varchar(255) not null,
  password_field varchar(255) not null,
  submit_element varchar(255) not null,
  constraint ukj5dfefjgfl316a2530vpri75
  unique (waiting_this_template, form_tag, user_field, password_field, submit_element)
)
;

create unique index css_selecror_id_uindex
  on css_selector (id)
;

create unique index css_selecror_waiting_this_template_form_tag_user_field_password
  on css_selector (waiting_this_template, form_tag, user_field, password_field, submit_element)
;

alter table affiliate_program
  add constraint affiliate_program_css_selector_id_fk
foreign key (css_selector_id) references css_selector
;

