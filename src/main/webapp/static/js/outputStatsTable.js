"use strict";

function createTableBody(jsonData) {
    var tbody = document.getElementById('mainTBody');

    var newTr;
    var empryTr;
    var apList = jsonData;
    var oldLogin = "asd";
    var oldAp = "asd";

    for (var key in apList) {
        if (apList.hasOwnProperty(key)) {
            newTr = document.createElement('tr');
            newTr.classList.add('line_tr');
            if ((oldLogin.localeCompare(apList[key][0]) !== 0) || (oldAp.localeCompare(apList[key][1]) !== 0)) {
                tbody.appendChild(document.createElement('tr'));
                empryTr = document.createElement('tr');
                empryTr.appendChild(createTdElement(apList[key][1]));
                empryTr.appendChild(createTdElement(apList[key][0]));

                empryTr.appendChild(createTdElement(""));
                empryTr.appendChild(createTdElement(""));
                empryTr.appendChild(createTdElement(""));
                empryTr.appendChild(createTdElement(""));
                empryTr.appendChild(createTdElement(""));
                empryTr.appendChild(createTdElement(""));
                empryTr.appendChild(createTdElement(""));
                empryTr.appendChild(createTdElement(""));
                empryTr.appendChild(createTdElement(""));
                empryTr.appendChild(createTdElement(""));
                empryTr.appendChild(createTdElement(""));
                empryTr.classList.add("info");
                tbody.appendChild(empryTr);
            }
            newTr.appendChild(createTdElement(""));
            newTr.appendChild(createTdElement(""));
            newTr.appendChild(createTdElement(apList[key][2]));
            // newTr.appendChild(createTdElement(apList[key][12] + "<br>" + apList[key][13])); //status
            newTr.appendChild(createTdElement(apList[key][12]));
            newTr.appendChild(createTdElement(apList[key][3]));
            newTr.appendChild(createTdElement(apList[key][4]));
            newTr.appendChild(createTdElement(apList[key][5]));
            newTr.appendChild(createTdElement(parseInt((apList[key][6]), 10) / 100));
            newTr.appendChild(createTdElement(apList[key][7]));
            newTr.appendChild(createTdElement(parseInt((apList[key][8]), 10) / 100));
            newTr.appendChild(createTdElement(apList[key][9]));
            newTr.appendChild(createTdElement(parseInt((apList[key][10]), 10) / 100));
            newTr.appendChild(createTdElement(parseInt((apList[key][11]), 10) / 100));
            tbody.appendChild(newTr);

            oldLogin = apList[key][0];
            oldAp = apList[key][1];
        }

        if (parseInt(apList[key][3], 10) === 0 &&
            parseInt(apList[key][4], 10) === 0 &&
            parseInt(apList[key][5], 10) === 0 &&
            parseInt(((apList[key][6]), 10) / 100, 10) === 0 &&
            parseInt(apList[key][7], 10) === 0 &&
            parseInt(((apList[key][8]), 10) / 100, 10) === 0 &&
            parseInt(apList[key][9], 10) === 0 &&
            parseInt(((apList[key][10]), 10) / 100, 10) === 0 &&
            parseInt(((apList[key][11]), 10) / 100, 10) === 0) {
            newTr.classList.add("zero");
            // newTr.classList.add("noMoney");
        }
        if (parseInt((apList[key][6]), 10) === 0 &&
            parseInt((apList[key][8]), 10) === 0 &&
            parseInt((apList[key][10]), 10) === 0 &&
            parseInt((apList[key][11]), 10) === 0) {
            newTr.classList.add("no_money");
        }
    }

    function createTdElement(param) {
        var td = document.createElement('td');
        td.classList.add('green_buts');
        td.innerHTML = param;
        return td;
    }

    function offZeroString() {
        var rows = document.getElementsByClassName('zero');
        for (var i = 0; i < rows.length; i++) {
            if (this.checked)
                rows[i].style.display = 'none';
            else
                rows[i].style.display = 'table-row'
        }
    }

    function offNoMoneyAndZeroString() {
        var rows = document.getElementsByClassName('no_money');
        for (var i = 0; i < rows.length; i++) {
            if (this.checked)
                rows[i].style.display = 'none';
            else
                rows[i].style.display = 'table-row'
        }
        offZeroString();
    }

    // function showMoneyString() {
    //     var rows = document.getElementsByClassName('noMoney');
    //     for (var i = 0; i < rows.length; i++) {
    //         if (this.checked) {
    //             rows[i].style.display = 'none';
    //         } else {
    //             rows[i].style.display = 'table-row';
    //         }
    //     }
    // }
    //
    // function showMoney() {
    //     var rows = document.querySelectorAll('#mainTBody > tr');
    //     for (var i = 0; i < rows.length; i++) {
    //         if (this.checked) {
    //             if ( $(rows[i]).hasClass("money")) {
    //             } else {
    //                 rows[i].style.display = 'none';
    //             }
    //         } else {
    //             rows[i].style.display = 'table-row';
    //         }
    //     }
    // }

    document.getElementById('chkZero').onchange = offZeroString;
    // document.getElementById('showMoneyString').onchange = showMoneyString;
    // document.getElementById('showMoneyString').onchange = showMoney;
    document.getElementById('showMoneyString').onchange = offNoMoneyAndZeroString;
}