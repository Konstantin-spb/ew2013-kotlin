"use strict";

function formatDate(date) {
    var dd = date.getDate();
    if (dd < 10) dd = '0' + dd;
    var mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;
    var yyyy = date.getFullYear();
    return yyyy + '-' + mm + '-' + dd;
}

function firstWeekDate( currentDate ) {
    if (currentDate.getDay() !== 0) {
        currentDate.setDate(currentDate.getDate() - (currentDate.getDay() - 1));
    } else {
        currentDate.setDate(currentDate.getDate() - 6);
    }
    return formatDate(currentDate);
}

document.querySelector('input[name=startDate]').value = firstWeekDate(new Date());
document.querySelector('input[name=endDate]').value = formatDate(new Date());