<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core_1_1" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
Created by IntelliJ IDEA.
User: konstantin
Date: 04.08.16
Time: 11:33
To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <style type="text/css">
        body {
            /*background-image: url('/static/images/bg1.jpg');*/
            background: url('<c:url value='/static/images/bg2.jpg' />') top right no-repeat;
            background-attachment: fixed;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-xl-12" style="background-color: bisque">
            <p>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam
                felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede
                justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.
                Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu,
                consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.
                Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi
                vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus
                eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam
                nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.
                Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros
                faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <sec:authorize access="hasRole('ADMIN')">
                <div>
                    <form action="<c:url value='/admin/panel'/>" method="get">
                        <button type="submit">Admin panel</button>
                    </form>
                </div>
                <div>
                <span>
                    <label>
                     Add affiliate programs from file
                    </label>
                    <form method="POST" action="<c:url value='/users/${myUser.loginName}/add-ap-from-file'/>"
                    <%--<form method="POST" action="<c:url value='/users/${apLoginBalance.loginName}/add-ap-from-file'/>"--%>
                          enctype="multipart/form-data">
                        <input type="file" name="file"/><br/>
                        <input type="submit" value="Submit"/>
                    </form>
                </span>
                </div>
            </sec:authorize>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12" style="background-color: gray">
            <table class="table">
                <tr class="line_tr">
                    <td class="button">
                        <div>
                            <form action="<c:url value='/users/${myUser.loginName}/add-new-affiliate-program-into-your-list'/>"
                                  method="get">
                                <button type="submit" class="btn btn-success">Add affiliate program</button>
                            </form>
                        </div>
                    </td>
                    <td>

                    </td>
                    <td>
                        <form action="<c:url value='/users/${myUser.loginName}/download-all-stats'/>" method="get">
                            <button type="submit" class="btn btn-success">Download all stats</button>
                        </form>
                    </td>
                    <td>
                        <form action="<c:url value='/users/${myUser.loginName}/show-all-stats'/>" method="POST">
                            <p>Выберите дату:
                                <input type="date" name="startDate" value="2016-01-01" min="2000-01-01">
                                <input type="date" name="endDate" value="" min="2000-01-01">
                                <button type="submit" class="btn btn-secondary">Show stats by period</button>
                        </form>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-3" style="background-color: darksalmon">
            <h1>Hello</h1>
            <span>Dear <strong>${myUser.loginName}</strong>, welcome.</span> <span><a
                href="<c:url value="/logout" />">Logout</a></span>
            <ul>
                <li><a href="<c:url value='/edit-user-${myUser.loginName}'/>">Edit profile</a></li>
                <li>2</li>
                <li>3</li>
                <li>4</li>
                <li>5</li>
                <li>6</li>
                <li>7</li>
                <li>8</li>
                <li>9</li>
                <li>10</li>
            </ul>
        </div>
        <div class="col-xl-9" style="background-color: bisque">
            <table class="table table-hover table-sm">
                <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>AP name</th>
                    <%--<th>Type</th>--%>
                    <th>Login</th>
                    <th>Balance</th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <%--Это конструкция if...else--%>
                    <c:when test="${apLoginBalance.size() > 0}">
                        <c:forEach items="${apLoginBalance}" var="log">
                            <tr class="line_tr">
                                <td>
                                    <form action="<c:url value='/users/${myUser.loginName}/${log.apName}/${log.loginName}/edit'/>"
                                          method="get">
                                        <button type="submit">Edit</button>
                                    </form>
                                </td>
                                <td>
                                        <%--<form action="<c:url value='/users/${myUser.loginName}/${log.affiliateProgram.name}/${log.login}/delete'/>"--%>
                                    <form action="<c:url value='/users/${myUser.loginName}/${log.apName}/${log.loginName}/delete/${log.loginId}' />"
                                          method="get">
                                        <button type="submit">Delete</button>
                                    </form>
                                </td>
                                <td>
                                    <form action="<c:url value='/users/${myUser.loginName}/${log.apName}/${log.loginName}/${log.loginId}/download'/>"
                                          method="get">
                                        <button type="submit">Download</button>
                                    </form>
                                </td>
                                <td>
                                    <a href="${log.protocol}://${log.domain}"
                                       target="_blank">${log.apName}</a></td>
                                <%--<td>${log.affiliateProgram.type}</td>--%>
                                <td>${log.loginName}</td>
                                <td>
                                        <c:if test="${log.amount != null}">
                                        ${log.amount / 100}
                                        </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        Нет программ
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>
        </div>

    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<c:url value='/static/js/firstDateInWeek.js' />"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
        integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
        crossorigin="anonymous"></script>
</body>
</html>
