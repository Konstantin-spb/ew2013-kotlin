<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%--<link href="<c:url value='/static/css/main.css' />" rel="stylesheet">--%>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>

<%--<%@include file="../authheader.jsp" %>--%>

<%--<form action="<c:url value='/user/${myUser.loginName}/show-all-stats'/>" method="POST">--%>
<%--<p>Выберите дату 1:--%>
<%--<input type="date" name="startDate" value="2016-01-01" min="2000-01-01">--%>
<%--<input type="date" name="endDate" value="" min="2000-01-01">--%>
<%--<button type="submit">Show stats by period</button>--%>
<%--</form>--%>

<div class="container">
    <div class="row">
        <div class="col-xl-12" style="background-color: bisque">
            <p>
                Основные типы В Kotlin всё является объектом, в том смысле, что пользователь может вызвать функцию или
                получить доступ к свойству любой переменной. Некоторые типы являются встроенными, т.к. их реализация
                оптимизирована, хотя для пользователя они выглядеть как обычные классы. В данном разделе описывается
                большинство этих типов: числа, символы, логические переменные и массивы.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12" style="background-color: gray">
            <table class="table">
                <tr class="line_tr">
                    <td>
                        <form action="<c:url value='/users/${myUser.loginName}/show-all-stats'/>" method="POST">
                            <p>Выберите дату:
                                <input type="date" name="startDate" value="2016-01-01" min="2000-01-01">
                                <input type="date" name="endDate" value="" min="2000-01-01">
                                <button type="submit">Show stats by period</button>
                        </form>
                    </td>
                    <td>
                        <div class="clock"></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12" style="background-color: darksalmon">
            <input type="checkbox" id="chkZero"/>
            <label for="chkZero">Hide all zero rows</label>

            <input type="checkbox" id="showMoneyString"/>
            <label for="showMoneyString">Show money string</label>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12" style="background-color: bisque">
            <table id="mainTableWithStats" class="table-sm table-hover table-striped">
                <thead>
                <tr class="line_tr">
                    <th>AP name</th>
                    <th>loginName</th>
                    <th>Sitename</th>
                    <th>Status</th>
                    <th>raw</th>
                    <th>uniq</th>
                    <th>signC</th>
                    <th>signM</th>
                    <th>rebillC</th>
                    <th>rebillM</th>
                    <th>refundC</th>
                    <th>refundM</th>
                    <th>totalM</th>
                </tr>
                </thead>
                <tbody id="mainTBody"></tbody>
            </table>
        </div>
    </div>
</div>

<script>
    var st = serverTime();
    var today = new Date(st);
    var date = today.getFullYear()+'/'+(today.getMonth()+1)+'/'+today.getDate();
//    alert(date);
</script>
<script src="<c:url value='/static/js/firstDateInWeek.js' />"></script>
<script src="<c:url value='/static/js/outputStatsTable.js' />"></script>
<script src="<c:url value='/static/js/clock.js' />"></script>
<script>createTableBody(${apJsonFromJava});</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
        integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
        crossorigin="anonymous"></script>

</body>
</html>


