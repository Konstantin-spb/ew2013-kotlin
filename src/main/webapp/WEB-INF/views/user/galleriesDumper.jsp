<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: konstantin
  Date: 30.05.17
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:forEach items="${galleriesList}" var="gallery">
    ${gallery.affiliateProgram.name}
    |
    ${gallery.domain.domainName}
    |
    ${gallery.protocol.protocolName}://${gallery.domain.domainName}${gallery.uri}?id=ladytop
    |
    ${gallery.title}
    |
    ${gallery.description}
    |
    <c:forEach items="${gallery.linkToContents}" var="image">
        <c:if test="${image.contentType.id == 1}">
            ${image.protocol.protocolName}://${image.domain.domainName}${image.imageLinkUri},
        </c:if>
    </c:forEach>
     |
    <c:forEach items="${gallery.linkToContents}" var="image">
        <c:if test="${image.contentType.id == 2}">
            ${image.protocol.protocolName}://${image.domain.domainName}${image.imageLinkUri}
        </c:if>
    </c:forEach>
    |
    <c:forEach items="${gallery.tags}" var="tag">
        ${tag.tagname},
    </c:forEach>
    |
    <c:forEach items="${gallery.pornModels}" var="model">
        ${model.name},
    </c:forEach>
    |
    <br>
</c:forEach>
</body>
</html>
