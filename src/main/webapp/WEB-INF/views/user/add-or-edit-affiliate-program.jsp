<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>

<head>
    <title>User Registration Form</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xl-9" style="background-color: bisque">

            <div class="well lead" align="center">
                User Registration Form<br>
                <a class="text-danger">
                    <h6>Здесь создаём нового пользователя и назначаем ему права</h6>
                </a>
            </div>
            <form:form method="POST" modelAttribute="affiliateProgramConstantInfo" class="form-horizontal">
                <%--<form:input type="hidden" path="id" id="id"/>--%>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-lable">Affiliate prorgram name</label>
                        <div class="col-md-7">
                            <c:choose>
                                <c:when test="${affiliateProgramConstantInfo.affiliateProgramName != null}">
                                    <%--<form:hidden path="affiliateProgramId" />--%>
                                    <form:input path="affiliateProgramName" class="form-control input-sm"
                                                readonly="true"/>
                                </c:when>
                                <c:otherwise>
                                    <form:select path="id" id="listAp" class="form-control input-sm">
                                        <form:option value="-1" label="-- choose AP"/>
                                        <form:options items="${affiliateProgramList}" itemValue="id" itemLabel="name"/>
                                    </form:select>
                                </c:otherwise>
                            </c:choose>
                            <div class="has-error">
                                <form:errors path="affiliateProgramName" class="help-inline"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-lable">Id (for Ccbill and other)</label>
                        <div class="col-md-7">
                            <form:input type="text" path="affiliateId" id="affiliateProgramID_id"
                                        class="form-control input-sm"/>
                            <div class="has-error">
                                <form:errors path="affiliateId" class="help-inline"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-lable">Login</label>
                        <div class="col-md-7">
                            <form:input type="text" path="login" id="login_id" class="form-control input-sm"
                                        readonly="${affiliateProgramConstantInfo.login != null}"/>
                            <div class="has-error">
                                <form:errors path="login" class="help-inline"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-lable" for="password">Password</label>
                        <div class="col-md-7">
                            <form:input type="password" path="password" id="password" class="form-control input-sm"/>
                            <div class="has-error">
                                <form:errors path="password" class="help-inline"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-lable">StartDate</label>
                        <div class="col-md-7">
                            <form:input type="text" path="startDate" id="startDate_id" class="form-control input-sm"/>
                            <div class="has-error">
                                <form:errors path="startDate" class="help-inline"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-actions floatRight">
                        <input type="submit" value="Create/Update" class="btn btn-primary btn-sm"/>
                    </div>
                </div>
            </form:form>
            <div class="row">
                <div class="col-xl-12" style="background-color: cornsilk">
                    <p class="h4">or</p>
                    <p class="h5">add you affiliate programs from file</p>
                </div>
            </div>
            <div class="row">
                <form method="POST" action="<c:url value='/users/${myUser.loginName}/uploadLoginFile'/>"
                      enctype="multipart/form-data">
                    <input type="file" name="file"/>
                    <div class="form-actions floatRight">
                        <input type="submit" value="Submit" class="btn btn-primary btn-sm"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
        integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
        crossorigin="anonymous"></script>
</body>

</html>