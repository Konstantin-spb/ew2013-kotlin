<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="<c:url value='/static/css/main.css' />" rel="stylesheet">
    <link href="<c:url value='/static/css/menu-horizontal.css' />" rel="stylesheet"/>
    <title>Title</title>
</head>
<body>
<h1>${executorCounter}</h1>
<form action="<c:url value='/admin/get-programs-info' />"
      method="get">
    <button type="submit">Get Program Info</button>
</form>

<form action="<c:url value='/admin/get-site-info'/>"
      method="get">
    <button type="submit">Get Site Info</button>
</form>

<div>
    <table>
        <thead>

        </thead>
        <tbody>
        <c:set var="tempString" value=""/>
        <c:forEach items="${programList}" var="program">
            <c:choose>
                <c:when test="${program[0] eq tempString}">
                    <tr>
                        <td></td>
                        <td>${program[6]}</td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:set var="tempString" value="${program[0]}"/>
                    <tr>
                        <td>${program[3]}</td>
                        <td>${program[6]}</td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
