package net.ew2013.webobject;

/**
 * Created by konstantin on 29.08.16.
 */
public class SiteForWeb {
    private String siteName;
    private StatsForWeb statsForWebs;
    private String downloadingStatus;
    private String lastDate;

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public StatsForWeb getStatsForWebs() {
        return statsForWebs;
    }

    public void setStatsForWebs(StatsForWeb statsForWebs) {
        this.statsForWebs = statsForWebs;
    }

    public String getDownloadingStatus() {
        return downloadingStatus;
    }

    public void setDownloadingStatus(String downloadingStatus) {
        this.downloadingStatus = downloadingStatus;
    }

    public String getLastDate() {
        return lastDate;
    }

    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }
}
