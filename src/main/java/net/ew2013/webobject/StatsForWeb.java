package net.ew2013.webobject;

/**
 * Created by konstantin on 29.08.16.
 */
public class StatsForWeb {

    private long raw;
    private long uniq;
    private long signC;
    private long signM;
    private long rebillC;
    private long rebillM;
    private long refundC;
    private long refundM;
    private long totalM;

    public long getRaw() {
        return raw;
    }

    public void setRaw(long raw) {
        this.raw = raw;
    }

    public long getUniq() {
        return uniq;
    }

    public void setUniq(long uniq) {
        this.uniq = uniq;
    }

    public long getSignC() {
        return signC;
    }

    public void setSignC(long signC) {
        this.signC = signC;
    }

    public long getSignM() {
        return signM;
    }

    public void setSignM(long signM) {
        this.signM = signM;
    }

    public long getRebillC() {
        return rebillC;
    }

    public void setRebillC(long rebillC) {
        this.rebillC = rebillC;
    }

    public long getRebillM() {
        return rebillM;
    }

    public void setRebillM(long rebillM) {
        this.rebillM = rebillM;
    }

    public long getRefundC() {
        return refundC;
    }

    public void setRefundC(long refundC) {
        this.refundC = refundC;
    }

    public long getRefundM() {
        return refundM;
    }

    public void setRefundM(long refundM) {
        this.refundM = refundM;
    }

    public long getTotalM() {
        return totalM;
    }

    public void setTotalM(long totalM) {
        this.totalM = totalM;
    }
//    projectionList.add(Projections.sum("rawClicks").as("raw"));
//    projectionList.add(Projections.sum("uniqueClicks").as("uniq"));
//    projectionList.add(Projections.sum("signuoCount").as("signC"));
//    projectionList.add(Projections.sum("signupMoney").as("signM"));
//    projectionList.add(Projections.sum("rebillCount").as("rebillC"));
//    projectionList.add(Projections.sum("rebillMoney").as("rebillM"));
//    projectionList.add(Projections.sum("refundChargebackCount").as("refundC"));
//    projectionList.add(Projections.sum("refundChargebackMoney").as("refundM"));
//    projectionList.add(Projections.sum("totalMoney").as("totalM"));
//    projectionList.add(Projections.groupProperty("user"));
//    projectionList.add(Projections.groupProperty("loginInfo"));
//    projectionList.add(Projections.groupProperty("affiliateProgram"));
//    projectionList.add(Projections.groupProperty("sitesFromAps"));
}
