package net.ew2013.webobject;

import net.ew2013.model.AffiliateProgram;

import java.sql.Date;

/**
 * Created by konstantin on 05.08.16.
 */
public class LoginInfoFromForm {
    private AffiliateProgram affiliateProgram;
    private String login;
    private String password;
    private Date startDate;
    private String affiliateLoginId;

    public String getAffiliateLoginId() {
        return affiliateLoginId;
    }

    public void setAffiliateLoginId(String affiliateLoginId) {
        this.affiliateLoginId = affiliateLoginId;
    }

    public AffiliateProgram getAffiliateProgram() {
        return affiliateProgram;
    }

    public void setAffiliateProgram(AffiliateProgram affiliateProgram) {
        this.affiliateProgram = affiliateProgram;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
