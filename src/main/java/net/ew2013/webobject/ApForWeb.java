package net.ew2013.webobject;

import java.util.List;

/**
 * Created by konstantin on 29.08.16.
 */
public class ApForWeb {

    private String login;
    private String startDate;
    private String endDate;
    private String apName;
    private String type;
    private String url;
    private List<SiteForWeb> siteList;
    private String downloadingStatus;

    public String getDownloadingStatus() {
        return downloadingStatus;
    }

    public void setDownloadingStatus(String downloadingStatus) {
        this.downloadingStatus = downloadingStatus;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getApName() {
        return apName;
    }

    public void setApName(String apName) {
        this.apName = apName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<SiteForWeb> getSiteList() {
        return siteList;
    }

    public void setSiteList(List<SiteForWeb> siteList) {
        this.siteList = siteList;
    }
}
