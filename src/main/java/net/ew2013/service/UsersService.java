package net.ew2013.service;


import net.ew2013.model.AppUser;

import java.util.List;

public interface UsersService {
	
	AppUser findOne(Long id);

	AppUser findByName(String name);
	
	AppUser saveOrUpdate(AppUser user);
	
	List<AppUser> findAll();
	
	boolean isUsersSSOUnique(Long id, String sso);

	String getPrincipal();
}