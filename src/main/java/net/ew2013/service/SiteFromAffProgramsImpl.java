package net.ew2013.service;

import net.ew2013.model.AffiliateProgram;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;
import net.ew2013.repository.SiteFromAffProgramRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by konstantin on 13.04.17.
 */
@Service
public class SiteFromAffProgramsImpl implements SiteFromAffProgramsService {

    @Autowired
    SiteFromAffProgramRepository siteFromAffProgramRepository;

    @Override
    public List<SiteFromAffProgram> getAll() {
        return siteFromAffProgramRepository.findAll();
    }

    @Override
    public SiteFromAffProgram findByExtIdAndApName(String extId, AffiliateProgram affiliateProgram) {
        System.err.println("Method do not work - findByExtIdAndApName");
        return null;
    }

    @Override
    public SiteFromAffProgram saveOrUpdate(SiteFromAffProgram siteFromAps) {
        return siteFromAffProgramRepository.save(siteFromAps);
    }

    @Override
    public List<SiteFromAffProgram> findAllSitesByUser(LoginInfo loginInfo) {
        return null;
    }

    @Override
    public void delete(SiteFromAffProgram siteFromAps) {

    }
}
