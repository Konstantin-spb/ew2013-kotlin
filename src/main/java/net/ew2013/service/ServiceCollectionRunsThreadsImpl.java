package net.ew2013.service;

import net.ew2013.model.LoginInfo;
import net.ew2013.model.ServiceCollectionRunsThreads;
import net.ew2013.model.SiteFromAffProgram;
import net.ew2013.repository.ServiceCollectionRunsThreadsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceCollectionRunsThreadsImpl implements ServiceCollectionRunsThreadsService{

    @Autowired
    ServiceCollectionRunsThreadsRepository serviceCollectionRunsThreadsRepository;

    @Override
    public ServiceCollectionRunsThreads setServiceCollectionRunsThreads(SiteFromAffProgram siteFromAffProgram, LoginInfo loginInfo) {

        ServiceCollectionRunsThreads serviceCollectionRunsThreads = new ServiceCollectionRunsThreads();
        serviceCollectionRunsThreads.setSiteFromAffProgram(siteFromAffProgram);
        serviceCollectionRunsThreads.setAffiliateProgram(loginInfo.getAffiliateProgram());
        serviceCollectionRunsThreads.setLoginInfo(loginInfo);
        serviceCollectionRunsThreads.setAppUser(loginInfo.getAppUser());
        return serviceCollectionRunsThreadsRepository.save(serviceCollectionRunsThreads);
    }
}
