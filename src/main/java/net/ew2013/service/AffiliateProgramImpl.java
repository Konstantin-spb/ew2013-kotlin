package net.ew2013.service;

import net.ew2013.model.AffiliateProgram;
import net.ew2013.model.Domain;
import net.ew2013.repository.AffiliateProgramRepository;
import net.ew2013.repository.CssSelectorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.List;

/**
 * Created by konstantin on 01.11.16.
 */
@Service
@Transactional
public class AffiliateProgramImpl implements AffiliateProgramService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AffiliateProgramImpl.class);

    @Autowired
    AffiliateProgramRepository affiliateProgramRepository;
    @Autowired
    ProtocolService protocolService;
    @Autowired
    DomainService domainService;
    @Autowired
    CssSelectorRepository cssSelectorRepository;

    @Override
    public void addAffiliateProgramsFromExternalFile(InputStream is) {
        try {
//            InputStream is = new FileInputStream(file);
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));
            String line = buf.readLine();

            while (line != null) {
                String[] sArray = line.split("\\|");
                try {
                    if (findByName(sArray[0].trim()) == null && (sArray[1].equals("http") || sArray[1].equals("https"))) {
                        AffiliateProgram affiliateProgram = new AffiliateProgram();
                        affiliateProgram.setName(sArray[0].trim());
                        if (sArray[1].equals("http")) {
                            affiliateProgram.setProtocol(protocolService.findOne(1L));
                        } else if (sArray[1].equals("https")) {
                            affiliateProgram.setProtocol(protocolService.findOne(2L));
                        }

                        Domain domain = domainService.findByName(sArray[2].trim());
                        if (domain == null) {
                            domain = new Domain();
                        }
                        domain.setDomainName(sArray[2].trim());
                        domain = domainService.saveOrUpdate(domain);
                        affiliateProgram.setDomain(domain);
                        affiliateProgram.setType(sArray[3].trim());
//                        if (sArray.length == 5) {
                        affiliateProgram.setUri(sArray[4]);
                        affiliateProgram.setJavascriptEnable(Boolean.parseBoolean(sArray[5].trim()));
                        if (sArray.length > 6 && !sArray[6].trim().isEmpty()) {
                            affiliateProgram.setCssSelector(cssSelectorRepository.findOne(Long.parseLong(sArray[6].trim())));
                        }
//                        }
                        saveOrUpdate(affiliateProgram);
                        affiliateProgram = null;
                        domain = null;
                    } else {
                        System.out.println("Ap " + sArray[2] + " is exist...");
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    LOGGER.warn("String has errors ---> " + line);
                }
                line = buf.readLine();
            }
        } catch (IOException e) {
            LOGGER.warn("Affiliate Programs don't load from files");
        }
    }

    //TODO will make normal query
    public List<Object[]> getAfProgramWithSalePrograms() {
//        Session session = sessionFactory.getCurrentSession();
//        Query query = session.createSQLQuery(
//                "SELECT a.id AS af_id," +
//                        "  p.PROTOCOLNAME," +
//                        "  a.DOMAIN," +
//                        "  a.NAME," +
//                        "  s.ID," +
//                        "  s.EXTERNAL_ID," +
//                        "  s.PROGRAM_NAME " +
//                        "FROM AFFILIATE_PROGRAMS AS a LEFT JOIN SALE_PROGRAMS AS s ON a.id = s.AFFILIATE_PROGRAM" +
//                        "  LEFT JOIN PROTOCOL AS p ON a.PROTOCOL_ID = p.ID ORDER BY a.ID;");

        List<Object[]> afProg = null;
//                afProg = query.list();
        String temp = "";
        for (Object[] o : afProg) {
            if (!o[0].toString().equals(temp)) {
                temp = o[0].toString();
                System.out.println();
                System.out.print(o[3] + " ---> ");
            }
//            System.out.print(o[6]);
        }
        return afProg;
    }

    @Override
    public List<AffiliateProgram> findAll() {
        return affiliateProgramRepository.findAll();
    }

    @Override
    public AffiliateProgram findByDomain(Domain domain) {
        return affiliateProgramRepository.findByDomain(domain);
    }

    @Override
    public AffiliateProgram saveOrUpdate(AffiliateProgram affiliateProgram) {
        return affiliateProgramRepository.saveAndFlush(affiliateProgram);
    }

    @Override
    public void delete(long id) {
        affiliateProgramRepository.delete(id);
    }

    @Override
    public AffiliateProgram findByName(String name) {
        return affiliateProgramRepository.findByName(name);
    }

}
