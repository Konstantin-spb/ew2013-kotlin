package net.ew2013.service;

import net.ew2013.model.LoginInfo;

import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * Created by konstantin on 08.08.16.
 */
public interface LoginInfoService {
    LoginInfo getReamLoginFromDb(String login, String affiliateProgramName);

    void addLoginsFromExternalFile(File file);

    void addLoginFromFile(InputStream inputStream);
//    LoginInfo getLoginFromDB(int apId, int loginId);
    LoginInfo findOne(Long id);
    LoginInfo saveOrUpdate(LoginInfo loginInfo);
    void delete(long id);
    List<LoginInfo> findAll();
}
