package net.ew2013.service;

import net.ew2013.model.SaleProgram;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
public interface SaleProgramService {
    SaleProgram saveOrUpdate(SaleProgram saleProgram);
    void delete(long id);
    List<SaleProgram> findAll();
}
