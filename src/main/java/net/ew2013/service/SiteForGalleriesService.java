package net.ew2013.service;

import net.ew2013.model.SiteForGalleries;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
public interface SiteForGalleriesService {
    SiteForGalleries saveOrUpdate(SiteForGalleries siteForGalleries);
    void delete(long id);
    List<SiteForGalleries> findAll();
}
