package net.ew2013.service;

import net.ew2013.model.LoginInfo;
import net.ew2013.model.ServiceCollectionRunsThreads;
import net.ew2013.model.SiteFromAffProgram;

public interface ServiceCollectionRunsThreadsService {

    ServiceCollectionRunsThreads setServiceCollectionRunsThreads(SiteFromAffProgram siteFromAffProgram, LoginInfo loginInfo);
}
