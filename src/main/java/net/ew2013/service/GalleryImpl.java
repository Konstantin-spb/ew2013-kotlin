package net.ew2013.service;

import net.ew2013.model.Gallery;
import net.ew2013.repository.GalleryRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
public class GalleryImpl implements GalleryService{

    @Autowired
    GalleryRepository galleryRepository;

    @Override
    public Gallery saveOrUpdate(Gallery gallery) {
        return galleryRepository.save(gallery);
    }

    @Override
    public void delete(long id) {
        galleryRepository.delete(id);
    }

//    @Override
//    public Gallery findByName(String name) {
//        return null;
//    }

    @Override
    public List<Gallery> findAll() {
        return galleryRepository.findAll();
    }
}
