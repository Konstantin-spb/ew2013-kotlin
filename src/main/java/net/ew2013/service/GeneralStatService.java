package net.ew2013.service;

import net.ew2013.model.GeneralStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;

import java.util.Date;
import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
public interface GeneralStatService {

    GeneralStat saveOrUpdate(GeneralStat generalStat);
    void delete(long id);
    List<GeneralStat> findAll();
    GeneralStat findStatsDateByLoginSiteAp(LoginInfo loginInfo, SiteFromAffProgram siteFromAffProgram, Date statsDate);
}
