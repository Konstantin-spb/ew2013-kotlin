package net.ew2013.service;


import net.ew2013.model.AppUserProfile;

import java.util.List;

public interface UserProfileService {

	AppUserProfile findOne(long id);

	AppUserProfile findByType(String type);
	
	List<AppUserProfile> findAll();
	
}
