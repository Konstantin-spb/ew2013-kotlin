package net.ew2013.service;

import net.ew2013.model.Status;

import java.util.List;

/**
 * Created by konstantin on 12.04.17.
 */
public interface StatusService {
        Status saveOrUpdate(Status status);
        void delete(long id);
        List<Status> findAll();
        Status findByName(String name);
}
