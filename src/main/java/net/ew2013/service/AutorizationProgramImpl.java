package net.ew2013.service;

import net.ew2013.model.AutorizationHistory;
import net.ew2013.repository.AutorizationHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
@Service
public class AutorizationProgramImpl implements AutorizationHistoryService {

    @Autowired
    AutorizationHistoryRepository autorizationHistoryRepository;

    @Override
    public AutorizationHistory saveOrUpdate(AutorizationHistory autorizationHistory) {
        return autorizationHistoryRepository.save(autorizationHistory);
    }

    @Override
    public void delete(long id) {
        autorizationHistoryRepository.delete(id);
    }

//    @Override
//    public AutorizationHistory findByName(String name) {
//        return autorizationHistoryRepository.findByName(name);
//    }
    @Override
    public List<AutorizationHistory> findAll() {
        return autorizationHistoryRepository.findAll();
}

}
