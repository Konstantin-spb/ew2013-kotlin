package net.ew2013.service;

import net.ew2013.model.AffiliateProgram;
import net.ew2013.model.Domain;

import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * Created by konstantin on 01.11.16.
 */
public interface AffiliateProgramService {
    void addAffiliateProgramsFromExternalFile(InputStream io);
    List<Object[]> getAfProgramWithSalePrograms();

    AffiliateProgram saveOrUpdate(AffiliateProgram status);
    void delete(long id);
    AffiliateProgram findByName(String name);
    List<AffiliateProgram> findAll();
    AffiliateProgram findByDomain(Domain domain);
}
