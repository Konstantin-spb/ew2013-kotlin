package net.ew2013.service;

import net.ew2013.model.Status;
import net.ew2013.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by konstantin on 12.04.17.
 */
@Service
public class StatusServiceImpl implements StatusService {

    private final
    StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public Status saveOrUpdate(Status status) {
        return statusRepository.save(status);
    }

    @Override
    public void delete(long id) {
        statusRepository.delete(id);
    }

    @Override
    public List<Status> findAll() {
        return statusRepository.findAll();
    }

    @Override
    public Status findByName(String name) {
        return statusRepository.findByName(name);
    }
}
