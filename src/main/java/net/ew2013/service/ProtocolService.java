package net.ew2013.service;

import net.ew2013.model.Protocol;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
public interface ProtocolService {
    Protocol saveOrUpdate(Protocol protocol);
    Protocol findOne(Long id);
    void delete(long id);
    List<Protocol> findAll();
    Protocol findByName(String name);
}
