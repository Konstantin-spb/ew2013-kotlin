package net.ew2013.service;

import net.ew2013.downloaders.DownloadersThreads;
import net.ew2013.model.AppUser;
import net.ew2013.model.LastdateDownloadStat;
import net.ew2013.model.LoginInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.Set;


/**
 * Created by konstantin on 11.08.16.
 */
@Repository
@Scope(value = "prototype")
public class DownloadServiceImpl implements DownloadService {

    @Autowired
    DownloadersThreads downloadersThreads;
    @Autowired
    UsersService userService;
    @Autowired
    LoginInfoService loginInfoService;
    @Autowired
    LastdateDownloadStatService lastdateDownloadStatService;

//    @Autowired
//    ExecutorService executorService;

    @Override
    public void downloadStatsOneAp (Long loginId) {
        LoginInfo loginInfo = loginInfoService.findOne(loginId);
        lastdateDownloadStatService.setLastdateByLoginInfoToInProgressMode(loginInfo);
        downloadersThreads.startDownloadsAP(loginInfo);
    }

    @Override
//    @Async
    public void downloadStatsForAllProgram() {
        AppUser appUser = userService.findByName(userService.getPrincipal());
        lastdateDownloadStatService.setAllLastdatesByUserToInProgressMode(appUser);
        Set<LoginInfo> loginInfoList = appUser.getLoginInfos();
        for (LoginInfo l :
                loginInfoList) {
            downloadersThreads.startDownloadsAP(l);
        }

    }
}
