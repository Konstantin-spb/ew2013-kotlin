package net.ew2013.service;

import net.ew2013.model.AffiliateProgram;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;

import java.util.List;

/**
 * Created by konstantin on 13.04.17.
 */
public interface SiteFromAffProgramsService {
    List<SiteFromAffProgram> getAll();
    SiteFromAffProgram findByExtIdAndApName(String extId, AffiliateProgram affiliateProgram);
    SiteFromAffProgram saveOrUpdate(SiteFromAffProgram siteFromAps);
    List<SiteFromAffProgram> findAllSitesByUser(LoginInfo loginInfo);
    void delete(SiteFromAffProgram siteFromAps);
}
