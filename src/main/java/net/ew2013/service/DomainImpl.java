package net.ew2013.service;

import net.ew2013.model.Domain;
import net.ew2013.repository.DomainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
@Service
public class DomainImpl implements DomainService {

    @Autowired
    DomainRepository domainRepository;

    @Override
    public Domain saveOrUpdate(Domain domain) {
        return domainRepository.save(domain);
    }

    @Override
    public void delete(long id) {
        domainRepository.delete(id);
    }

    @Override
    public Domain findByName(String name) {
        return domainRepository.findByName(name);
    }

    @Override
    public List<Domain> findAll() {
        return domainRepository.findAll();
    }
}
