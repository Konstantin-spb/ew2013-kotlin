package net.ew2013.service;

import net.ew2013.model.AppUser;
import net.ew2013.model.LastdateDownloadStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;
import net.ew2013.repository.LastdateDownloadStatRepository;
import net.ew2013.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by konstantin on 12.04.17.
 */
@Service
public class LastdateDownloadStatsImpl implements LastdateDownloadStatService{

    @Autowired
    StatusRepository statusRepository;
    @Autowired
    LastdateDownloadStatRepository lastdateDownloadStatRepository;

    @Override
    public LastdateDownloadStat getLastdateForSite(SiteFromAffProgram site, LoginInfo loginInfo) {
        return lastdateDownloadStatRepository.getLastdateForSite(site, loginInfo);
    }

    @Override
    public LastdateDownloadStat saveOrUpdate(LastdateDownloadStat lastdateDownloadStat) {
        return lastdateDownloadStatRepository.save(lastdateDownloadStat);
    }

    @Override
    public void delete(long id) {
        lastdateDownloadStatRepository.delete(id);
    }

    @Override
    public List<LastdateDownloadStat> findAll() {
        return lastdateDownloadStatRepository.findAll();
    }

    @Override
    public LastdateDownloadStat saveLastDateWithStatus (AppUser appUser, LoginInfo loginInfo, SiteFromAffProgram siteFromAffProgram, String statusName, Date date) {
        LastdateDownloadStat lastdateDownloadStat = lastdateDownloadStatRepository.getLastdateForSite(siteFromAffProgram, loginInfo);

        if (lastdateDownloadStat == null) {
            lastdateDownloadStat = new LastdateDownloadStat();
        }

        lastdateDownloadStat.setAppUser(appUser);
        lastdateDownloadStat.setLastDate(date);
        lastdateDownloadStat.setLoginInfo(loginInfo);
        lastdateDownloadStat.setSiteFromAffProgram(siteFromAffProgram);
        lastdateDownloadStat.setStatus(statusRepository.findByName(statusName));
        lastdateDownloadStatRepository.save(lastdateDownloadStat);
        return lastdateDownloadStat;
    }

    public void setLastdateByLoginInfoToInProgressMode(LoginInfo loginInfo) {
        List<LastdateDownloadStat> lastdateDownloadStatList = lastdateDownloadStatRepository.findAllByLoginInfo(loginInfo);
        for (LastdateDownloadStat l :
                lastdateDownloadStatList) {
            l.setStatus(statusRepository.findOne(5L));//In progress mod
            lastdateDownloadStatRepository.save(l);
        }
    }


    @Override
    public void setAllLastdatesByUserToInProgressMode(AppUser appUser) {
        List<LastdateDownloadStat> lastdateDownloadStatList = lastdateDownloadStatRepository.findAllByAppUser(appUser);
        for (LastdateDownloadStat l :
                lastdateDownloadStatList) {
            l.setStatus(statusRepository.findOne(5L));//In progress mod
            lastdateDownloadStatRepository.save(l);
        }
    }
}
