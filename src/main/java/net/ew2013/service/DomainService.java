package net.ew2013.service;

import net.ew2013.model.Domain;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
public interface DomainService {
    Domain saveOrUpdate(Domain domain);
    void delete(long id);
    Domain findByName(String name);
    List<Domain> findAll();
}
