package net.ew2013.service;

import net.ew2013.model.AppUser;
import net.ew2013.model.LastdateDownloadStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;

import java.util.Date;
import java.util.List;

/**
 * Created by konstantin on 12.04.17.
 */
public interface LastdateDownloadStatService {
    LastdateDownloadStat getLastdateForSite(SiteFromAffProgram site, LoginInfo loginInfo);
    LastdateDownloadStat saveOrUpdate(LastdateDownloadStat lastdateDownloadStat);
    void delete(long id);
    List<LastdateDownloadStat> findAll();
    LastdateDownloadStat saveLastDateWithStatus(AppUser appUser, LoginInfo loginInfo, SiteFromAffProgram siteFromAffProgram, String statusName, Date date);
    void setLastdateByLoginInfoToInProgressMode(LoginInfo loginInfo);
    void setAllLastdatesByUserToInProgressMode(AppUser appUser);
}
