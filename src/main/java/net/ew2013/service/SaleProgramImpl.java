package net.ew2013.service;

import net.ew2013.model.SaleProgram;
import net.ew2013.repository.SaleProgramRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
@Service
public class SaleProgramImpl implements SaleProgramService {
    @Autowired
    SaleProgramRepository saleProgramRepository;

    @Override
    public SaleProgram saveOrUpdate(SaleProgram saleProgram) {
        return saleProgramRepository.save(saleProgram);
    }

    @Override
    public void delete(long id) {
        saleProgramRepository.delete(id);
    }

    @Override
    public List<SaleProgram> findAll() {
        return saleProgramRepository.findAll();
    }
}
