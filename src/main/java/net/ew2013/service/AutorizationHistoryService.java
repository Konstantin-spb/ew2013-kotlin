package net.ew2013.service;

import net.ew2013.model.AutorizationHistory;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
public interface AutorizationHistoryService {
    AutorizationHistory saveOrUpdate(AutorizationHistory autorizationHistory);
    void delete(long id);
//    AutorizationHistory findByName(String name);
    List<AutorizationHistory> findAll();
}
