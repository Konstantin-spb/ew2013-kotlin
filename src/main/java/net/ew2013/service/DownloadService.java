package net.ew2013.service;

/**
 * Created by konstantin on 11.08.16.
 */
public interface DownloadService {

    void downloadStatsOneAp(Long loginId);
    void downloadStatsForAllProgram();
}
