package net.ew2013.service;

import net.ew2013.model.PersistentLogins;
import net.ew2013.repository.PersistentLoginsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
@Service
public class PersistentsLoginsImpl implements PersistentLoginsService {

    @Autowired
    PersistentLoginsRepository persistentLoginsRepository;

    @Override
    public PersistentLogins saveOrUpdate(PersistentLogins persistentLogins) {
        return persistentLoginsRepository.save(persistentLogins);
    }

    @Override
    public void delete(long id) {
        persistentLoginsRepository.delete(id);
    }

    @Override
    public List<PersistentLogins> findAll() {
        return persistentLoginsRepository.findAll();
    }

    @Override
    public PersistentLogins findByName(String name) {
        return persistentLoginsRepository.findByName(name);
    }
}
