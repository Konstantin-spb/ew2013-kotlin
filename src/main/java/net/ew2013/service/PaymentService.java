package net.ew2013.service;

import net.ew2013.model.Payment;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
public interface PaymentService {
    Payment saveOrUpdate(Payment payment);
    void delete(long id);
    List<Payment> findAll();
}
