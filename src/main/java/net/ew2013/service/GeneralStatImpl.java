package net.ew2013.service;

import net.ew2013.model.GeneralStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;
import net.ew2013.repository.GeneralStatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
@Service
public class GeneralStatImpl implements GeneralStatService {

    @Autowired
    GeneralStatRepository generalStatRepository;

    @Override
    public GeneralStat saveOrUpdate(GeneralStat generalStat) {
        return generalStatRepository.save(generalStat);
    }

    @Override
    public void delete(long id) {
        generalStatRepository.delete(id);
    }

    @Override
    public List<GeneralStat> findAll() {
        return generalStatRepository.findAll();
    }

    @Override
    public GeneralStat findStatsDateByLoginSiteAp(LoginInfo loginInfo, SiteFromAffProgram siteFromAffProgram, Date statsDate) {
        String login = loginInfo.getLogin();
        String ap = loginInfo.getAffiliateProgram().getName();
        String site = siteFromAffProgram.getSiteName();
        String date = String.valueOf(statsDate.getTime());
        return generalStatRepository.findByLoginInfoAndAffiliateProgramAndSiteFromAffProgramAndStatsDate(loginInfo, loginInfo.getAffiliateProgram(), siteFromAffProgram, statsDate);
    }
}
