package net.ew2013.service;

import net.ew2013.model.Protocol;
import net.ew2013.repository.ProtocolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
@Service
public class ProtocolImpl implements ProtocolService {
    @Autowired
    ProtocolRepository protocolRepository;

    public Protocol findOne(Long id) {
        return protocolRepository.findOne(id);
    }

    @Override
    public Protocol saveOrUpdate(Protocol protocol) {
        return protocolRepository.save(protocol);
    }

    @Override
    public void delete(long id) {
        protocolRepository.delete(id);
    }

    @Override
    public List<Protocol> findAll() {
        return protocolRepository.findAll();
    }

    @Override
    public Protocol findByName(String name) {
        return protocolRepository.findByName(name);
    }
}
