package net.ew2013.service;

import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;
import net.ew2013.repository.SiteFromAffProgramRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by konstantin on 31.08.16.
 */
@Component
@Scope(value = "prototype")
public class SiteService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SiteService.class);

    @Autowired
    SiteFromAffProgramsService siteFromAffProgramsService;
    @Autowired
    SiteFromAffProgramRepository siteFromAffProgramRepository;

    /*Берёт параметры сайта из базы, если сайта в базе нет, то добавляет его туда*/
    @Transactional
    public synchronized SiteFromAffProgram setOneSite(String externalSiteId, String siteName, LoginInfo loginInfo) {

        SiteFromAffProgram sfa;

        sfa = siteFromAffProgramRepository.findByAffiliateProgramAndExternalSiteId(loginInfo.getAffiliateProgram(), externalSiteId);

        if (sfa == null) {
            sfa = new SiteFromAffProgram();
            sfa.setAffiliateProgram(loginInfo.getAffiliateProgram());
            sfa.setExternalSiteId(externalSiteId);
            sfa.setSiteName(siteName);
            siteFromAffProgramsService.saveOrUpdate(sfa);
        } else {
//            LOGGER.info("Site Already exists");
        }
        return sfa;
    }
}
