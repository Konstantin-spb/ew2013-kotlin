package net.ew2013.service;

import net.ew2013.model.Gallery;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
public interface GalleryService {

    Gallery saveOrUpdate(Gallery gallery);
    void delete(long id);
//    Gallery findByName(String name);
    List<Gallery> findAll();
}
