package net.ew2013.service;

import net.ew2013.model.AffiliateProgram;
import net.ew2013.model.AppUser;
import net.ew2013.model.Domain;
import net.ew2013.model.LoginInfo;
import net.ew2013.repository.DomainRepository;
import net.ew2013.repository.LoginInfoRepository;
import net.ew2013.security.AES;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.io.FileUtils.readLines;

/**
 * Created by konstantin on 08.08.16.
 */
@Service
public class LoginInfoImpl implements LoginInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginInfoImpl.class);

    @Autowired
    UsersService usersService;
    @Autowired
    LoginInfoRepository loginInfoRepository;
    @Autowired
    AffiliateProgramService affiliateProgramService;
    @Autowired
    DomainService domainService;
    @Autowired
    DomainRepository domainRepository;

    public void addLoginFromFile(InputStream inputStream) {
        AppUser user = usersService.findByName(usersService.getPrincipal());
//        InputStream inputStream = null;
        BufferedReader buf = null;
        try {
//            inputStream = new FileInputStream(file);
            buf = new BufferedReader(new InputStreamReader(inputStream));
            String line = buf.readLine();

            while (line != null) {
                String[] sArray = line.split("\\|");
                try {
//                    System.out.println(sArray[0]);

                    AffiliateProgram ap = null;
                    if (sArray[0].length() > 0) {
//                        Domain domain = domainRepository.findByDomainNameContains(sArray[0]);
                        Domain domain = domainRepository.findByDomainName(sArray[0]);
                        ap = affiliateProgramService.findByDomain(domain);
                    }
                    if (ap != null) {
                        LoginInfo loginInfo = loginInfoRepository.findByAppUserAndAffiliateProgramAndLogin(user, ap, sArray[1]);
                        if (loginInfo == null) {
                            loginInfo = new LoginInfo();
                            loginInfo.setLogin(sArray[1]);
                            loginInfo.setPassword(new AES().encrypt((sArray[2]), user.getPassword()));
                            loginInfo.setStartDate(Date.valueOf(sArray[3]));
                            try {
                                if (sArray.length > 4) {
                                    loginInfo.setExternalAffiliateLoginId(sArray[4]);
                                }
                            } catch (Exception e) {
                                LOGGER.error("Array error");
                            }
                            loginInfo.setAffiliateProgram(ap);
                            loginInfo.setAppUser(user);
                            saveOrUpdate(loginInfo);
                            loginInfo = null;
                        }
                        System.out.println("Added login ... for " + ap.getName());
                    } else {
                        System.out.println("Do not have this AP ... " + sArray[0]);
                    }
                } catch (Exception e) {
                    LOGGER.warn("Login do not added - " + line);
                    e.printStackTrace();
                }
                line = buf.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            loginInfoRepository.flush();
            LOGGER.info("AddPrograms function DONE");
            user = null;
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                buf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public LoginInfo getReamLoginFromDb(String login, String affiliateProgramName) {
        AppUser user = usersService.findByName(usersService.getPrincipal());
        LoginInfo loginInfo = null;
        for (LoginInfo l : user.getLoginInfos()) {
            try {
                if (l.getLogin().equals(login) && l.getAffiliateProgram().getName().equals(affiliateProgramName)) {
                    System.out.println(l.getLogin() + " - " + l.getAffiliateProgram().getName());
                    loginInfo = l;
                }
            } catch (NullPointerException e) {
                LOGGER.warn("Empty affiliateProgramName in DB");
            }
        }
        return loginInfo;
    }

    @Override
    public LoginInfo saveOrUpdate(LoginInfo loginInfo) {
        loginInfo = loginInfoRepository.save(loginInfo);
//        loginInfoRepository.flush();
        return loginInfo;
    }

    @Override
    public void delete(long id) {
        loginInfoRepository.delete(id);
    }

    @Override
    public List<LoginInfo> findAll() {
        return loginInfoRepository.findAll();
    }

    @Override
    public void addLoginsFromExternalFile(File file) {

        AppUser user = usersService.findByName(usersService.getPrincipal());

        List<String> fileLines = new ArrayList<>();
        AffiliateProgram affiliateProgram;

        try {
            fileLines = readLines(file);
        } catch (IOException e) {
            e.printStackTrace();
//                logger.error("IOException or File Not Found");
        }

        String[] oneLine;
        for (String s : fileLines) {

            oneLine = s.split("\\|\\|");

            affiliateProgram = affiliateProgramService.findByName(oneLine[0]);
            if (oneLine.length == 9) {
                LoginInfo loginInfo = new LoginInfo();
                if (affiliateProgram != null) {

                } else {
                    affiliateProgram = new AffiliateProgram();
                    affiliateProgram.setName(oneLine[0]);
//                    affiliateProgram.setUrl(oneLine[1]);
                    affiliateProgram.setType(oneLine[2]);
                    if (!oneLine[7].equalsIgnoreCase("none")) {
                        affiliateProgram.setUri(oneLine[7]);
                    }

                    affiliateProgramService.saveOrUpdate(affiliateProgram);
                }
                if (!oneLine[8].equalsIgnoreCase("none")) {
                    loginInfo.setExternalAffiliateLoginId(oneLine[8]);
                }
                loginInfo.setAffiliateProgram(affiliateProgram);
                loginInfo.setLogin(oneLine[3]);
                loginInfo.setPassword(new AES().encrypt((oneLine[4]), user.getPassword()));
                loginInfo.setStartDate(Date.valueOf(oneLine[5]));
                loginInfo.setLastDate(Date.valueOf(oneLine[5]));
                loginInfo.setAppUser(user);
                saveOrUpdate(loginInfo);

                loginInfo = null;
                affiliateProgram = null;
            } else if (s.isEmpty()) {
                //Если строка пустая - ничего не делаем
            } else {
//                    logger.info(s + " -- содержит ошибки");
            }
        }
    }

    public LoginInfo findOne(Long id) {
        return loginInfoRepository.findOne(id);
    }

}
