package net.ew2013.service;

import net.ew2013.model.SiteForGalleries;
import net.ew2013.repository.SiteForGalleriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
@Service
public class SiteForGalleriesImpl implements SiteForGalleriesService {
    @Autowired
    SiteForGalleriesRepository siteForGalleriesRepository;

    @Override
    public SiteForGalleries saveOrUpdate(SiteForGalleries siteForGalleries) {
        return siteForGalleriesRepository.save(siteForGalleries);
    }

    @Override
    public void delete(long id) {
        siteForGalleriesRepository.delete(id);
    }

    @Override
    public List<SiteForGalleries> findAll() {
        return siteForGalleriesRepository.findAll();
    }
}
