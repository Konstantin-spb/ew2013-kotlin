package net.ew2013.service;

import net.ew2013.model.PersistentLogins;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
public interface PersistentLoginsService {
    PersistentLogins saveOrUpdate(PersistentLogins persistentLogins);
    void delete(long id);
    List<PersistentLogins> findAll();
    PersistentLogins findByName(String name);
}
