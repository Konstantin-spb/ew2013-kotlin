package net.ew2013.service;

import net.ew2013.model.Payment;
import net.ew2013.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by konstantin on 18.04.17.
 */
@Service
public class PaymentImpl implements PaymentService {
    @Autowired
    PaymentRepository paymentRepository;

    @Override
    public Payment saveOrUpdate(Payment payment) {
        return paymentRepository.save(payment);
    }

    @Override
    public void delete(long id) {
        paymentRepository.delete(id);
    }

    @Override
    public List<Payment> findAll() {
        return paymentRepository.findAll();
    }
}
