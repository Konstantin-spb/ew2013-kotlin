package net.ew2013.service;

import net.ew2013.model.AppUserProfile;
import net.ew2013.repository.AppUserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("userProfileService")
@Transactional
public class UserProfileServiceImpl implements UserProfileService{
	
	@Autowired
    AppUserProfileRepository appUserProfileRepository;
	
	public AppUserProfile findOne(long id) {
		return appUserProfileRepository.findOne(id);
	}

	public AppUserProfile findByType(String type){
		return appUserProfileRepository.findByType(type);
	}

	public List<AppUserProfile> findAll() {
		return appUserProfileRepository.findAll();
	}
}
