package net.ew2013.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "css_selector", uniqueConstraints = @UniqueConstraint(columnNames = {
        "waiting_this_template",
        "form_tag",
        "user_field",
        "password_field",
        "submit_element"}))
public class CssSelector {

    private long id;
    private String waitingThisTemplate;
    private String formTag;
    private String userField;
    private String passwordField;
    private String submitElement;
    private Set<AffiliateProgram> appUsers = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cssSelector")
    public Set<AffiliateProgram> getAppUsers() {
        return appUsers;
    }

    public void setAppUsers(Set<AffiliateProgram> appUsers) {
        this.appUsers = appUsers;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "waiting_this_template", nullable = false)
    public String getWaitingThisTemplate() {
        return waitingThisTemplate;
    }

    public void setWaitingThisTemplate(String waitingThisTemplate) {
        this.waitingThisTemplate = waitingThisTemplate;
    }

    @Column(name = "form_tag", nullable = false)
    public String getFormTag() {
        return formTag;
    }

    public void setFormTag(String formTag) {
        this.formTag = formTag;
    }

    @Column(name = "user_field", nullable = false)
    public String getUserField() {
        return userField;
    }

    public void setUserField(String userField) {
        this.userField = userField;
    }

    @Column(name = "password_field", nullable = false)
    public String getPasswordField() {
        return passwordField;
    }

    public void setPasswordField(String passwordField) {
        this.passwordField = passwordField;
    }

    @Column(name = "submit_element", nullable = false)
    public String getSubmitElement() {
        return submitElement;
    }

    public void setSubmitElement(String submitElement) {
        this.submitElement = submitElement;
    }
}
