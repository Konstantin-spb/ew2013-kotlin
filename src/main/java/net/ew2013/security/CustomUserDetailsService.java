package net.ew2013.security;

import net.ew2013.model.AppUser;
import net.ew2013.model.AppUserProfile;
import net.ew2013.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	static final Logger LOGGER = LoggerFactory.getLogger(CustomUserDetailsService.class);
	
	@Autowired
	private UsersService userService;
	
	@Transactional(readOnly=true)
	//Здесь определяем правило по которому считаем , что пользователь авторизован.
	public UserDetails loadUserByUsername(String login)
			throws UsernameNotFoundException {
		AppUser appUser = userService.findByName(login);
		LOGGER.info("User : {}", appUser);
		if(appUser==null){
			LOGGER.info("User not found");
			throw new UsernameNotFoundException("Username not found");
		} else if (appUser.getStatus().getId() != 3) {
			LOGGER.info("User is DISABLED");
			throw new UsernameNotFoundException(login + " - User is DISABLED");
		}
			return new org.springframework.security.core.userdetails.User(appUser.getLoginName(), appUser.getPassword(),
				 true, true, true, true, getGrantedAuthorities(appUser));
	}

	
	private List<GrantedAuthority> getGrantedAuthorities(AppUser appUser){
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		for(AppUserProfile appUserProfile : appUser.getAppUserProfiles()){
			LOGGER.info("AppUserProfile : {}", appUserProfile);
			authorities.add(new SimpleGrantedAuthority("ROLE_"+ appUserProfile.getType()));
		}
		LOGGER.info("authorities : {}", authorities);
		return authorities;
	}
	
}
