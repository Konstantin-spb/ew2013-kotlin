package net.ew2013.sqlQueries;

import net.ew2013.sqlQueries.wrapSqlQueries.ApLoginBalance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class SqlQueries {

    @Autowired
    EntityManager entityManager;

    public List<ApLoginBalance> getApLoginBalance(long apUserId) {

//        myUser.loginName
        
        String sql = "SELECT sub.id,sub.protocol_name, sub.domain_name, sub.name, sub.login, acb.amount, sub.max_date FROM" +
                "(SELECT li.id, p.protocol_name, d.domain_name, ap.name, li.login, MAX(ab.date_amount) max_date FROM app_user au" +
                "  LEFT JOIN login_info li ON au.id = li.app_user_id" +
                "  LEFT JOIN affiliate_program ap ON li.affiliate_program_id = ap.id" +
                "  LEFT JOIN account_balance ab ON li.id = ab.login_id" +
                "  LEFT JOIN protocol p ON ap.protocol_id = p.id" +
                "  LEFT JOIN domain d ON ap.domain_id = d.id " +
                "WHERE li.app_user_id = " + apUserId + " " +
                "GROUP BY li.id, p.protocol_name, d.domain_name, ap.name, li.login) sub " +
                "LEFT JOIN account_balance acb ON sub.id = acb.login_id AND sub.max_date = acb.date_amount " +
                "ORDER BY sub.name;";
        List<Object[]> results = entityManager.createNativeQuery(sql).getResultList();
        List<ApLoginBalance> apLoginBalanceList = new ArrayList<>();

        results.stream().forEach((record) -> {
            ApLoginBalance apLoginBalance = new ApLoginBalance();
            Long loginId = Long.parseLong(record[0].toString());
            String protocol = record[1].toString();
            String domain = record[2].toString();
            String apName = (String) record[3];
            String loginName = (String) record[4];
            apLoginBalance.setLoginId(loginId);
            apLoginBalance.setProtocol(protocol);
            apLoginBalance.setDomain(domain);
            apLoginBalance.setApName(apName);
            apLoginBalance.setLoginName(loginName);
            if (record[5] != null) {
                Integer amount = Integer.parseInt(record[5].toString());
                apLoginBalance.setAmount(amount);
            }
            if (record[6] != null) {
                Date lastDateDownloadingBalance = (Date) record[6];
                apLoginBalance.setLastDateDownloadingBalance(lastDateDownloadingBalance);
            }
            apLoginBalanceList.add(apLoginBalance);
        });

        return apLoginBalanceList;
    }
}
