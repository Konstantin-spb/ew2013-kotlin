package net.ew2013.sqlQueries.wrapSqlQueries;


import java.util.Date;

public class ApLoginBalance {

    private long loginId;
    private String protocol;
    private String domain;
    private String apName;
    private String loginName;
    private long amount;
    private Date lastDateDownloadingBalance;

    public long getLoginId() {
        return loginId;
    }

    public void setLoginId(long loginId) {
        this.loginId = loginId;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getApName() {
        return apName;
    }

    public void setApName(String apName) {
        this.apName = apName;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public Date getLastDateDownloadingBalance() {
        return lastDateDownloadingBalance;
    }

    public void setLastDateDownloadingBalance(Date lastDateDownloadingBalance) {
        this.lastDateDownloadingBalance = lastDateDownloadingBalance;
    }
}
