package net.ew2013.downloaders.nats;

import net.ew2013.controller.AppController;
import net.ew2013.downloaders.nats.fileHandlers.xmlHandlers.ReadXmlFileNats3DOM;
import net.ew2013.downloaders.nats.fileHandlers.xmlHandlers.XmlHandlerNats4DOM;
import net.ew2013.downloaders.nats.versions.StatsNats;
import net.ew2013.model.*;
import net.ew2013.repository.*;
import net.ew2013.security.AES;
import net.ew2013.service.LastdateDownloadStatService;
import net.ew2013.service.ServiceCollectionRunsThreadsService;
import net.ew2013.service.SiteService;
import net.ew2013.utils.DataFromDb;
import net.ew2013.utils.PauseSleep;
import net.ew2013.utils.StreamToString;
import net.ew2013.utils.dateUtils.DateWorks;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by konstantin on 14.10.15.
 */
@Component
@Scope(value = "prototype")
public class Nats {

    @Autowired
    SiteService siteService;
    @Autowired
    ApplicationContext context;
    @Autowired
    ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Autowired
    StatusRepository statusRepository;
    @Autowired
    GeneralStatRepository generalStatRepository;
    @Autowired
    LastdateDownloadStatService lastdateDownloadStatService;
    @Autowired
    AccountBalanceRepository accountBalanceRepository;
    @Autowired
    ServiceCollectionRunsThreadsRepository serviceCollectionRunsThreadsRepository;
    @Autowired
    ServiceCollectionRunsThreadsService serviceCollectionRunsThreadsService;

    private List<GeneralStat> generalStatsList;
    private CloseableHttpClient httpClient;
    private CloseableHttpResponse responsePost;

    private HttpGet httpget;
    //    private String tempSourceFile;
    private String xmlData;

    private static final Logger LOGGER = LoggerFactory.getLogger(Nats.class);
    private LoginInfo loginInfo;
    private String protocol;
    private String domain;

    public void setParams(LoginInfo loginInfo) {

        this.protocol = loginInfo.getAffiliateProgram().getProtocol().getProtocolName();
        this.domain = loginInfo.getAffiliateProgram().getDomain().getDomainName();
        this.loginInfo = loginInfo;
    }

    @Async("threadPoolTaskExecutor")
    public void run() throws IOException {

        ServiceCollectionRunsThreads serviceCollectionRunsThreads = serviceCollectionRunsThreadsService.setServiceCollectionRunsThreads(null, loginInfo);
        List<NameValuePair> nvps = null;
        Map<String, String> sitesList = null;
        String threadName = loginInfo.getAffiliateProgram().getType() + "_" + loginInfo.getAppUser().getLoginName()
                + "_" + loginInfo.getAffiliateProgram().getName();
        AppController.lisfOfThreads.add(threadName);
        Thread.currentThread().setName(threadName);

        try {
            String uriFromRequest = "";
            boolean autorizationStatus = autorizationNats(uriFromRequest);
            uriFromRequest = getNatsUriFromLoginPage();

            if (!uriFromRequest.isEmpty()) {
                String uriNatsVersion = "";
                String statsUri = "";
                String attributeValueName;
                String attributeValueSiteId;
                String formFieldPassword = "pass";
                String formFieldUser = "user";
                String login = loginInfo.getLogin();
                nvps = new ArrayList<>();
                String password = new AES().decrypt(loginInfo.getPassword(), loginInfo.getAppUser().getPassword());

                nvps.add(new BasicNameValuePair(formFieldUser, login));
                nvps.add(new BasicNameValuePair(formFieldPassword, password));
                if (autorizationStatus) {
                    getPaymentStats();

                    attributeValueName = "name";
                    attributeValueSiteId = "siteid";
                    if (uriFromRequest.contains("members.php")) {
                        uriNatsVersion = "members.php";
                        statsUri = "stats.php";
                    } else if (uriFromRequest.contains("internal.php")) {
                        uriNatsVersion = "internal.php";
                        statsUri = "internal.php?page=stats&view=date&period=2";
                        attributeValueSiteId = "filter_" + attributeValueSiteId;
                    }

                    sitesList = getSitesList(statsUri, attributeValueName, attributeValueSiteId);
                    for (Map.Entry<String, String> site : sitesList.entrySet()) {
                        getStats(siteService.setOneSite(site.getKey(), site.getValue(), loginInfo), uriNatsVersion);
                        System.out.println(site.getKey() + " --- " + site.getValue());
                    }
                } else {
                    LOGGER.warn(protocol + "://" + domain + " - Не удалось залогиниться :(");
                }
            }
            logout();
        } finally {
            serviceCollectionRunsThreadsRepository.delete(serviceCollectionRunsThreads);
            Thread.currentThread().interrupt();
            if (generalStatsList != null) {
                generalStatsList.clear();
                generalStatsList = null;
            }
            if (nvps != null) {
                nvps.clear();
                nvps = null;
            }
            if (sitesList != null) {
                sitesList.clear();
                sitesList = null;
            }
            AppController.lisfOfThreads.remove(threadName);
            threadPoolTaskExecutor.getThreadPoolExecutor().purge();
            httpClient.close();
            responsePost.close();
//            autorizationNats.closeResources();
        }
    }
//https://stackoverflow.com/questions/36183624/how-to-autowire-by-name-in-spring-with-annotations
    private void getStats(SiteFromAffProgram siteFromAffProgram, String uriNatsVersion) {

        try {
//            PauseSleep.pause(1, 15);
            boolean isGood = false;

            if (uriNatsVersion.equalsIgnoreCase("internal.php")) {
                isGood = getStatsNats4(siteFromAffProgram);
            } else if (uriNatsVersion.equalsIgnoreCase("members.php")) {
                isGood = getStatsNats3(siteFromAffProgram);
            }
            if (isGood) {
                logout();
                saveAllStats(generalStatsList);
                lastdateDownloadStatService.saveLastDateWithStatus(loginInfo.getAppUser(), loginInfo, siteFromAffProgram, "DONE", new java.util.Date());
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(loginInfo.getAffiliateProgram().getName() + "---" + siteFromAffProgram.getSiteName() + " ---> downloading error");
        } finally {
            LOGGER.warn(String.valueOf(threadPoolTaskExecutor.getActiveCount()));
        }
    }

    private boolean getStatsNats3(SiteFromAffProgram sfap) {

        String tempSourceFile;
        boolean goodJob = false;
        LocalDate lastDateForSite = new Date(
                new DataFromDb().getLastDate(sfap, loginInfo).getTime()).toLocalDate().minusDays(1);
        String periodUri = "/stats.php?xml=1&period=8&period_start=" + lastDateForSite + "&period_end=" + DateWorks.getLastDayOfMonth() + "&siteid=" + sfap.getExternalSiteId();
        HttpGet httpgetNats3 = new HttpGet(protocol + "://" + domain + periodUri);

        CloseableHttpResponse responseGet = null;
        HttpEntity entityGet;
        Matcher m;
        Pattern p;
        Pattern p1;
        Matcher m1;
        try {
            responseGet = httpClient.execute(httpgetNats3);
            entityGet = responseGet.getEntity();
//            tempSourceFile = EntityUtils.toString(entityGet);
            tempSourceFile = new StreamToString().getStringFromInputStream(entityGet.getContent());
            p = Pattern.compile("<statistics>([\\S\\s]+?)</statistics>");
            m = p.matcher(tempSourceFile);
            if (m.find()) {
                xmlData = m.group();
//				LOGGER.info(site.getValue() + "(" + site.getKey() + ")" + xmlData);
                goodJob = true;

                if (xmlData.matches("<statistics>[\\s]+<ratiofield>unq</ratiofield>[\\s]+</statistics>")) {
//					goodJob = false;
//					LOGGER.info(siteFromAffProgram.getProgramName() + "(" + siteFromAffProgram.getExternalSiteId() + ") - нет статистики");
                }
                ReadXmlFileNats3DOM readXmlFileNats3DOM = new ReadXmlFileNats3DOM();
                readXmlFileNats3DOM.getStatsNats3DOM(xmlData, loginInfo, sfap);
                generalStatsList = readXmlFileNats3DOM.getGeneralStatsList();
            } else if (xmlData == null) {
                p1 = Pattern.compile("(<by>[\\S\\s]*</by)>");
                m1 = p1.matcher(tempSourceFile);
//				LOGGER.info(site.getValue() + "(" + site.getKey() + ")" + xmlData);
                goodJob = true;

                if (m1.find()) {
                    xmlData = "<statistics>\n" + "\t<ratiofield>unq</ratiofield>" + m1.group() + "\n</statistics>";
//					LOGGER.info(siteFromAffProgram.getProgramName() + "(" + siteFromAffProgram.getExternalSiteId() + ") - статистика получена");
                }
                ReadXmlFileNats3DOM readXmlFileNats3DOM = new ReadXmlFileNats3DOM();
                readXmlFileNats3DOM.getStatsNats3DOM(xmlData, loginInfo, sfap);
                generalStatsList = readXmlFileNats3DOM.getGeneralStatsList();
            }
            tempSourceFile = "";
        } catch (IOException e) {
//            errors.append(e);
//            e.printStackTrace();
        } finally {
//			httpget = null;
            tempSourceFile = null;

            try {
                if (responseGet != null) {
                    responseGet.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            entityGet = null;
            m = null;
            p = null;
            p1 = null;
            m1 = null;
        }
        return goodJob;
    }

    private boolean getStatsNats4(SiteFromAffProgram siteFromAffProgram) {
        boolean isDone = false;

        try {
            String xmlFile = getXMLFileNats4(siteFromAffProgram);
            XmlHandlerNats4DOM xmlHandlerNats4DOM = new XmlHandlerNats4DOM();
            xmlHandlerNats4DOM.getStatsNats4DOM(xmlFile, loginInfo, siteFromAffProgram);
            generalStatsList = xmlHandlerNats4DOM.getGeneralStatList();
            isDone = true;
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        } finally {
//            tempSourceFile = null;
        }

        return isDone;
    }

    private String getXMLFileNats4(SiteFromAffProgram siteFromAffProgram) throws IOException {
        // "/internal.php?page=dump&period=7&format=xml"
        LocalDate lastDateForSite = new Date(
                new DataFromDb().getLastDate(siteFromAffProgram, loginInfo).getTime()).toLocalDate().minusDays(1);
        String periodUri = "/internal.php?page=dump&period=8&start=" + lastDateForSite + "&end=" + DateWorks.getLastDayOfMonth() + "&filter_siteid=" + siteFromAffProgram.getExternalSiteId() + "&format=xml";
        httpget = new HttpGet(protocol + "://" + domain + periodUri);
        String tempSourceFile;
        HttpResponse responseGet;
        HttpEntity entityGet = null;
        responseGet = httpClient.execute(httpget);
        entityGet = responseGet.getEntity();
        tempSourceFile = new StreamToString().getStringFromInputStream(entityGet.getContent());
//        tempSourceFile = EntityUtils.toString(entityGet);
        tempSourceFile = tempSourceFile.replaceAll("<!--.+-->", "");
        httpget = null;
        if (entityGet != null) {
            try {
                EntityUtils.consume(entityGet);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return tempSourceFile;
    }

    private String getNatsUriFromLoginPage() {
        Element formElement = null;
        String uriFromRequest = "";
        String actionUri;
        Document doc;
        String uriForGetVersion;
        CloseableHttpResponse responseGet = null;
        HttpEntity entityGet = null;


        if (loginInfo.getAffiliateProgram().getUri() != null) {
            uriForGetVersion = loginInfo.getAffiliateProgram().getUri();
        } else {
            uriForGetVersion = "";
        }

        try {
            httpget = new HttpGet(protocol + "://" + domain + "/" + uriForGetVersion);
            responseGet = httpClient.execute(httpget);
            entityGet = responseGet.getEntity();
            doc = Jsoup.parse(new StreamToString().getStringFromInputStream(entityGet.getContent()));

//            doc = Jsoup.connect(fullPath)
////                    .cookies(set)
//                    .validateTLSCertificates(false)
//                    .timeout(50 * 1000)
//                    .followRedirects(true)
//                    .get();
            formElement = doc.select("form").first();
//            System.out.println(formElement);
            if (formElement != null) {
                actionUri = formElement.attr("action");
                if (actionUri.contains("members.php")) {
                    uriFromRequest = "members.php";
                } else if (actionUri.contains("internal.php")) {
                    uriFromRequest = "internal.php";
                } else if (actionUri.contains("external.php")) {
                    LOGGER.info("Wrong password or account is blocked.");
                } else {
                    LOGGER.warn(protocol + "://" + domain + " uriFromRequest for login do not detected");
                }
            } else {
                LOGGER.warn("Do not have Form for login");
            }
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.warn("Website " + protocol + "://" + domain + " is not available");
        } finally {
            if (responseGet != null) {
                try {
                    responseGet.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            entityGet = null;
            formElement = null;
            actionUri = null;
            doc = null;
        }
        return uriFromRequest;
    }

    private void logout() {
        try {
            Jsoup.connect(protocol + "://" + domain + "/" + "index.php?logout=1").validateTLSCertificates(false).get();
        } catch (IOException e) {
//            e.printStackTrace();
            System.err.println("IOException");
        }
    }

    private Map<String, String> getSitesList(String uriForGetSitesList, String attributeValueName,
                                             String attributeValueSiteId) throws IOException {

        Map<String, String> sitesList = new HashMap<>();
        Document doc;

        httpget = new HttpGet(protocol + "://" + domain + "/" + uriForGetSitesList);
        HttpResponse responseGet;
        HttpEntity entityGet = null;
        responseGet = httpClient.execute(httpget);
        entityGet = responseGet.getEntity();
        String sourceFile = new StreamToString().getStringFromInputStream(entityGet.getContent());

//        Connection connection = Jsoup.connect(protocol + "://" + domain + "/" + uriForGetSitesList)
//                .timeout(50 * 1000)
//                .validateTLSCertificates(false)
//                .cookies(response.cookies());
//        if (protocol.equalsIgnoreCase("https")) {
//            connection.validateTLSCertificates(false);
//        }
        doc = Jsoup.parse(sourceFile, "UTF-8");
        String siteIDloc;
        String siteNameloc;
        Elements content = doc.getElementsByAttributeValue(attributeValueName, attributeValueSiteId);
        Elements options = content.select("option");
        for (Element option : options) {
            siteIDloc = option.attr("value");
            siteNameloc = option.text();
            if (!siteIDloc.equalsIgnoreCase("") && !siteIDloc.equalsIgnoreCase("0")) {
                sitesList.put(siteIDloc, siteNameloc);
            }
        }

        doc = null;
        content = null;
        options = null;

        return sitesList;
    }

    @Transactional
    void getPaymentStats() {
        String paymentUri = "internal.php?page=payments";
//        httpGet.setURI(URI.create(protocol + "://" + domain + "/" + paymentUri));
        String tempSourceFile;
        Document doc = null;
        Element table;
        Elements trs;

        try {
            httpget = new HttpGet(protocol + "://" + domain + "/" + paymentUri);
            HttpResponse responseGet;
            HttpEntity entityGet = null;
            responseGet = httpClient.execute(httpget);
            entityGet = responseGet.getEntity();
            tempSourceFile = new StreamToString().getStringFromInputStream(entityGet.getContent());
//            doc = Jsoup.connect(protocol + "://" + domain + "/" + paymentUri)
//                    .cookies(response.cookies())
//                    .validateTLSCertificates(false)
//                    .timeout(50 * 1000)
//                    .get();
            doc = Jsoup.parse(tempSourceFile, "UTF-8");
            if (doc.getElementById("payment-table") != null) {
                table = doc.getElementById("payment-table");
                trs = table.select("tr[class*=\"data-row\"]");

                for (Element tr : trs) {
//                    LOGGER.info("==============");
                    Payment payment = new Payment();
                    payment.setAppUser(loginInfo.getAppUser());
                    payment.setAffiliateProgram(loginInfo.getAffiliateProgram());
                    payment.setLoginInfo(loginInfo);
                    for (Element td : tr.select("td")) {
                        if (td.className().contains("col_1")) {
                            payment.setStatus(td.text());
                        }
                        if (td.className().contains("col_2")) {
                            try {
                                payment.setPaymentSent(Date.valueOf(td.text()));
                            } catch (IllegalArgumentException e) {
                                // e.printStackTrace();
                            }
                        }
                        if (td.className().contains("col_4")) {
                            payment.setAmount(Integer.parseInt(td.text().replaceAll("\\D", "")));
                        }
                        if (td.className().contains("col_5")) {
                            payment.setMethod(td.text());
                        }
                        // LOGGER.info(td.text());
                    }
                    if (payment.getMethod() != null && payment.getStatus() != null
                            && payment.getPaymentSent() != null) {
                        // paymentDao.saveOrUpdate(payment);
                    } else if (payment.getPaymentSent() == null && payment.getAmount() != null) {
                        saveAccountBallance(payment);

                    }
                    payment = null;
                    // LOGGER.info("==============");
                }
            } else if (doc.select("table[class=\"payments_table\"]") != null && doc.select("table[class=\"payments_table\"]").size() > 0) {
                table = doc.select("table[class=\"payments_table\"]").first();
                trs = table.select("tr[class~=payments_tile_]");

                for (Element tr : trs) {
                    // LOGGER.info("==============");
                    Payment payment = new Payment();
                    payment.setAppUser(loginInfo.getAppUser());
                    payment.setAffiliateProgram(loginInfo.getAffiliateProgram());
                    payment.setLoginInfo(loginInfo);

                    for (int i = 0; i < tr.select("td").size(); i++) {
                        if (i == 0) {
                            payment.setStatus(tr.select("td").get(i).text());
                        }
                        if (i == 2) {
                            try {
                                payment.setPaymentSent(Date.valueOf(tr.select("td").get(i).text()));
                            } catch (IllegalArgumentException e) {
                                // e.printStackTrace();
                            }
                        }
                        if (i == 3) {
                            payment.setAmount(Integer.parseInt((tr.select("td").get(i).text()).replaceAll("\\D", "")));
                        }
                        if (i == 4) {
                            payment.setMethod(tr.select("td").get(i).text());
                        }
                    }
                    if (payment.getMethod() != null && payment.getStatus() != null
                            && payment.getPaymentSent() != null) {
                    } else if (payment.getPaymentSent() == null && tr.select("td").size() > 5) {
                        saveAccountBallance(payment);
                    }
                    payment = null;
                }
            }
        } catch (IOException e) {
            // LOGGER.warn("Сайт " + urlForLogin + " не доступен!!!");
        } finally {
            tempSourceFile = null;
            doc = null;
        }
    }


    private void saveAccountBallance(Payment payment) {
        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setAmount(payment.getAmount().longValue());
        accountBalance.setDateAmount(new java.util.Date());
        accountBalance.setLoginInfo(loginInfo);
        accountBalanceRepository.save(accountBalance);//TODO сделать сервисом для всех
    }

    @Transactional
    void saveAllStats(List<GeneralStat> statList) {
        for (GeneralStat g : statList) {
            GeneralStat generalStat = generalStatRepository.findByStatsDateAndAppUserAndSiteFromAffProgramAndLoginInfoAndAffiliateProgram(g.getStatsDate(), g.getAppUser(), g.getSiteFromAffProgram(), g.getLoginInfo(), g.getAffiliateProgram());
            if (generalStat == null) {
                generalStatRepository.save(g);
            }
        }
    }

    private boolean autorizationNats(String uri) {
//        this.httpClient = HttpClients.createDefault();
        try {
            this.httpClient = HttpClients.custom()
                    .setSSLSocketFactory(new SSLConnectionSocketFactory(SSLContexts.custom()
                                    .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                                    .build()
                            )
                    ).build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            e.printStackTrace();
        }
        String login = loginInfo.getLogin();
        String password = new AES().decrypt(loginInfo.getPassword(), loginInfo.getAppUser().getPassword());
        //Hard stop connection
        int hardTimeout = 300; // seconds
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (httpget != null) {
                    httpget.abort();
                }
            }
        };
        new Timer(true).schedule(task, hardTimeout * 1000);

        String fullURL;
        HttpPost httpPost;
        List<NameValuePair> nvps = new ArrayList<>();
        String formFieldPassword = "pass";
        String formFieldUser = "user";
        httpPost = new HttpPost(protocol + "://" + domain + "/" + uri);
        nvps.add(new BasicNameValuePair(formFieldUser, login));
        nvps.add(new BasicNameValuePair(formFieldPassword, password));
        Header[] headers = null;
        boolean loginStatus = false;

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            responsePost = httpClient.execute(httpPost);
            HttpEntity entityGet = responsePost.getEntity();
            String tempSourceFile = new StreamToString().getStringFromInputStream(entityGet.getContent());
            headers = responsePost.getHeaders("Location");

        } catch (IOException e) {
            e.printStackTrace();
        }

        fullURL = null;
        httpPost = null;
        nvps = null;

        if ((headers.length > 0) && (headers[0].toString().contains("internal.php") || headers[0].toString().contains("members.php") || headers[0].toString().contains("admin_overview.php"))) {
            loginStatus = true;
        } else {
            for (Header header : headers) {
                System.err.println("Login false");
                System.err.println(header);
            }
        }
        return loginStatus;
    }
}
