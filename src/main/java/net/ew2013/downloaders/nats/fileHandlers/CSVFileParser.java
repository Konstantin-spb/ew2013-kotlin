package net.ew2013.downloaders.nats.fileHandlers;

//import au.com.bytecode.opencsv.CSVReader;
import net.ew2013.model.GeneralStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CSVFileParser {

//    @Autowired
//    GeneralStatsService statsService;

    private List<GeneralStat> generalStatsList = new ArrayList<>();
    private LoginInfo loginInfo;
    private SiteFromAffProgram siteFromAffProgram;
    private byte rawHitsIndex = 0;
    private byte uniqueHitsIndex = 0;
    private byte initialCountIndex = 0;
    private byte trialCountIndex = 0;
    private byte rebillCountIndex = 0;
    private byte creditCountIndex = 0;
    private byte chargebackCountIndex = 0;
    private byte initialPayoutIndex = 0;
    private byte trialPayoutIndex = 0;
    private byte rebillPayoutIndex = 0;
    private byte creditPayoutIndex = 0;
    private byte chargebackPayoutIndex = 0;
    private byte totalReferralPayoutIndex = 0;
    private byte dateIndex = 0;

    private List<String[]> myEntries;

    public Boolean getParsingIsDone() {
        return parsingIsDone;
    }

    private Boolean parsingIsDone = false;

    public CSVFileParser(String csvFileByLine, LoginInfo loginInfo, SiteFromAffProgram siteFromAffProgram) throws IOException, SQLException {

//        this.loginInfo = loginInfo;
//        this.siteFromAffProgram = siteFromAffProgram;
//
//        List<String> arrayList = new ArrayList<String>();
//        Reader targetReader = new StringReader(csvFileByLine.replaceAll(";", ""));
//        CSVReader reader = new CSVReader(targetReader, ',', '\"');
////        CSVReader reader = new CSVReader(new FileReader("/home/konstantin/Temp/deluxecoin.html"), ',', '\"', 1);
//        myEntries = reader.readAll();
//        Collections.addAll(arrayList, myEntries.get(0));
//
//        rawHitsIndex = (byte) arrayList.indexOf("raw_hits");
//        uniqueHitsIndex = (byte) arrayList.indexOf("unique_hits");
//        initialCountIndex = (byte) arrayList.indexOf("initial_count");
//        trialCountIndex = (byte) arrayList.indexOf("trial_count");
//        rebillCountIndex = (byte) arrayList.indexOf("rebill_count");
//        creditCountIndex = (byte) arrayList.indexOf("credit_count");
//        chargebackCountIndex = (byte) arrayList.indexOf("chargeback_count");
//        initialPayoutIndex = (byte) arrayList.indexOf("initial_payout");
//        trialPayoutIndex = (byte) arrayList.indexOf("trial_payout");
//        rebillPayoutIndex = (byte) arrayList.indexOf("rebill_payout");
//        creditPayoutIndex = (byte) arrayList.indexOf("credit_payout");
//        chargebackPayoutIndex = (byte) arrayList.indexOf("chargeback_payout");
//        totalReferralPayoutIndex = (byte) arrayList.indexOf("total_referral_payout");
//        dateIndex = (byte) arrayList.indexOf("name");
//
//        try {
//            GetDataFromRow();
////            parsingIsDone = true;
//        } catch (ArrayIndexOutOfBoundsException e) {
//            System.err.println("Партнерка Nats - CSV-ошибка парсинга");
//        } finally {
//            arrayList = null;
//            targetReader = null;
//            myEntries = null;
//            reader.close();
//        }
    }

    private void GetDataFromRow() throws SQLException {

        String[] row;

       long rawHits;
       long uniqueHits;
       long initialCount;
       long trialCount;
       long rebillCount;
       long creditCount;
       long chargebackCount;
       long initialPayout;
       long trialPayout;
       long rebillPayout;
       long creditPayout;
       long chargebackPayout;
       long totalReferralPayout;
        Date date = null;

       long totalMoney;
       long signupsMoney;

        for (int i = 1; i < myEntries.size() - 2; i++) {

            row = myEntries.get(i);

            try {
                rawHits = Integer.parseInt(row[rawHitsIndex]);
            } catch (NumberFormatException e) {
                rawHits = 0;
            }
            try {
                uniqueHits = Integer.parseInt(row[uniqueHitsIndex]);
            } catch (NumberFormatException e) {
                uniqueHits = 0;
            }
            try {
                initialCount = Integer.parseInt(row[initialCountIndex]);
            } catch (NumberFormatException e) {
                initialCount = 0;
            }
            try {
                trialCount = Integer.parseInt(row[trialCountIndex]);
            } catch (NumberFormatException e) {
                trialCount = 0;
            }
            try {
                rebillCount = Integer.parseInt(row[rebillCountIndex]);
            } catch (NumberFormatException e) {
                rebillCount = 0;
            }
            try {
                creditCount = Integer.parseInt(row[creditCountIndex]);
            } catch (NumberFormatException e) {
                creditCount = 0;
            }
            try {
                chargebackCount = Integer.parseInt(row[chargebackCountIndex]);
            } catch (NumberFormatException e) {
                chargebackCount = 0;
            }
            try {
                initialPayout = Integer.parseInt(row[initialPayoutIndex]);
            } catch (NumberFormatException e) {
                initialPayout = 0;
            }
            try {
                trialPayout = Integer.parseInt(row[trialPayoutIndex]);
            } catch (NumberFormatException e) {
                trialPayout = 0;
            }
            try {
                rebillPayout = Integer.parseInt(row[rebillPayoutIndex]);
            } catch (NumberFormatException e) {
                rebillPayout = 0;
            }
            try {
                creditPayout = Integer.parseInt(row[creditPayoutIndex].replaceAll("\\D", ""));
//                if (creditPayout > 0) {
//                    creditPayout = creditPayout * -1;
//                }
            } catch (NumberFormatException e) {
                creditPayout = 0;
            }
            try {
                chargebackPayout = Integer.parseInt(row[chargebackPayoutIndex].replaceAll("\\D", ""));
//                if (chargebackPayout > 0) {
//                    chargebackPayout = chargebackPayout * -1;
//                }
            } catch (NumberFormatException e) {
                chargebackPayout = 0;
            }
            try {
                totalReferralPayout = Integer.parseInt(row[totalReferralPayoutIndex]);
            } catch (NumberFormatException e) {
                totalReferralPayout = 0;
            }
            try {
//                date = (new java.text.SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date((long) Integer.parseInt(row[dateIndex]) * 1000)));
                date = new Date((long) Integer.parseInt(row[dateIndex]) * 1000);
            } catch (NumberFormatException e) {
                System.out.println("Дата должна быть всегда");
            }

            //**************QueryToDB_Start*************************
            signupsMoney = trialPayout + initialPayout;
            totalMoney = signupsMoney + rebillPayout - chargebackPayout + creditPayout + totalReferralPayout;

            GeneralStat generalStats = new GeneralStat();

            if (rawHits != 0 || uniqueHits != 0 || trialCount != 0 || initialCount == 0
                    && signupsMoney != 0 || rebillCount != 0 || rebillPayout == 0 
                    && chargebackCount != 0 || creditCount != 0 || chargebackPayout != 0 || creditPayout == 0
                    && totalMoney != 0) {

                generalStats.setRawClick(rawHits);
                generalStats.setUniqueClicks(uniqueHits);
                generalStats.setSignupCount(trialCount + initialCount);
                generalStats.setSignupMoney(signupsMoney);
                generalStats.setRebillCount(rebillCount);
                generalStats.setRebillMoney(rebillPayout);
                generalStats.setRefundChargebackCount(chargebackCount + creditCount);
                generalStats.setRefundChargebackMoney(chargebackPayout + creditPayout);
                generalStats.setTotalMoney(totalMoney);
                generalStats.setStatsDate(date);
                generalStats.setSiteFromAffProgram(siteFromAffProgram);
                generalStats.setAffiliateProgram(loginInfo.getAffiliateProgram());
                generalStatsList.add(generalStats);
                generalStats.setLoginInfo(loginInfo);
                generalStats.setAppUser(loginInfo.getAppUser());
                generalStatsList.add(generalStats);
            }
        }
    }

    public List<GeneralStat> getGeneralStatList() {
        return generalStatsList;
    }
}
