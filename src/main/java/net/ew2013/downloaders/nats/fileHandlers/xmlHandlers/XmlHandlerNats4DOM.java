package net.ew2013.downloaders.nats.fileHandlers.xmlHandlers;


import net.ew2013.model.GeneralStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class XmlHandlerNats4DOM {

    List<GeneralStat> generalStatList = new ArrayList<>();
//    LoginInfo loginInfo;
//    SiteFromAps siteFromAffProgram;

    public XmlHandlerNats4DOM() {
//        System.out.println("На помощь пришёл XML парсеp...");
    }

    public boolean getStatsNats4DOM(String xmlFile, LoginInfo loginInfo, SiteFromAffProgram siteFromAffProgram) throws SQLException {

        boolean allIsDone = false;

//        Date date = null;
        String unixDateFormat = null;
        Date statsDate = null;

//        File fXmlFile = new File("/home/konstantin/Temp/serious-cash.html");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        Document doc = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(xmlFile)));
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
            return false;
        }
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("item");

//        org.jsoup.nodes.Document document = Jsoup.parse(xmlFile);
//        Elements elements = document.getElementsByTag("item");
//        date = (new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date((long) Integer.parseInt(unixDateFormat) * 1000)));
//        for (org.jsoup.nodes.Element e: elements) {
//            statsDate = new Date(Long.parseLong(e.attr("id")) * 1000);

//            long rawHits = 0;
//            long uniqueHits = 0;
//            long initialCount = 0;
//            long trialCount = 0;
//            long rebillCount = 0;
//            long creditCount = 0;
//            long chargebackCount = 0;
//            long initialPayout = 0;
//            long trialPayout = 0;
//            long rebillPayout = 0;
//            long creditPayout = 0;
//            long chargebackPayout = 0;
//            long totalReferralPayout = 0;
//
//            long signupsMoney = 0;
//            long totalMoney = 0;

//            try {
//                rawHits = Integer.parseInt(e.getElementsByTag("raw_hits").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }
//            try {
//                uniqueHits = Integer.parseInt(e.getElementsByTag("unique_hits").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }
//            try {
//                initialCount = Integer.parseInt(e.getElementsByTag("initial_count").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }
//            try {
//                trialCount = Integer.parseInt(e.getElementsByTag("trial_count").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }
//            try {
//                rebillCount = Integer.parseInt(e.getElementsByTag("total_rebill_count").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }
//            try {
//                creditCount = Integer.parseInt(e.getElementsByTag("credit_count").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }
//            try {
//                chargebackCount = Integer.parseInt(e.getElementsByTag("chargeback_count").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }
//            try {
//                initialPayout = Integer.parseInt(e.getElementsByTag("initial_payout").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }
//            try {
//                trialPayout = Integer.parseInt(e.getElementsByTag("trial_payout").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }
//            try {
//                rebillPayout = Integer.parseInt(e.getElementsByTag("total_rebill_payout").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }
//            try {
//                creditPayout = Integer.parseInt(e.getElementsByTag("credit_payout").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }
//            try {
//                chargebackPayout = Integer.parseInt(e.getElementsByTag("chargeback_payout").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }
//            try {
//                totalReferralPayout = Integer.parseInt(e.getElementsByTag("total_referral_payout").first().text());
//            } catch (NullPointerException | NumberFormatException er) {
//            }

//            GeneralStat generalStat = new GeneralStat();
//
//            if (rawHits != 0 || uniqueHits != 0 || initialCount != 0 || trialCount != 0 || rebillCount != 0 || creditCount != 0
//                    || chargebackCount != 0 || initialPayout != 0 || trialPayout != 0 || rebillPayout != 0 ||
//                    creditPayout != 0 || chargebackPayout != 0 || totalReferralPayout != 0 || signupsMoney != 0 ||
//                    totalMoney != 0) {
//
//                signupsMoney = trialPayout + initialPayout;
//                totalMoney = signupsMoney + rebillPayout + chargebackPayout + creditPayout + totalReferralPayout;

//**************QueryToDB_Start*************************
//                generalStat.setRawClick(rawHits);
//                generalStat.setUniqueClicks(uniqueHits);
//                generalStat.setSignupCount(trialCount + initialCount);
//                generalStat.setSignupMoney(signupsMoney);
//                generalStat.setRebillCount(rebillCount);
//                generalStat.setRebillMoney(rebillPayout);
//                generalStat.setRefundChargebackCount(chargebackCount + creditCount);
//                generalStat.setRefundChargebackMoney(chargebackPayout + creditPayout);
//                generalStat.setTotalMoney(totalMoney);
//                generalStat.setStatsDate(statsDate);
//                generalStat.setSiteFromAffProgram(siteFromAffProgram);
//                generalStat.setAffiliateProgram(loginInfo.getAffiliateProgram());
//                generalStatList.add(generalStat);
//                generalStat.setLoginInfo(loginInfo);
//                generalStat.setUsers(loginInfo.getUsers());
//                generalStatList.add(generalStat);
//**************QueryToDB_End***************************

//                allIsDone = true;

//            System.out.println();
//            }

//        System.out.println("----------------------------");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            Element eElement = (Element) nNode;
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                unixDateFormat = eElement.getAttribute("id");

                long rawHits = 0;
            long uniqueHits = 0;
            long initialCount = 0;
            long trialCount = 0;
            long rebillCount = 0;
            long creditCount = 0;
            long chargebackCount = 0;
            long initialPayout = 0;
            long trialPayout = 0;
            long rebillPayout = 0;
            long creditPayout = 0;
            long chargebackPayout = 0;
            long totalReferralPayout = 0;

            long signupsMoney = 0;
            long totalMoney = 0;

                statsDate = new Date(Long.parseLong(unixDateFormat) * 1000);
                try {
                    rawHits = Integer.parseInt(eElement.getElementsByTagName("raw_hits").item(0).getTextContent().trim());
//                    System.out.println(rawHits);
                } catch (NullPointerException | NumberFormatException e) {
//                    e.printStackTrace();
                }
                try {
                    uniqueHits = Integer.parseInt(eElement.getElementsByTagName("unique_hits").item(0).getTextContent().trim());
                } catch (NullPointerException | NumberFormatException e) {
                }
                try {
                    initialCount = Integer.parseInt(eElement.getElementsByTagName("initial_count").item(0).getTextContent().trim());
                } catch (NullPointerException | NumberFormatException e) {
                }
                try {
                    trialCount = Integer.parseInt(eElement.getElementsByTagName("trial_count").item(0).getTextContent().trim());
                } catch (NullPointerException | NumberFormatException e) {
                }
                try {
                    rebillCount = Integer.parseInt(eElement.getElementsByTagName("total_rebill_count").item(0).getTextContent().trim());
//                    if (rebillCount != 0) {
//                        System.out.println(rebillCount);
//                    }
                } catch (NullPointerException | NumberFormatException e) {
                }
                try {
                    creditCount = Integer.parseInt(eElement.getElementsByTagName("credit_count").item(0).getTextContent().trim());
                } catch (NullPointerException | NumberFormatException e) {
                }
                try {
                    chargebackCount = Integer.parseInt(eElement.getElementsByTagName("chargeback_count").item(0).getTextContent().trim());
                } catch (NullPointerException | NumberFormatException e) {
                }
                try {
                    initialPayout = Integer.parseInt(eElement.getElementsByTagName("initial_payout").item(0).getTextContent().trim());
                } catch (NullPointerException | NumberFormatException e) {
                }
                try {
                    trialPayout = Integer.parseInt(eElement.getElementsByTagName("trial_payout").item(0).getTextContent().trim());
                } catch (NullPointerException | NumberFormatException e) {
                }
                try {
                    rebillPayout = Integer.parseInt(eElement.getElementsByTagName("total_rebill_payout").item(0).getTextContent().trim());
                } catch (NullPointerException | NumberFormatException e) {
                }
                try {
                    creditPayout = Integer.parseInt(eElement.getElementsByTagName("credit_payout").item(0).getTextContent().trim());
                } catch (NullPointerException | NumberFormatException e) {
                }
                try {
                    chargebackPayout = Integer.parseInt(eElement.getElementsByTagName("chargeback_payout").item(0).getTextContent().trim());
                } catch (NullPointerException | NumberFormatException e) {
                }
                try {
                    totalReferralPayout = Integer.parseInt(eElement.getElementsByTagName("total_referral_payout").item(0).getTextContent().trim());
                } catch (NullPointerException | NumberFormatException e) {
                }

                GeneralStat generalStat = new GeneralStat();

                if (rawHits != 0 || uniqueHits != 0 || initialCount != 0 || trialCount != 0 || rebillCount != 0 || creditCount != 0
                        || chargebackCount != 0 || initialPayout != 0 || trialPayout != 0 || rebillPayout != 0 ||
                        creditPayout != 0 || chargebackPayout != 0 || totalReferralPayout != 0 || signupsMoney != 0 ||
                        totalMoney != 0) {

                    signupsMoney = trialPayout + initialPayout;
                    totalMoney = signupsMoney + rebillPayout + chargebackPayout + creditPayout + totalReferralPayout;

//**************QueryToDB_Start*************************
                    generalStat.setRawClick(rawHits);
                    generalStat.setUniqueClicks(uniqueHits);
                    generalStat.setSignupCount(trialCount + initialCount);
                    generalStat.setSignupMoney(signupsMoney);
                    generalStat.setRebillCount(rebillCount);
                    generalStat.setRebillMoney(rebillPayout);
                    generalStat.setRefundChargebackCount(chargebackCount + creditCount);
                    generalStat.setRefundChargebackMoney(chargebackPayout + creditPayout);
                    generalStat.setTotalMoney(totalMoney);
                    generalStat.setStatsDate(statsDate);
                    generalStat.setSiteFromAffProgram(siteFromAffProgram);
                    generalStat.setAffiliateProgram(loginInfo.getAffiliateProgram());
                    generalStat.setLoginInfo(loginInfo);
                    generalStat.setAppUser(loginInfo.getAppUser());
                    generalStatList.add(generalStat);
//**************QueryToDB_End***************************

//                    rawHits = 0;
//                    uniqueHits = 0;
//                    initialCount = 0;
//                    trialCount = 0;
//                    rebillCount = 0;
//                    creditCount = 0;
//                    chargebackCount = 0;
//                    initialPayout = 0;
//                    trialPayout = 0;
//                    rebillPayout = 0;
//                    creditPayout = 0;
//                    chargebackPayout = 0;
//                    totalReferralPayout = 0;
//
//                    signupsMoney = 0;
//                    totalMoney = 0;
                    allIsDone = true;
                }
            }
        }
//        System.out.println(siteFromAffProgram.getSiteName() + " Добавлен");
        xmlFile = null;
        dbFactory = null;
        dBuilder = null;
        doc = null;
        return allIsDone;
    }

    public List<GeneralStat> getGeneralStatList() {
        return generalStatList;
    }

    public void setGeneralStatList(List<GeneralStat> generalStatList) {
        this.generalStatList = generalStatList;
    }
}