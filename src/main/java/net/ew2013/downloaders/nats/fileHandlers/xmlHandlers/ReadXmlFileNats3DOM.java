package net.ew2013.downloaders.nats.fileHandlers.xmlHandlers;

import net.ew2013.model.GeneralStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by konstantin on 07.09.14.
 */
public class ReadXmlFileNats3DOM {

    List<GeneralStat> generalStatsList = new ArrayList<>();

    private StringBuilder errors;

    public boolean getStatsNats3DOM(String xmlFile, LoginInfo loginInfo, SiteFromAffProgram siteFromAffPrograms) {

        this.errors = errors;

//        String[] stringParams = new String[2];
//        int[] intParams = new int[10];
        boolean allIsDone = false;

        String statsDate = null;
        long raw = 0;
        long unq = 0;
        long signups = 0;
        long signups_earned = 0;
        long rebills = 0;
        long rebills_earned = 0;
        long credits = 0;
        long credits_loss = 0;
        long chargebacks = 0;
        long chargebacks_loss = 0;
        long earned = 0;
        long totalMoney = 0;

//        File fXmlFile = new File("/home/konstantin/Temp/serious-cash.html");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        Document doc = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(xmlFile)));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            errors.append(e);
            e.printStackTrace();
        }

        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("by");

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) nNode;

                try {
                    statsDate = new SimpleDateFormat("yyyy-MM-dd").format((new SimpleDateFormat("MM/dd/yyyy").parse(eElement.getElementsByTagName("row").item(0).getTextContent().trim())));
                } catch (ParseException e) {
                    errors.append(e);
                    e.printStackTrace();
                }
                raw = Integer.parseInt(eElement.getElementsByTagName("raw").item(0).getTextContent().trim());
                unq = Integer.parseInt(eElement.getElementsByTagName("unq").item(0).getTextContent().trim());
                signups = Integer.parseInt(eElement.getElementsByTagName("signups").item(0).getTextContent().trim());
                signups_earned = (int) (Double.parseDouble(eElement.getElementsByTagName("signups_earned").item(0).getTextContent().trim()) * 100);
                rebills = Integer.parseInt(eElement.getElementsByTagName("rebills").item(0).getTextContent().trim());
                rebills_earned = (int) (Double.parseDouble(eElement.getElementsByTagName("rebills_earned").item(0).getTextContent().trim()) * 100);
                credits = Integer.parseInt(eElement.getElementsByTagName("credits").item(0).getTextContent().trim());
                credits_loss = (int) ((Double.parseDouble(eElement.getElementsByTagName("credits_loss").item(0).getTextContent().trim())) * 100);
                if (credits_loss < 0) {
                    credits_loss = credits_loss * -1;
                }
                chargebacks = Integer.parseInt(eElement.getElementsByTagName("chargebacks").item(0).getTextContent().trim());
                chargebacks_loss = (int) ((Double.parseDouble(eElement.getElementsByTagName("chargebacks_loss").item(0).getTextContent().trim())) * 100);
                if (chargebacks_loss < 0) {
                    chargebacks_loss = chargebacks_loss * -1;
                }
                earned = (int) ((Double.parseDouble(eElement.getElementsByTagName("earned").item(0).getTextContent().trim())) * 100);
                totalMoney = earned;
                //**************QueryToDB_Start*************************
                GeneralStat generalStat = new GeneralStat();

                generalStat.setRawClick(raw);
                generalStat.setUniqueClicks(unq);
                generalStat.setSignupCount(signups);
                generalStat.setSignupMoney(signups_earned);
                generalStat.setRebillCount(rebills);
                generalStat.setRebillMoney(rebills_earned);
                generalStat.setRefundChargebackCount(chargebacks + credits);
                generalStat.setRefundChargebackMoney(chargebacks_loss + credits_loss);
                generalStat.setTotalMoney(totalMoney);
                generalStat.setStatsDate(Date.valueOf(statsDate));
                generalStat.setSiteFromAffProgram(siteFromAffPrograms);
                generalStat.setAffiliateProgram(loginInfo.getAffiliateProgram());
                generalStatsList.add(generalStat);
                generalStat.setLoginInfo(loginInfo);
                generalStat.setAppUser(loginInfo.getAppUser());
                generalStatsList.add(generalStat);
                generalStat.setStatsDate(Date.valueOf(statsDate));

                allIsDone = true;
            }

        }
//        System.out.println(siteName + " Добавлен");
        dbFactory = null;
        dBuilder = null;
        doc = null;
        xmlFile = null;
        return allIsDone;
    }

    public List<GeneralStat> getGeneralStatsList() {
        return generalStatsList;
    }
}
