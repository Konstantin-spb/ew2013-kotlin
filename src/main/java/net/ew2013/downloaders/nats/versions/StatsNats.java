package net.ew2013.downloaders.nats.versions;

import net.ew2013.utils.dateUtils.DateWorks;
import net.ew2013.downloaders.nats.AutorizationNats;
import net.ew2013.downloaders.nats.fileHandlers.xmlHandlers.ReadXmlFileNats3DOM;
import net.ew2013.downloaders.nats.fileHandlers.xmlHandlers.XmlHandlerNats4DOM;
import net.ew2013.model.GeneralStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.ServiceCollectionRunsThreads;
import net.ew2013.model.SiteFromAffProgram;
import net.ew2013.repository.*;
import net.ew2013.service.LastdateDownloadStatService;
import net.ew2013.service.ServiceCollectionRunsThreadsService;
import net.ew2013.utils.DataFromDb;
import net.ew2013.utils.PauseSleep;
import net.ew2013.utils.StreamToString;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by konstantin on 14.10.15.
 */
@Component
@Scope(value = "prototype")
public class StatsNats {
    private static final Logger LOGGER = LoggerFactory.getLogger(StatsNats.class);

    @Autowired
    ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Autowired
    StatusRepository statusRepository;
    @Autowired
    GeneralStatRepository generalStatRepository;
    @Autowired
    SiteFromAffProgramRepository siteFromAffProgramRepository;
    @Autowired
    LastdateDownloadStatRepository lastdateDownloadStatRepository;
    @Autowired
    LastdateDownloadStatService lastdateDownloadStatService;
    @Autowired
    ServiceCollectionRunsThreadsRepository serviceCollectionRunsThreadsRepository;
    @Autowired
    ServiceCollectionRunsThreadsService serviceCollectionRunsThreadsService;

    private List<GeneralStat> generalStatsList;
    private CloseableHttpClient httpClient;
    private HttpGet httpget;
    private String periodUri;
    private LocalDate lastDate;
    private StringBuilder errors;
    private LoginInfo loginInfo;
    private SiteFromAffProgram siteFromAffProgram;
    private String tempSourceFile;
    private String xmlData;
    private String uriNatsVersion;
    private String protocol;
    private String domain;

    public void setParams(LoginInfo loginInfo, SiteFromAffProgram siteFromAffProgram, String uriNatsVersion) {
        this.loginInfo = loginInfo;
        this.siteFromAffProgram = siteFromAffProgram;
        this.lastDate = new java.sql.Date(
                new DataFromDb().getLastDate(siteFromAffProgram, loginInfo).getTime()).toLocalDate().minusDays(1);
//		siteFromAffProgram.setStatus("progressing");
        this.protocol = loginInfo.getAffiliateProgram().getProtocol().getProtocolName();
        this.domain = loginInfo.getAffiliateProgram().getDomain().getDomainName();
        this.periodUri = "/stats.php?xml=1&period=8&period_start=" + lastDate + "&period_end=" + DateWorks.getLastDayOfMonth() + "&siteid=" + this.siteFromAffProgram.getExternalSiteId();
        this.httpget = new HttpGet(protocol + "://" + domain + periodUri);
        errors = new StringBuilder("");
        this.uriNatsVersion = uriNatsVersion;
    }

    private boolean getStatsNats3() {

        String tempSourceFile;
        boolean goodJob = false;

        CloseableHttpResponse responseGet = null;
        HttpEntity entityGet;
        Matcher m;
        Pattern p;
        Pattern p1;
        Matcher m1;
        try {
            responseGet = httpClient.execute(httpget);
            entityGet = responseGet.getEntity();
            tempSourceFile = EntityUtils.toString(entityGet);
            p = Pattern.compile("<statistics>([\\S\\s]+?)</statistics>");
            m = p.matcher(tempSourceFile);
            if (m.find()) {
                xmlData = m.group();
//				LOGGER.info(site.getValue() + "(" + site.getKey() + ")" + xmlData);
                goodJob = true;

                if (xmlData.matches("<statistics>[\\s]+<ratiofield>unq</ratiofield>[\\s]+</statistics>")) {
//					goodJob = false;
//					LOGGER.info(siteFromAffProgram.getProgramName() + "(" + siteFromAffProgram.getExternalSiteId() + ") - нет статистики");
                }
                ReadXmlFileNats3DOM readXmlFileNats3DOM = new ReadXmlFileNats3DOM();
                readXmlFileNats3DOM.getStatsNats3DOM(xmlData, loginInfo, siteFromAffProgram);
                generalStatsList = readXmlFileNats3DOM.getGeneralStatsList();
            } else if (xmlData == null) {
                p1 = Pattern.compile("(<by>[\\S\\s]*</by)>");
                m1 = p1.matcher(tempSourceFile);
//				LOGGER.info(site.getValue() + "(" + site.getKey() + ")" + xmlData);
                goodJob = true;

                if (m1.find()) {
                    xmlData = "<statistics>\n" + "\t<ratiofield>unq</ratiofield>" + m1.group() + "\n</statistics>";
//					LOGGER.info(siteFromAffProgram.getProgramName() + "(" + siteFromAffProgram.getExternalSiteId() + ") - статистика получена");
                }
                ReadXmlFileNats3DOM readXmlFileNats3DOM = new ReadXmlFileNats3DOM();
                readXmlFileNats3DOM.getStatsNats3DOM(xmlData, loginInfo, siteFromAffProgram);
                generalStatsList = readXmlFileNats3DOM.getGeneralStatsList();
            }
            tempSourceFile = "";
        } catch (IOException e) {
            errors.append(e);
//            e.printStackTrace();
        } finally {
//			httpget = null;
            tempSourceFile = null;

            try {
                if (responseGet != null) {
                    responseGet.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            entityGet = null;
            m = null;
            p = null;
            p1 = null;
            m1 = null;
        }
        return goodJob;
    }

    private boolean getStatsNats4() throws SQLException, IOException {
        boolean isDone;
        periodUri = "/internal.php?page=dump&period=8&start=" + lastDate + "&end=" + DateWorks.getLastDayOfMonth()
                + "&filter_siteid=" + siteFromAffProgram.getExternalSiteId() + "&format=csv";
        tempSourceFile = null;
        periodUri = "/internal.php?page=dump&period=8&start=" + lastDate + "&end=" + DateWorks.getLastDayOfMonth() + "&filter_siteid=" + siteFromAffProgram.getExternalSiteId() + "&format=xml";
        String xmlFile = getXMLFileNats4();
        XmlHandlerNats4DOM xmlHandlerNats4DOM = new XmlHandlerNats4DOM();
        xmlHandlerNats4DOM.getStatsNats4DOM(xmlFile, loginInfo, siteFromAffProgram);
        generalStatsList = xmlHandlerNats4DOM.getGeneralStatList();
        isDone = true;
        tempSourceFile = null;

        return isDone;
    }

    private String getXMLFileNats4() throws IOException {
        // "/internal.php?page=dump&period=7&format=xml"
        httpget = new HttpGet(protocol + "://" + domain + periodUri);
        tempSourceFile = null;
        HttpResponse responseGet;
        HttpEntity entityGet = null;
        responseGet = httpClient.execute(httpget);
        entityGet = responseGet.getEntity();
        tempSourceFile = new StreamToString().getStringFromInputStream(entityGet.getContent());
//        tempSourceFile = EntityUtils.toString(entityGet);
        tempSourceFile = tempSourceFile.replaceAll("<!--.+-->", "");
        httpget = null;
        if (entityGet != null) {
            try {
                EntityUtils.consume(entityGet);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return tempSourceFile;
    }

    private void logout() {
        try {
            httpClient.execute(new HttpGet(protocol + "://" + domain + "/index.php?logout=1"));
        } catch (IOException ignored) {
        }
    }

    @Async("threadPoolTaskExecutor")
    public void run() {
        ServiceCollectionRunsThreads serviceCollectionRunsThreads = null;
        Thread.currentThread().setName(loginInfo.getAppUser().getId() + "_" + loginInfo.getLogin() + "_" + loginInfo.getAffiliateProgram().getName() + "_" + siteFromAffProgram.getSiteName());

        try {
            serviceCollectionRunsThreads = serviceCollectionRunsThreadsService.setServiceCollectionRunsThreads(siteFromAffProgram, loginInfo);
            PauseSleep.pause(1, 15);
            boolean isGood = false;

            //Hard stop connection
            int hardTimeout = 180; // seconds
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    if (httpget != null) {
                        httpget.abort();
                    }
                }
            };
            new Timer(true).schedule(task, hardTimeout * 1000);

            for (int i = 0; ; i++) {
                if (i >= 5) {
//                    LOGGER.info("Статистика НЕ скачана - " + siteFromAffProgram.getSiteName() + " -> " + siteFromAffProgram.getAffiliateProgram().getName());
                    logout();
                    System.out.println(loginInfo.getAffiliateProgram().getName() + "---" + siteFromAffProgram.getSiteName() + " ---> downloading error");
                    break;
                }

                AutorizationNats autorizationNats = new AutorizationNats(loginInfo, uriNatsVersion);
                if (autorizationNats.tryToLoginToNats()) {
                    if (i > 0) {
//                        LOGGER.info("Повторная авторизация номер " + (i + 1) + " выполнена - "
//                                + loginInfo.getAffiliateProgram().getName() + " -> " + siteFromAffProgram.getSiteName());
                    }
                    this.httpClient = autorizationNats.getHttpClient();
                    if (uriNatsVersion.equalsIgnoreCase("internal.php")) {
                        isGood = getStatsNats4();
                    } else if (uriNatsVersion.equalsIgnoreCase("members.php")) {
                        isGood = getStatsNats3();
                    }
                    if (errors.toString().isEmpty() && isGood) {
                        logout();
                        saveAllStats(generalStatsList);
                        lastdateDownloadStatService.saveLastDateWithStatus(loginInfo.getAppUser(), loginInfo, siteFromAffProgram, "DONE", new Date());
//                        finishPeriodDate = new Date();
//                        LOGGER.info(loginInfo.getAffiliateProgram().getName() + " -> " + siteFromAffProgram.getSiteName() + " - скачал статистику за " + ((finishPeriodDate.getTime() - startPeriodData.getTime()) / 1000F) + " sec");
                        break;
                    }
                } else {
                    System.out.println(loginInfo.getAffiliateProgram().getName() + "---" + siteFromAffProgram.getSiteName() + " ---> downloading error");
                }
                PauseSleep.pause(1, 5);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(loginInfo.getAffiliateProgram().getName() + "---" + siteFromAffProgram.getSiteName() + " ---> downloading error");
        } finally {
            serviceCollectionRunsThreadsRepository.delete(serviceCollectionRunsThreads);
//            if (threadPoolTaskExecutor.getActiveCount() <= 1) {
                LOGGER.warn(String.valueOf(threadPoolTaskExecutor.getActiveCount()));
//            }
        }

        try {
            httpClient.close();
            httpget = null;

        } catch (IOException | NullPointerException e) {
            System.out.println(loginInfo.getAffiliateProgram().getName() + "---" + siteFromAffProgram.getSiteName() + " ---> Try/Catch downloading error");
//            e.printStackTrace();
        } finally {
            Thread.currentThread().interrupt();

        }
    }

    @Transactional
    void saveAllStats(List<GeneralStat> statList) {
        for (GeneralStat g : statList) {
            GeneralStat generalStat = generalStatRepository.findByStatsDateAndAppUserAndSiteFromAffProgramAndLoginInfoAndAffiliateProgram(g.getStatsDate(), g.getAppUser(), g.getSiteFromAffProgram(), g.getLoginInfo(), g.getAffiliateProgram());
            if (generalStat == null) {
                generalStatRepository.save(g);
            }
        }
    }
}