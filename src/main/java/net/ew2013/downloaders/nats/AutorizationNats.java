package net.ew2013.downloaders.nats;

import net.ew2013.model.Domain;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.Protocol;
import net.ew2013.security.AES;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContexts;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

/**
 * Created by konstantin on 10.08.16.
 */
public class AutorizationNats {
    private static final Logger LOGGER = LoggerFactory.getLogger(AutorizationNats.class);

    private CloseableHttpClient httpClient;
    private CloseableHttpResponse responsePost;
    private RequestConfig requestConfig;
    private String urlForLogin;
    private String login;
    private String password;
    private String uri;
    private String protocol;
    private String domain;

    public AutorizationNats() {}

    public AutorizationNats(String url, String login, String pass, String uri) {
//        this.httpClient = HttpClients.createDefault();
        try {
            this.httpClient = HttpClients.custom()
                    .setSSLSocketFactory(new SSLConnectionSocketFactory(SSLContexts.custom()
                                    .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                                    .build()
                            )
                    ).build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            e.printStackTrace();
        }
        this.urlForLogin = url;
        this.login = login;
        this.password = pass;
        this.uri = uri;
    }

    public AutorizationNats(LoginInfo loginInfo, String uri) {
//        this.httpClient = HttpClients.createDefault();
        try {
            this.httpClient = HttpClients.custom()
                    .setSSLSocketFactory(new SSLConnectionSocketFactory(SSLContexts.custom()
                                    .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                                    .build()
                            )
                    ).build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            e.printStackTrace();
        }
//        this.urlForLogin = loginInfo.getAffiliateProgram().getUrl();
        this.protocol = loginInfo.getAffiliateProgram().getProtocol().getProtocolName();
        this.domain = loginInfo.getAffiliateProgram().getDomain().getDomainName();
        this.urlForLogin = protocol + "://" + domain + "/";
        this.login = loginInfo.getLogin();
        this.password = new AES().decrypt(loginInfo.getPassword(), loginInfo.getAppUser().getPassword());
//        this.uri = loginInfo.getAffiliateProgram().getUri();
        this.uri = uri;
        //Hard stop connection
        int hardTimeout = 0; // seconds
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (httpClient != null) {
                    try {
                        httpClient.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

    }

    private boolean checkAutotizationStatus(Header[] headers) {
        boolean loginStatus = false;
        if ((headers.length > 0) && (headers[0].toString().contains("internal.php") || headers[0].toString().contains("members.php") || headers[0].toString().contains("admin_overview.php"))) {
            loginStatus = true;
        } else {
            for (Header header : headers) {
                System.err.println("Login false");
                System.err.println(header);
            }
        }
        return loginStatus;
    }

    public Boolean tryToLoginToNats() {
//        int timeoutSeconds = 5;
//        int CONNECTION_TIMEOUT_MS = timeoutSeconds * 1000; // Timeout in millis.
//        this.requestConfig = RequestConfig.custom()
//                .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
//                .setConnectTimeout(CONNECTION_TIMEOUT_MS)
//                .setSocketTimeout(CONNECTION_TIMEOUT_MS)
//                .build();

        String fullURL;
        HttpPost httpPost;
        List<NameValuePair> nvps = new ArrayList<>();
        String formFieldPassword = "pass";
        String formFieldUser = "user";
        fullURL = urlForLogin + uri;
        httpPost = new HttpPost(fullURL);
//        httpPost.setConfig(requestConfig);
        // httpPost.setConfig(requestConfig);
        nvps.add(new BasicNameValuePair(formFieldUser, login));
        nvps.add(new BasicNameValuePair(formFieldPassword, password));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            responsePost = httpClient.execute(httpPost);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Header[] headers = responsePost.getHeaders("Location");

        fullURL = null;
        httpPost = null;
        nvps = null;

        return checkAutotizationStatus(headers);
    }

    public void closeResources() {
        try {
            httpClient.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (responsePost != null) {
            try {
                responsePost.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public CloseableHttpClient getHttpClient() {
        return httpClient;
    }

    public Connection.Response tryToAutorization(Protocol p, Domain d, String uri, LoginInfo loginInfo) {
        Connection connection = null;
        Connection.Response response = null;
        String login = loginInfo.getLogin();
        String password = new AES().decrypt(loginInfo.getPassword(), loginInfo.getAppUser().getPassword());
        try {
            response = Jsoup.connect(p.getProtocolName() + "://" + d.getDomainName() + "/")
                    .method(Connection.Method.GET)
                    .execute();
            connection = Jsoup.connect(p.getProtocolName() + "://" + d.getDomainName() + "/" + uri)
                    .userAgent("Ew2013-collect-stats-program")
                    .cookies(response.cookies())
                    .timeout(50 * 1000)
                    .method(Connection.Method.POST)
                    .data("user", login)
                    .data("pass", password)
                    .followRedirects(false);
            if (p.getProtocolName().equalsIgnoreCase("https")) {
                connection.validateTLSCertificates(false);
            }
            response = connection.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
