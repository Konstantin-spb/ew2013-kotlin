package net.ew2013.downloaders.galleries;

import net.ew2013.downloaders.nats.AutorizationNats;

import net.ew2013.model.LoginInfo;
import net.ew2013.model.SaleProgram;
import net.ew2013.model.SiteForGalleries;
import net.ew2013.security.AES;
import net.ew2013.service.SaleProgramService;
import net.ew2013.service.SiteForGalleriesService;
import net.ew2013.utils.StreamToString;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by konstantin on 03.03.17.
 */
@Component
public class GalleriesDumperNats41 {

    @Autowired
    SiteForGalleriesService siteForGalleriesService;
    @Autowired
    SaleProgramService saleProgramService;

    private CloseableHttpClient httpClient;
    private LoginInfo loginInfo;
    private String protocol = "https";
    private String domain = "www.perfectgonzo.net";
    private String uriForLogin;
//    private String login = "cashstats";
    private String login = "ladytop";
//    private String password = "itisagood";
    private String password = "Handbook232323";

    public void setParams(LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
        this.protocol = loginInfo.getAffiliateProgram().getProtocol().getProtocolName();
        this.domain = loginInfo.getAffiliateProgram().getDomain().getDomainName();
        this.login = loginInfo.getLogin();
        this.password = new AES().decrypt(loginInfo.getPassword(), loginInfo.getAppUser().getPassword());
        if (loginInfo.getAffiliateProgram().getUri() != null) {
            this.uriForLogin = loginInfo.getAffiliateProgram().getUri();
        }
    }

//    public void getProgramIdInfo() {
//        Elements options = getOptionsElements("link_program");
////        SaleProgramDao saleProgramDao = new SaleProgramImpl();
//        if (options != null && options.size() > 0) {
//            for (Element e: options) {
//                if (e.attr("value").equals("0")) {
//                    continue;
//                }
//                SaleProgram saleProgram = new SaleProgram();
//                saleProgram.setAffiliateProgram(loginInfo.getAffiliateProgram());
//                saleProgram.setExternalId(e.attr("value"));
//                saleProgram.setProgramName(e.text());
//                try {
//                    saleProgramDao.saveOrUpdate(saleProgram);
//                } catch (Exception e1) {
//                    System.err.println("Unique index or primary key violation");
//                }
//                saleProgram = null;
//            }
//        } else {
//            System.out.println("Program do not have a link_program");
//        }
//    }

    public void getSiteOrProgramInfo(String elementsId) {
        Elements options = getOptionsElements(elementsId);
        String temp;
        if ((elementsId.equals("link_site") || elementsId.equals("link_program")) && options != null && options.size() > 0) {
//            System.out.println(loginInfo.getAffiliateProgram().getName());
            for (Element e: options) {
                if (e.attr("value").equals("0")) {
                    continue;
                }
                if (elementsId.equals("link_site")) {
                    SiteForGalleries siteForGalleries = new SiteForGalleries();
                    siteForGalleries.setAffiliateProgram(loginInfo.getAffiliateProgram());
                    siteForGalleries.setExternalId(e.attr("value"));
                    temp = e.text().replaceAll(" - Membership", "");
                    siteForGalleries.setSiteName(temp);
                    try {
                        System.out.println(e.attr("value") + " ---> " + temp);
                        siteForGalleriesService.saveOrUpdate(siteForGalleries);
                    } catch (Exception e1) {
                        System.err.println("Unique index or primary key violation");
                        e1.printStackTrace();
                    }
                    temp = "";
                    siteForGalleries = null;
                } else if (elementsId.equals("link_program")) {
                    SaleProgram saleProgram = new SaleProgram();
                    saleProgram.setAffiliateProgram(loginInfo.getAffiliateProgram());
                    saleProgram.setExternalId(e.attr("value"));
                    saleProgram.setProgramName(e.text());
                    try {
                        System.out.println(e.attr("value") + " ---> " + e.text());
                        saleProgramService.saveOrUpdate(saleProgram);
                    } catch (Exception e1) {
                        System.err.println("Unique index or primary key violation");
                    }
                    saleProgram = null;
                }
            }
        } else {
            System.out.println("do not have a link_program " + elementsId);
        }
    }

//    public void getSiteIdInfo() {
//        Elements options = getOptionsElements("link_site");
//
//    }

    public void getGalleriesFromSites() {

        //Start http://stackoverflow.com/questions/23870899/sslprotocolexception-handshake-alert-unrecognized-name-client-side-code-worka
        System.setProperty("jsse.enableSNIExtension", "false");
        //End
        String linksDump;
        String url = null;
        CloseableHttpResponse httpResponse = null;

        int siteId = -1;
        int programId = 2;

        AutorizationNats an = new AutorizationNats(protocol + "://" + domain, login, password, "external.php?page=access");
        httpClient = an.getHttpClient();
        try {
            if (an.tryToLoginToNats()) {
                url = protocol + "://" + domain + "/" + "internal_data.php?" +
                        "&function=nats_display_adtools_with_dump" +
                        "&category=2" +
                        "&typeid=2" +
                        "&campaignid=0" +
                        "&programid=" + programId +
                        "&siteid=" + siteId +
                        "&tourid=" +
                        "&submit_adtool_limits=TRUE" +
                        "&beg=" +
                        "&sep=%7C" +
                        "&end=" +
                        "&le=1" +
                        "&fields=url%2C%2C%2C%2C%2C%2C%2C" +
                        "&topts=Array" +
                        "&orderby=-999" +
                        "&ajax_dump=TRUE" +
                        "&toggle=1" +
                        "&start=0" +
                        "&count=100";
                //String d = "http://new.brothersincash.com/internal_data.php?&function=nats_display_adtools_with_dump&category=2&typeid=2&campaignid=0&programid=1&siteid=-1&tourid= &submit_adtool_limits=TRUE&beg=&sep=%7C&end=&le=1&fields=name%2Curl%2Ctype%2Citems%2Cmodel&topts=Array&orderby=-999&ajax_dump=TRUE&toggle=&start=0&count=1000";
                //http://www.julesjordancash.com/internal_data.php?&function=nats_display_adtools_with_dump&category=2&typeid=2&campaignid=0&programid=3&siteid=-1&tourid=0&submit_adtool_limits=TRUE&beg=&sep=%7C&end=&le=1&fields=url&topts=Array&orderby=-999&ajax_dump=TRUE&toggle=1&start=0&count=1000
                httpResponse = httpClient.execute(new HttpGet(url));
                linksDump = EntityUtils.toString(httpResponse.getEntity());
                System.out.println("1 ---> " + linksDump);
                linksDump = linksDump.replaceAll("\\\\r\\\\n|\\\\n", "|");
                linksDump = linksDump.replaceAll("\\\\", "");
                Pattern p = Pattern.compile("(http[?]*[^\"]+)");
                Matcher m = p.matcher(linksDump);
                String[] links;
                while (m.find()) {
                    linksDump = m.group();
                    System.out.println(m.group());
                }
                links = linksDump.split("\\|");
                int counter = 0;
                for (String s : links) {
                    System.out.println(s);
                    counter++;
                }
                System.out.println(counter);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (httpResponse != null) {
                    httpResponse.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            an.closeResources();
        }
    }

    private Elements getOptionsElements(String elementId) {
//        String uriForGetSiteInfo = "/internal.php?page=adtools&category=2&typeid=2";
        String uriForGetSiteInfo = "/internal.php?page=codes";
        String urlForGetProgramId = protocol + "://" + domain + "/" + uriForGetSiteInfo;
        CloseableHttpResponse httpResponse = null;
        AutorizationNats an = new AutorizationNats(protocol + "://" + domain, login, password, "external.php?page=access");
        Elements options = null;
        try {
            if (an.tryToLoginToNats()) {
                httpClient = an.getHttpClient();
                httpResponse = httpClient.execute(new HttpGet(urlForGetProgramId));
//                Document doc = Jsoup.parse(EntityUtils.toString(httpResponse.getEntity()));
                Document doc = Jsoup.parse(new StreamToString().getStringFromInputStream(httpResponse.getEntity().getContent()));
                Element id = doc.getElementById(elementId);
                options = id.getElementsByTag("option");
//                for (Element e :
//                        options) {
//                    System.out.println(e.attr("value") + " ---> " + e.text());
//                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
            System.err.println("catch option error -> " + loginInfo.getAffiliateProgram().getName());
        } finally {
            an.closeResources();
            try {
                if (httpResponse != null) {
                    httpResponse.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return options;
    }
}