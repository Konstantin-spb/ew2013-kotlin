package net.ew2013.downloaders.BangBrosOnline;

/**
 * Created by konstantin on 10.10.16.
 */
public class OneSite {

    private String siteName;
    private String externalSiteId;
    private long hits;
    private long signups;
    private long rebills;
    private long rebillMoney;
    private long chargebacks;
    private long chatgebackMoney;
    private long totalMoney;

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getExternalSiteId() {
        return externalSiteId;
    }

    public void setExternalSiteId(String externalSiteId) {
        this.externalSiteId = externalSiteId;
    }

    public long getHits() {
        return hits;
    }

    public void setHits(long hits) {
        this.hits = hits;
    }

    public long getSignups() {
        return signups;
    }

    public void setSignups(long signups) {
        this.signups = signups;
    }

    public long getRebills() {
        return rebills;
    }

    public void setRebills(long rebills) {
        this.rebills = rebills;
    }

    public long getRebillMoney() {
        return rebillMoney;
    }

    public void setRebillMoney(long rebillMoney) {
        this.rebillMoney = rebillMoney;
    }

    public long getChargebacks() {
        return chargebacks;
    }

    public void setChargebacks(long chargebacks) {
        this.chargebacks = chargebacks;
    }

    public long getChatgebackMoney() {
        return chatgebackMoney;
    }

    public void setChatgebackMoney(long chatgebackMoney) {
        this.chatgebackMoney = chatgebackMoney;
    }

    public long getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(long totalMoney) {
        this.totalMoney = totalMoney;
    }
}
