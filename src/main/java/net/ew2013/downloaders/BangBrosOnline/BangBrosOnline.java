package net.ew2013.downloaders.BangBrosOnline;

import net.ew2013.controller.AppController;
import net.ew2013.downloaders.PostRequest;
import net.ew2013.model.GeneralStat;
import net.ew2013.model.LastdateDownloadStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.ServiceCollectionRunsThreads;
import net.ew2013.model.SiteFromAffProgram;
import net.ew2013.repository.GeneralStatRepository;
import net.ew2013.repository.LastdateDownloadStatRepository;
import net.ew2013.repository.ServiceCollectionRunsThreadsRepository;
import net.ew2013.repository.StatusRepository;
import net.ew2013.security.AES;
import net.ew2013.service.LastdateDownloadStatService;
import net.ew2013.service.LoginInfoService;
import net.ew2013.service.PaymentService;
import net.ew2013.service.SiteFromAffProgramsService;
import net.ew2013.service.SiteService;
import net.ew2013.service.StatusService;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import java.sql.Date;

/**
 * Created by konstantin on 07.10.16.
 */
@Component
@Scope(value = "prototype")
public class BangBrosOnline {
    //    io_bb
//    login cashstats
//    password itisagood
//    process webmaster.login
//    @Autowired
//    SiteFromAffProgramsDao_ siteFromAffProgramsService;
    @Autowired
    LoginInfoService loginInfoService;
    @Autowired
    SiteService siteService;
    @Autowired
    ApplicationContext context;
    @Autowired
    PaymentService paymentService;
    @Autowired
    SiteFromAffProgramsService siteFromAffProgramsService;
    @Autowired
    StatusService statusService;
    @Autowired
    LastdateDownloadStatService lastdateDownloadStatService;
    @Autowired
    GeneralStatRepository generalStatRepository;
    @Autowired
    ServiceCollectionRunsThreadsRepository serviceCollectionRunsThreadsRepository;
    @Autowired
    LastdateDownloadStatRepository lastdateDownloadStatRepository;
    @Autowired
    StatusRepository statusRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(BangBrosOnline.class);
    private CloseableHttpClient httpClient;
    private LoginInfo loginInfo;
    //    private String urlForLogin;
    private String login;
    private String password;
    private String uriForLogin;
    private String protocol;
    private String domain;

    public void setParams(LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
        this.httpClient = HttpClients.createDefault();
        this.protocol = loginInfo.getAffiliateProgram().getProtocol().getProtocolName();
        this.domain = loginInfo.getAffiliateProgram().getDomain().getDomainName();
//        this.urlForLogin = protocol + "://" + domain;
        this.login = loginInfo.getLogin();
        this.password = new AES().decrypt(loginInfo.getPassword(), loginInfo.getAppUser().getPassword());
        this.uriForLogin = loginInfo.getAffiliateProgram().getUri() != null ? loginInfo.getAffiliateProgram().getUri() : "";
        HttpGet httpGet = new HttpGet(protocol + "://" + domain + "/" + loginInfo.getAffiliateProgram().getUri());
        try {
            this.httpClient.execute(httpGet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Async("ccbillThreadPoolTaskExecutor")
    public void call() {

        ServiceCollectionRunsThreads serviceCollectionRunsThreads = new ServiceCollectionRunsThreads();
        serviceCollectionRunsThreads.setAffiliateProgram(loginInfo.getAffiliateProgram());
        serviceCollectionRunsThreads.setLoginInfo(loginInfo);
        serviceCollectionRunsThreads.setAppUser(loginInfo.getAppUser());
//        serviceCollectionRunsThreads.setSiteFromAffProgram(siteFromAffProgram);
        serviceCollectionRunsThreads = serviceCollectionRunsThreadsRepository.save(serviceCollectionRunsThreads);

        String threadName = loginInfo.getAffiliateProgram().getType() + "_" + loginInfo.getAppUser().getLoginName()
                + "_" + loginInfo.getAffiliateProgram().getName();
        AppController.lisfOfThreads.add(threadName);
        Thread.currentThread().setName(threadName);

        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("io_bb", ""));
        nvps.add(new BasicNameValuePair("login", login));
        nvps.add(new BasicNameValuePair("password", password));
        nvps.add(new BasicNameValuePair("process", "webmaster.login"));

        PostRequest autorization = new PostRequest();
        try {
            Header[] headers = autorization.sendRequest(httpClient, nvps, protocol + "://" + domain + "/" + uriForLogin);
            if (headers.length > 0 && String.valueOf(headers[0]).contains("members/menu.php")) {

                Date lastDate;
                LocalDate currentDate;
                LocalDate today;
                Document doc;

                lastDate = ((loginInfo.getLastDate()) != null? loginInfo.getLastDate():loginInfo.getStartDate());
                currentDate = new java.sql.Date(lastDate.getTime()).toLocalDate();
                today = LocalDate.now();

                while (currentDate.isBefore(today)) {
                    String htmlPage = getStatsByOneDay(currentDate);
                    doc = Jsoup.parse(htmlPage);
                    Elements elements = doc.select("#table2");
                    for (Element e : elements) {
                        Elements trs = e.getElementsByTag("tr");//sites info
                        for (int i = 0; i < trs.size() - 1; i++) {
                            Element tr = trs.get(i);
                            Elements tds = tr.getElementsByTag("td");

                            if (tds.toString().contains("site_id")) {
                                SiteFromAffProgram siteFromAffProgram;
                                OneSite oneSite = new OneSite();
                                GeneralStat generalStat = null;
                                String[] uriParams = tds.get(0).toString().split("\\&");

                                for (String s : uriParams) {
                                    if (s.contains("site_id")) {
                                        oneSite.setExternalSiteId(s.replaceAll("\\D", ""));
                                    }
                                }

                                if (tds.size() == 5) {
                                    oneSite.setSiteName(tds.get(0).text());
                                    oneSite.setHits(Integer.parseInt(tds.get(1).text().replaceAll("\\D", "")));
                                    oneSite.setSignups(Integer.parseInt(tds.get(2).text().replaceAll("\\D", "")));
                                    oneSite.setTotalMoney(Integer.parseInt(tds.get(3).text().replaceAll("\\D", "")));
                                } else if (tds.size() == 8) {
                                    if (oneSite.getSiteName() == null) {
                                        oneSite.setSiteName(tds.get(0).text());
                                    }
                                    oneSite.setHits(oneSite.getHits() + Integer.parseInt(tds.get(1).text().replaceAll("\\D", "")));
                                    oneSite.setSignups(oneSite.getSignups() + Integer.parseInt(tds.get(2).text().replaceAll("\\D", "")));
                                    oneSite.setRebills(Integer.parseInt(tds.get(4).text().replaceAll("\\D", "")));
                                    oneSite.setRebillMoney(Integer.parseInt(tds.get(5).text().replaceAll("\\D", "")));
                                    oneSite.setChargebacks(Integer.parseInt(tds.get(6).text().replaceAll("\\D", "")));
                                    oneSite.setChatgebackMoney(Integer.parseInt(tds.get(7).text().replaceAll("\\D", "")));
                                    oneSite.setTotalMoney(oneSite.getTotalMoney() + oneSite.getRebillMoney() - oneSite.getChatgebackMoney());
                                } else if (tds.size() == 6) {
                                    oneSite.setSiteName(tds.get(0).text());
                                    oneSite.setExternalSiteId("999");
                                    oneSite.setTotalMoney(oneSite.getTotalMoney() + Integer.parseInt(tds.get(5).text().replaceAll("\\D", "")));
                                } else {
                                    continue;
//                            LOGGER.warn("Такой таблицы не существует");
                                }

                                siteFromAffProgram = siteService.setOneSite(oneSite.getExternalSiteId(), oneSite.getSiteName(), loginInfo);
                                if (oneSite.getHits() != 0 || oneSite.getSignups() != 0 || oneSite.getRebills() != 0 || oneSite.getRebillMoney() != 0
                                        || oneSite.getChargebacks() != 0 || oneSite.getChatgebackMoney() != 0 || oneSite.getTotalMoney() != 0) {
//                                siteFromAffProgram.setLastdate(new java.util.Date());
//                                siteFromAffProgram.setDownloadingStatus("DONE");

//                                Date tempDate = java.sql.Date.valueOf(currentDate);
                                    generalStat = generalStatRepository.findByStatsDateAndAppUserAndSiteFromAffProgramAndLoginInfoAndAffiliateProgram(java.sql.Date.valueOf(currentDate), loginInfo.getAppUser(), siteFromAffProgram, loginInfo, loginInfo.getAffiliateProgram());

                                    if (generalStat == null) {
                                        generalStat = new GeneralStat();
                                        //TODO make one method for all downloaders
                                        siteFromAffProgramsService.saveOrUpdate(siteFromAffProgram);
                                        generalStat.setAppUser(loginInfo.getAppUser());
                                        generalStat.setLoginInfo(loginInfo);
                                        generalStat.setAffiliateProgram(loginInfo.getAffiliateProgram());
                                        generalStat.setSiteFromAffProgram(siteFromAffProgram);
                                        generalStat.setStatsDate(java.sql.Date.valueOf(currentDate));
                                        generalStat.setUniqueClicks(oneSite.getHits());
                                        generalStat.setSignupCount(oneSite.getSignups());
                                        generalStat.setRebillCount(oneSite.getRebills());
                                        generalStat.setTotalMoney(oneSite.getTotalMoney());
                                        generalStatRepository.save(generalStat);
                                    }
                                }
                                lastdateDownloadStatService.saveLastDateWithStatus(loginInfo.getAppUser(), loginInfo, siteFromAffProgram, "DONE", new Date());
                                oneSite = null;
                                generalStat = null;
                            }
                        }
                    }
                    currentDate = currentDate.plusDays(1);
                    loginInfo.setLastDate(java.sql.Date.valueOf(currentDate.toString()));
                    loginInfoService.saveOrUpdate(loginInfo);

                    doc = null;
                    LOGGER.info(String.valueOf(currentDate));
                }
            } else {
                LOGGER.warn(loginInfo.getAffiliateProgram().getName() + " login failed.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            serviceCollectionRunsThreadsRepository.delete(serviceCollectionRunsThreads);
            LOGGER.info(loginInfo.getAffiliateProgram().getName() + " downloads complete");
            AppController.lisfOfThreads.remove(threadName);
            Thread.currentThread().interrupt();
        }
    }

    //Stars and end dates is equals
    private String getStatsByOneDay(LocalDate startDate) {
//        end_date 2014-10-15
//        process webmaster.stats
//        start_date 2014-10-01
//        submit Run Report
//        type summary
        String statsUrl = "http://bangbrosonline.com/members/stats.php";
        String sourceHtmlFile = null;
        LocalDate endDate;
        endDate = startDate;

        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("process", "webmaster.stats"));
        nvps.add(new BasicNameValuePair("type", "summary"));
        nvps.add(new BasicNameValuePair("start_date", startDate.toString()));
        nvps.add(new BasicNameValuePair("end_date", endDate.toString()));
        nvps.add(new BasicNameValuePair("submit", "Run Report"));

        PostRequest postRequest = new PostRequest();
        for (int i = 0; i < 5; i++) {
            try {
                postRequest.sendRequest(httpClient, nvps, statsUrl);
                break;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        sourceHtmlFile = postRequest.getSourceTextFile();

        return sourceHtmlFile;
    }


}