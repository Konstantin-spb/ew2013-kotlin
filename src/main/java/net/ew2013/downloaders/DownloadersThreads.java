package net.ew2013.downloaders;

import net.ew2013.downloaders.BangBrosOnline.BangBrosOnline;
import net.ew2013.downloaders.Ccbill.Ccbills;
import net.ew2013.downloaders.badoinkcash.BadoinkCash;
import net.ew2013.downloaders.gammae.Gammae;
import net.ew2013.downloaders.nats.Nats;
import net.ew2013.downloaders.selenium.Nats4StandartSelenium;
import net.ew2013.model.LoginInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.SQLException;


@Component
//@Scope(value = "prototype")
public class DownloadersThreads {

//    @Autowired
//    ExecutorService executorService;

    @Autowired
    private ApplicationContext ctx;

    public void startDownloadsAP(LoginInfo loginInfo) {

        Nats4StandartSelenium nats4StandartSelenium = ctx.getBean(Nats4StandartSelenium.class);
        Nats nats = ctx.getBean(Nats.class);
        Ccbills ccbill = ctx.getBean(Ccbills.class);
        BangBrosOnline bangBrosOnline = ctx.getBean(BangBrosOnline.class);
        BadoinkCash badoinkCash = ctx.getBean(BadoinkCash.class);
        Gammae gammae = ctx.getBean(Gammae.class);

        if (loginInfo.getAffiliateProgram().getType().equalsIgnoreCase("ccbill")) {
            ccbill.setParams(loginInfo);
            ccbill.run();
        } else if (loginInfo.getAffiliateProgram().getType().equalsIgnoreCase("nats")) {

            nats.setParams(loginInfo);
            try {
                nats.run();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (loginInfo.getAffiliateProgram().getType().equalsIgnoreCase("Nats4StandartSelenium")) {
            nats4StandartSelenium.setParams(loginInfo);
            nats4StandartSelenium.Autorization();
        } else if (loginInfo.getAffiliateProgram().getType().equalsIgnoreCase("bangbrosonline")) {
            bangBrosOnline.setParams(loginInfo);
            bangBrosOnline.call();
        } else if (loginInfo.getAffiliateProgram().getType().equalsIgnoreCase("badoinkcash")) {
            badoinkCash.setParams(loginInfo);
            badoinkCash.run();
        } else if (loginInfo.getAffiliateProgram().getType().equalsIgnoreCase("famedollars")) {
            gammae.setParams(loginInfo);
            gammae.call();
        }
    }
}