package net.ew2013.downloaders.badoinkcash;

import net.ew2013.controller.AppController;
import net.ew2013.utils.dateUtils.DateWorks;
import net.ew2013.model.GeneralStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.ServiceCollectionRunsThreads;
import net.ew2013.model.SiteFromAffProgram;
import net.ew2013.repository.GeneralStatRepository;
import net.ew2013.repository.LastdateDownloadStatRepository;
import net.ew2013.repository.ServiceCollectionRunsThreadsRepository;
import net.ew2013.repository.StatusRepository;
import net.ew2013.security.AES;
import net.ew2013.service.LastdateDownloadStatService;
import net.ew2013.service.SiteFromAffProgramsService;
import net.ew2013.service.SiteService;
import net.ew2013.service.StatusService;
import net.ew2013.utils.DataFromDb;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope(value = "prototype")
public class BadoinkCash {
    private static final Logger LOGGER = LoggerFactory.getLogger(BadoinkCash.class);
    @Autowired
    GeneralStatRepository generalStatRepository;
    @Autowired
    SiteFromAffProgramsService siteFromAffProgramsServicel;
    @Autowired
    SiteService siteService;
    @Autowired
    ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Autowired
    StatusService statusService;
    @Autowired
    ServiceCollectionRunsThreadsRepository serviceCollectionRunsThreadsRepository;
    @Autowired
    LastdateDownloadStatRepository lastdateDownloadStatRepository;
    @Autowired
    StatusRepository statusRepository;
    @Autowired
    LastdateDownloadStatService lastdateDownloadStatService;

    private LoginInfo loginInfo;
    private String protocolName;
    private String domainName;

    @Async("threadPoolTaskExecutor")
    public void run() {
        ServiceCollectionRunsThreads serviceCollectionRunsThreads = new ServiceCollectionRunsThreads();
        serviceCollectionRunsThreads.setAffiliateProgram(loginInfo.getAffiliateProgram());
        serviceCollectionRunsThreads.setLoginInfo(loginInfo);
        serviceCollectionRunsThreads.setAppUser(loginInfo.getAppUser());
        String threadName = loginInfo.getAffiliateProgram().getType() + "_" + loginInfo.getAppUser().getLoginName()
                + "_" + loginInfo.getAffiliateProgram().getName();
        AppController.lisfOfThreads.add(threadName);
        Thread.currentThread().setName(threadName);

        Connection.Response response = tryToAutorization(loginInfo);
        try {
            Map<String, String> sitesList = getSitesList("members/stats", response);
            for (Map.Entry<String, String> site : sitesList.entrySet()) {
                getSiteStats(loginInfo, siteService.setOneSite(site.getKey(), site.getValue(), loginInfo), response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            AppController.lisfOfThreads.remove(threadName);
            logout();
            serviceCollectionRunsThreadsRepository.delete(serviceCollectionRunsThreads);
            Thread.currentThread().interrupt();
        }
    }

    private void getSiteStats(LoginInfo loginInfo, SiteFromAffProgram siteFromAffProgram, Connection.Response response) {
        List<GeneralStat> generalStatList = new ArrayList<>();
        LocalDate lastDate = new java.sql.Date(
                new DataFromDb().getLastDate(siteFromAffProgram, loginInfo).getTime()).toLocalDate().minusDays(1);
        Connection connection = Jsoup.connect(protocolName + "://" + domainName + "/" + "members/stats?start=" + lastDate.toString().replaceAll("\\D", "") + "&end=" + DateWorks.getLastDayOfMonth() + "&tab=daily&product=" + siteFromAffProgram.getExternalSiteId())
                .userAgent("Ew2013-collect-stats-program")
                .timeout(50 * 1000)
                .cookies(response.cookies())
                .method(Connection.Method.GET)
                .validateTLSCertificates(false)
                .followRedirects(true);
        try {
            Document doc = connection.execute().parse();
            Elements trs = doc.getElementsByTag("tbody").first().children();

            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            String dateInString = "";

            for (Element aTr : trs) {
                Elements tds = aTr.children();
                GeneralStat generalStat = new GeneralStat();
                Date date = formatter.parse(tds.get(0).text());
                generalStat.setStatsDate(date);
                generalStat.setUniqueClicks(Long.parseLong(tds.get(1).text().replaceAll("\\D", "")));
                generalStat.setSignupCount(Long.parseLong(tds.get(2).text().replaceAll("\\D", "")));
                generalStat.setRebillCount(Long.parseLong(tds.get(3).text().replaceAll("\\D", "")));
                generalStat.setRefundChargebackCount(Long.parseLong(tds.get(4).text().replaceAll("\\D", "")));
                generalStat.setTotalMoney(Long.parseLong(tds.get(6).text().replaceAll("\\D", "")));

                generalStat.setAppUser(loginInfo.getAppUser());
                generalStat.setAffiliateProgram(loginInfo.getAffiliateProgram());
                generalStat.setLoginInfo(loginInfo);
                generalStat.setSiteFromAffProgram(siteFromAffProgram);
                generalStatList.add(generalStat);
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        saveAllStats(generalStatList);
        lastdateDownloadStatService.saveLastDateWithStatus(loginInfo.getAppUser(), loginInfo, siteFromAffProgram, "DONE", new Date());

        LOGGER.info(siteFromAffProgram.getSiteName());
    }

    private Map<String, String> getSitesList(String uriForGetSitesList, Connection.Response response) throws IOException {

        Map<String, String> sitesList = new HashMap<>();
        Document doc = null;

        Connection connection = Jsoup.connect(protocolName + "://" + domainName + "/" + uriForGetSitesList)
                .timeout(50 * 1000)
                .cookies(response.cookies());
        if (protocolName.equalsIgnoreCase("https")) {
            connection.validateTLSCertificates(false);
        }
        doc = connection.get();

        Elements sites = doc.getElementById("product").children();

        String siteIDloc;
        String siteNameloc;

        for (Element s : sites) {
            siteIDloc = s.attr("value");
            siteNameloc = s.text();
            if (!siteIDloc.equalsIgnoreCase("")) {
                sitesList.put(siteIDloc, siteNameloc);
            }
        }
        doc = null;
        return sitesList;
    }

    @Transactional
    void saveAllStats(List<GeneralStat> statList) {
        for (GeneralStat g : statList) {
            GeneralStat generalStat = generalStatRepository.findByStatsDateAndAppUserAndSiteFromAffProgramAndLoginInfoAndAffiliateProgram(g.getStatsDate(), g.getAppUser(), g.getSiteFromAffProgram(), g.getLoginInfo(), g.getAffiliateProgram());
            if (generalStat == null) {
                generalStatRepository.save(g);
            }
        }
    }

    private void logout() {
        try {
            Jsoup.connect(protocolName + "://" + domainName + "/" + "login/logout").validateTLSCertificates(false).get();
        } catch (IOException e) {
//            e.printStackTrace();
            System.err.println("IOException Logout");
        }
        LOGGER.info(loginInfo.getAffiliateProgram().getName() + ": The program has finished work");
//            httpClient.execute(new HttpGet(protocol + "://" + domain + "/" + "index.php?logout=1"));
    }

    public void setParams(LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
        this.protocolName = loginInfo.getAffiliateProgram().getProtocol().getProtocolName();
        this.domainName = loginInfo.getAffiliateProgram().getDomain().getDomainName();
    }

    private Connection.Response tryToAutorization(LoginInfo l) {
        Connection connection = null;
        Connection.Response response = null;
        String userField = "username";
        String passwordField = "password";
        String login = loginInfo.getLogin();
        String password = new AES().decrypt(l.getPassword(), l.getAppUser().getPassword());

        String url = l.getAffiliateProgram().getProtocol().getProtocolName() + "://" + l.getAffiliateProgram().getDomain().getDomainName() + "/" + l.getAffiliateProgram().getUri();
        try {
            connection = Jsoup.connect(url)
                    .userAgent("Ew2013-collect-stats-progtam")
                    .timeout(50 * 1000)
                    .method(Connection.Method.POST)
                    .data(userField, login)
                    .data(passwordField, password)
                    .followRedirects(true);
            if (l.getAffiliateProgram().getProtocol().getProtocolName().equalsIgnoreCase("https")) {
                connection.validateTLSCertificates(false);
            }
            response = connection.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
