package net.ew2013.downloaders.gammae;

import net.ew2013.downloaders.BangBrosOnline.BangBrosOnline;
import net.ew2013.downloaders.PostRequest;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.ServiceCollectionRunsThreads;
import net.ew2013.repository.ServiceCollectionRunsThreadsRepository;
import net.ew2013.security.AES;
import net.ew2013.utils.StreamToString;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.print.Doc;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope(value = "prototype")
public class Gammae {

//    back	/stats/famedollars
//    user	202170
//    password	absolute2323

//    http://stats.gammae.com/stats/famedollars
//    http://stats.gammae.com/login.php?back=/stats/famedollars
//    http://stats.gammae.com/index.php

    @Autowired
    ServiceCollectionRunsThreadsRepository serviceCollectionRunsThreadsRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(BangBrosOnline.class);
    private CloseableHttpClient httpClient;
    private LoginInfo loginInfo;
    private String login;
    private String password;
    private String uriForLogin;
    private String protocol;
    private String domain;

    public void setParams(LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
        this.httpClient = HttpClients.createDefault();
        this.protocol = loginInfo.getAffiliateProgram().getProtocol().getProtocolName();
        this.domain = loginInfo.getAffiliateProgram().getDomain().getDomainName();
        this.login = loginInfo.getLogin();
        this.password = new AES().decrypt(loginInfo.getPassword(), loginInfo.getAppUser().getPassword());
        this.uriForLogin = loginInfo.getAffiliateProgram().getUri() != null ? loginInfo.getAffiliateProgram().getUri() : "";
        HttpGet httpGet = new HttpGet(protocol + "://" + domain + "/" + loginInfo.getAffiliateProgram().getUri());
        try {
            this.httpClient.execute(httpGet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Async("ccbillThreadPoolTaskExecutor")
    public void call() {

        ServiceCollectionRunsThreads serviceCollectionRunsThreads = new ServiceCollectionRunsThreads();
        serviceCollectionRunsThreads.setAffiliateProgram(loginInfo.getAffiliateProgram());
        serviceCollectionRunsThreads.setLoginInfo(loginInfo);
        serviceCollectionRunsThreads.setAppUser(loginInfo.getAppUser());
//        serviceCollectionRunsThreads.setSiteFromAffProgram(siteFromAffProgram);
        serviceCollectionRunsThreadsRepository.save(serviceCollectionRunsThreads);

        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair("user", login));
        nvps.add(new BasicNameValuePair("password", password));
        nvps.add(new BasicNameValuePair("back", "/stats/famedollars"));

//        link for revshare program
//        http://stats.gammae.com/famedollars/stats/
// revshare.php?s_year=2017&s_month=01&s_day=01&e_year=2017&e_month=01&e_day=01&list=site&subprogram_id=1&program_id=8&submit=go

//        link for signup program
//        http://stats.gammae.com/famedollars/stats/
// persignup.php?s_year=2017&s_month=01&s_day=01&e_year=2017&e_month=01&e_day=01&list=site&subprogram_id=2&program_id=8&submit=go

//        link for webmaster refferal program
//        http://stats.gammae.com/famedollars/stats/
// refwebmaster.php?s_year=2017&s_month=01&s_day=01&e_year=2017&e_month=01&e_day=01&list=site&subprogram_id=4&program_id=8&submit=go

//        link for mobile revshare program
//        http://stats.gammae.com/famedollars/stats/
// revshare.php?s_year=2017&s_month=01&s_day=01&e_year=2017&e_month=01&e_day=01&list=site&subprogram_id=8&program_id=8&submit=go

        PostRequest autorization = new PostRequest();

        try {
            Header[] headers = autorization.sendRequest(httpClient, nvps, protocol + "://" + domain + "/" + uriForLogin);

            if (headers.length > 0 && String.valueOf(headers[0]).contains("/stats/famedollars")) {
                System.out.println("Login true");
            }

            String url = protocol + "://" + domain + "/" +"famedollars/stats/revshare.php?s_year=2017&s_month=01&s_day=01&e_year=2017&e_month=01&e_day=01&list=site&subprogram_id=1&program_id=8&submit=go";
            String sourceFile = sendGetRequest(url);
            getRevshareStat(sourceFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String sendGetRequest(String url) {
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse responseGet;
        HttpEntity entityGet;

        try {
            responseGet = httpClient.execute(httpGet);
            entityGet = responseGet.getEntity();
            String tempSourceFile = new StreamToString().getStringFromInputStream(entityGet.getContent());
//            System.out.println(tempSourceFile);
            return tempSourceFile;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    private void getRevshareStat(String soutceFile) {
        Document doc = Jsoup.parse(soutceFile);
        Element table = doc.select("table.stats-outline").first();
        Elements trs = table.select("tr.stats-pale-bckgrnd, tr.stats-vpale-bckgrnd");
        for (int i = 4; i < trs.size() - 2; i++) {
            Elements tds = trs.get(i).select("td");
            if (tds.size() == 9) {
                for (int j = 0; j < tds.size(); j++) {
                    if (j == 0) {
                        System.out.print(tds.get(j).text() + " ");
                    } else if (j == 1) {
                        System.out.print(tds.get(j).text() + " ");
                    } else if (j == 5) {
                        System.out.print(tds.get(j).text().replaceAll("\\D", "") + " ");
                    } else if (j == 7) {
                        System.out.print(tds.get(j).text().replaceAll("\\D", "") + " ");
                    }
                }
            }
            System.out.println();
        }
    }
}
