package net.ew2013.downloaders.selenium;

import net.ew2013.controller.AppController;
import net.ew2013.utils.dateUtils.DateWorks;
import net.ew2013.downloaders.nats.fileHandlers.xmlHandlers.XmlHandlerNats4DOM;
import net.ew2013.model.*;
import net.ew2013.repository.AccountBalanceRepository;
import net.ew2013.repository.GeneralStatRepository;
import net.ew2013.repository.ServiceCollectionRunsThreadsRepository;
import net.ew2013.security.AES;
import net.ew2013.service.LastdateDownloadStatService;
import net.ew2013.service.ServiceCollectionRunsThreadsService;
import net.ew2013.service.SiteService;
import net.ew2013.utils.DataFromDb;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
@Scope(value = "prototype")
public class Nats4StandartSelenium {
    private static final Logger LOGGER = LoggerFactory.getLogger(Nats4StandartSelenium.class);

    @Autowired
    SiteService siteService;
    @Autowired
    ApplicationContext context;
    @Autowired
    AccountBalanceRepository accountBalanceRepository;
    @Autowired
    ServiceCollectionRunsThreadsRepository serviceCollectionRunsThreadsRepository;
    @Autowired
    GeneralStatRepository generalStatRepository;
    @Autowired
    LastdateDownloadStatService lastdateDownloadStatService;
    @Autowired
    ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Autowired
    ServiceCollectionRunsThreadsService serviceCollectionRunsThreadsService;

    //    String domain = "http://new.brothersincash.com";

    private LoginInfo loginInfo;
    private String protocol;
    private String domain;
    private boolean javaSciptEnable;

    public void setParams(LoginInfo loginInfo) {
        this.protocol = loginInfo.getAffiliateProgram().getProtocol().getProtocolName();
        this.domain = loginInfo.getAffiliateProgram().getDomain().getDomainName();
        this.loginInfo = loginInfo;
        this.javaSciptEnable = loginInfo.getAffiliateProgram().isJavascriptEnable();
    }

    @Async("threadPoolTaskExecutor")
    public void Autorization() {
//        String periodUriForStat;
        String threadName = loginInfo.getAffiliateProgram().getType() + "_" + loginInfo.getAppUser().getLoginName()
                + "_" + loginInfo.getAffiliateProgram().getName();
        AppController.lisfOfThreads.add(threadName);
        Thread.currentThread().setName(threadName);

        WebDriver driver = new HtmlUnitDriver(javaSciptEnable);
        String sitelistUri = "/internal.php?page=stats&view=date&period=2";
        String paymentUri = "internal.php?page=payments";
        ServiceCollectionRunsThreads serviceCollectionRunsThreads = null;

//        String cssTemplate = "#header";//driver.findElement(By.id("header"))

        String pageSource;


        Map<String, String> siteList;

        try {
            for (int tryingCount = 1; tryingCount <= 5; tryingCount++) {
                try {

                    autorizationForm(driver);
                    //get payment info
                    driver.get(protocol + "://" + domain + "/" + paymentUri);
                    getPaymentInfo(driver.getPageSource());

//        driver.get(domain + "/internal.php?page=stats&view=date");//страница для получения id сайтов
                    driver.get(protocol + "://" + domain + sitelistUri);
                    pageSource = driver.getPageSource();

                    siteList = getSiteList(pageSource);
                    serviceCollectionRunsThreads = serviceCollectionRunsThreadsService.setServiceCollectionRunsThreads(null, loginInfo);
                    for (Map.Entry<String, String> site :
                            siteList.entrySet()) {
                        SiteFromAffProgram siteFromAffProgram = null;
                        Date lastDate = null;
                        try {
                            siteFromAffProgram = siteService.setOneSite(site.getKey(), site.getValue(), loginInfo);
                            lastDate = new DataFromDb().getLastDate(siteFromAffProgram, loginInfo);
                            LocalDate localLastDate = new java.sql.Date(new DataFromDb().getLastDate(siteFromAffProgram, loginInfo).getTime()).toLocalDate().minusDays(1);
                            driver.get(protocol + "://" + domain + "/internal.php?page=dump&period=8&start=" + localLastDate + "&end=" + DateWorks.getLastDayOfMonth() + "&filter_siteid=" + site.getKey() + "&format=xml");
                            XmlHandlerNats4DOM xmlHandlerNats4DOM = new XmlHandlerNats4DOM();
                            xmlHandlerNats4DOM.getStatsNats4DOM(driver.getPageSource(), loginInfo, siteFromAffProgram);
                            saveAllStats(xmlHandlerNats4DOM.getGeneralStatList());
                            xmlHandlerNats4DOM.setGeneralStatList(null);
                            lastdateDownloadStatService.saveLastDateWithStatus(loginInfo.getAppUser(), loginInfo, siteFromAffProgram, "DONE", new Date());
                            System.out.println(site.getKey() + " --- " + site.getValue());
                            siteFromAffProgram = null;
                            lastDate = null;
                            xmlHandlerNats4DOM = null;
                        } catch (Exception ex) {
                            lastdateDownloadStatService.saveLastDateWithStatus(loginInfo.getAppUser(), loginInfo, siteFromAffProgram, "ERROR", lastDate);
                        }
                    }
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.info(loginInfo.getAffiliateProgram().getName() + "Попытка номер" + tryingCount + " была неудачная");
                }
            }
        } finally {
            if (serviceCollectionRunsThreads != null) {
                serviceCollectionRunsThreadsRepository.delete(serviceCollectionRunsThreads);
            }
            System.out.println("Number of threads - " + threadPoolTaskExecutor.getActiveCount());
            sitelistUri = null;
            paymentUri = null;
            serviceCollectionRunsThreads = null;
            pageSource = null;
            AppController.lisfOfThreads.remove(threadName);
            threadPoolTaskExecutor.getThreadPoolExecutor().purge();
            driver.close();
        }
    }

    private Map<String, String> getSiteList(String sourcePage) {

        Map<String, String> sitesList = new HashMap<>();

        String siteIDloc;
        String siteNameloc;
        Document doc = Jsoup.parse(sourcePage);
        Elements content = doc.getElementsByAttributeValue("name", "filter_siteid");
        Elements options = content.select("option");
        for (Element option : options) {
            siteIDloc = option.attr("value");
            siteNameloc = option.text();
            if (!siteIDloc.equalsIgnoreCase("") && !siteIDloc.equalsIgnoreCase("0")) {
                sitesList.put(siteIDloc, siteNameloc);
            }
        }

        return sitesList;
    }

    @Transactional
    void saveAllStats(List<GeneralStat> statList) {
        for (GeneralStat g : statList) {
            GeneralStat generalStat = generalStatRepository.findByStatsDateAndAppUserAndSiteFromAffProgramAndLoginInfoAndAffiliateProgram(g.getStatsDate(), g.getAppUser(), g.getSiteFromAffProgram(), g.getLoginInfo(), g.getAffiliateProgram());
            if (generalStat == null) {
                generalStatRepository.save(g);
            }
        }
        statList.clear();
        statList = null;
    }

    private void getPaymentInfo(String sourceFile) {
        Document doc = Jsoup.parse(sourceFile);
        Element table;
        Elements trs;

        if (doc.getElementById("payment-table") != null) {
            table = doc.getElementById("payment-table");
            trs = table.select("tr[class*=\"data-row\"]");

            for (Element tr : trs) {
//                    LOGGER.info("==============");
                Payment payment = new Payment();
                payment.setAppUser(loginInfo.getAppUser());
                payment.setAffiliateProgram(loginInfo.getAffiliateProgram());
                payment.setLoginInfo(loginInfo);
                for (Element td : tr.select("td")) {
                    if (td.className().contains("col_1")) {
                        payment.setStatus(td.text());
                    }
                    if (td.className().contains("col_2")) {
                        try {
                            payment.setPaymentSent(java.sql.Date.valueOf(td.text()));
                        } catch (IllegalArgumentException e) {
                            // e.printStackTrace();
                        }
                    }
                    if (td.className().contains("col_4")) {
                        payment.setAmount(Integer.parseInt(td.text().replaceAll("\\D", "")));
                    }
                    if (td.className().contains("col_5")) {
                        payment.setMethod(td.text());
                    }
                    // LOGGER.info(td.text());
                }
                if (payment.getMethod() != null && payment.getStatus() != null
                        && payment.getPaymentSent() != null) {
                    // paymentDao.saveOrUpdate(payment);
                } else if (payment.getPaymentSent() == null && payment.getAmount() != null) {
                    saveAccountBallance(payment);

                }
                payment = null;
                // LOGGER.info("==============");
            }
        } else if (doc.select("table[class=\"payments_table\"]") != null && doc.select("table[class=\"payments_table\"]").size() > 0) {
            table = doc.select("table[class=\"payments_table\"]").first();
            trs = table.select("tr[class~=payments_tile_]");
            for (Element tr : trs) {
                // LOGGER.info("==============");
                Payment payment = new Payment();
                payment.setAppUser(loginInfo.getAppUser());
                payment.setAffiliateProgram(loginInfo.getAffiliateProgram());
                payment.setLoginInfo(loginInfo);

                for (int i = 0; i < tr.select("td").size(); i++) {
                    if (i == 0) {
                        payment.setStatus(tr.select("td").get(i).text());
                    }
                    if (i == 2) {
                        try {
                            payment.setPaymentSent(java.sql.Date.valueOf(tr.select("td").get(i).text()));
                        } catch (IllegalArgumentException e) {
                            // e.printStackTrace();
                        }
                    }
                    if (i == 3) {
                        payment.setAmount(Integer.parseInt((tr.select("td").get(i).text()).replaceAll("\\D", "")));
                    }
                    if (i == 4) {
                        payment.setMethod(tr.select("td").get(i).text());
                    }
                }
                if (payment.getMethod() != null && payment.getStatus() != null
                        && payment.getPaymentSent() != null) {
                } else if (payment.getPaymentSent() == null && tr.select("td").size() > 5) {
                    saveAccountBallance(payment);
                }
                payment = null;
            }
        }
    }

    private void saveAccountBallance(Payment payment) {
        AccountBalance accountBalance = new AccountBalance();
        accountBalance.setAmount(payment.getAmount().longValue());
        accountBalance.setDateAmount(new Date());
        accountBalance.setLoginInfo(loginInfo);
        accountBalanceRepository.save(accountBalance);//TODO сделать сервисом для всех
    }

    private void autorizationForm(WebDriver driver) {
        String waitThisCssTemplate = loginInfo.getAffiliateProgram().getCssSelector().getWaitingThisTemplate();
        String formTag = loginInfo.getAffiliateProgram().getCssSelector().getFormTag();
        String userField = loginInfo.getAffiliateProgram().getCssSelector().getUserField();
        String passwordField = loginInfo.getAffiliateProgram().getCssSelector().getPasswordField();
        String submitElement = loginInfo.getAffiliateProgram().getCssSelector().getSubmitElement();
        WebElement dynamicElement;
        WebElement formElem;
        WebElement userElem;
        WebElement passElem;
        WebElement submitButton;
        String password = new AES().decrypt(loginInfo.getPassword(), loginInfo.getAppUser().getPassword());

        driver.get(protocol + "://" + domain + "/" + loginInfo.getAffiliateProgram().getUri());
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        dynamicElement = driver.findElement(By.cssSelector(waitThisCssTemplate));

        formElem = driver.findElement(By.cssSelector(formTag));
        userElem = formElem.findElement(By.cssSelector(userField));
        userElem.clear();
        userElem.sendKeys(loginInfo.getLogin());//
        passElem = formElem.findElement(By.cssSelector(passwordField));
        passElem.clear();
        passElem.sendKeys(password);
//            WebElement submitButton = formElem.findElement(By.id("head-login"));//for brothersincash
        submitButton = formElem.findElement(By.cssSelector(submitElement));//for royal-cash
        submitButton.click();
    }
}