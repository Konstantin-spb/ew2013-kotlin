package net.ew2013.downloaders.Ccbill.subReports;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by konstantin on 07.03.14.
 */
public class FindOtherDate {

    public List<String> findENCAndExtraHTMLintoPage(String sourceHTMLFiles) {

        List<String> tagsInputForm = new ArrayList<>();
        Pattern p = Pattern.compile("<input type=hidden name=enc value=[\\\\\"']+([\\da-zA-Z]+)[\\\\\"']+>");
        Pattern p2 = Pattern.compile("<input type=hidden name=\\\\\"extraHTML\\\\\" value=\\\\\"([\\d\\S]+)\\\\\">");
        Matcher m = p.matcher(sourceHTMLFiles);
        Matcher m2 = p2.matcher(sourceHTMLFiles);
        if (m.find()) {
            tagsInputForm.add( m.group(1));
//            tagsInputForm[0] = m.group(1);
            if (m2.find()) {
                tagsInputForm.add(m2.group(1));
//                tagsInputForm[1] = m2.group(1);
            }
        }
        return tagsInputForm;
    }

    public String findDateENCForToolsTools(String sourceHTMLFile) {
        Pattern pattern = Pattern.compile("value='([\\da-zA-Z]+)'\\)");
        Matcher matcher = pattern.matcher(sourceHTMLFile);
        String valueDateENC = null;
        if (matcher.find()) {
            valueDateENC = matcher.group(1);
        }
        return valueDateENC;
    }

    public String getFormParams(String html) {

        Document doc = Jsoup.parse(html, "UTF-8");
        Element loginform = doc.getElementById("linkEnc");
        Elements inputElements = loginform.getElementsByTag("input");
        String valueDateENC = null;
        for (Element inputElement : inputElements) {
                valueDateENC = inputElement.attr("value");
            if (!valueDateENC.isEmpty()) {
                break;
            }
        }
        return valueDateENC;
    }

    public String findDateENC(String sourceHTMLFile) {
        String valueDateENC = null;
        Document doc = Jsoup.parse(sourceHTMLFile);
        String s;
        Elements cellBreakdownLink = doc.select("a.cellBreakdownLink");
        for (Element element : cellBreakdownLink) {
            if (element.text().trim().equalsIgnoreCase("date")) {
                s = element.attr("href");
                valueDateENC = (s.split(","))[1].replaceAll("[^\\d^\\w]", "");
            }
        }
        return valueDateENC;
    }

    public String findDataENCForReferredAffiliates(String s) {
        String valueDateENC = null;
        Pattern pattern = Pattern.compile("<a href=\"javascript:link\\('viewer\\.cgi\\?', '([\\da-zA-Z]+)'\\);\" class=cellLink>");
        Matcher matcher = pattern.matcher(s);
        if (matcher.find()) {
            valueDateENC = matcher.group(1);
        }
        return valueDateENC;
    }
}
