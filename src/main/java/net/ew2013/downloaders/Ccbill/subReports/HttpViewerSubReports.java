package net.ew2013.downloaders.Ccbill.subReports;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by konstantin on 07.03.14.
 */
public class HttpViewerSubReports {

	private CloseableHttpClient httpСlient;
	// StringBuilder error;

	public HttpViewerSubReports(CloseableHttpClient httpСlient) {
		// this.error = error;
		this.httpСlient = httpСlient;
	}

	public String httpViewer(List<String> tags) throws IOException {

		HttpPost httpost1 = new HttpPost("https://admin.ccbill.com/htmlviewer/viewer.cgi");
		String[] tempTags = new String[2];
		tempTags[0] = "";
		tempTags[1] = "";
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();

//		if (i == 1) {
			nvps.add(new BasicNameValuePair("enc", tags.get(0)));
//			nvps.add(new BasicNameValuePair("relative_path", relativePath));
			nvps.add(new BasicNameValuePair("relative_path", "/affiliateadmin/reports"));
			nvps.add(new BasicNameValuePair("brandDomain", "https%3A%2F%2Fccbill.com"));
			nvps.add(new BasicNameValuePair("groupbreakdownoptions", "false"));
			nvps.add(new BasicNameValuePair("followdefaultbreakdown", ""));
			nvps.add(new BasicNameValuePair("bypass", "false"));
			nvps.add(new BasicNameValuePair("reportHeight", "100%25"));
			nvps.add(new BasicNameValuePair("reportWidth", "100%25"));
			nvps.add(new BasicNameValuePair("paramNames", ""));
			nvps.add(new BasicNameValuePair("paramValues", ""));
			nvps.add(new BasicNameValuePair("extraHTML", tags.get(1)));
//		} else if (i == 2) {
//			nvps.add(new BasicNameValuePair("enc", tags[0]));
//		}

		httpost1.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));

		String responseFile;
		CloseableHttpResponse responsePost1 = null;
		HttpEntity entityPost1 = null;
		Document doc;
		Element innerLoginCenter = null;
		int count = 0;

		// for (int j = 0; j < 5; j++) {
//		PauseSleep.pause(10, 30);
		responsePost1 = httpСlient.execute(httpost1);
		entityPost1 = responsePost1.getEntity();
		responseFile = EntityUtils.toString(entityPost1);

		nvps = null;
		return responseFile;
	}

	public String httpViewerItemsPerPage(String tag, StringBuilder errors) {

		String responseFile = null;
		HttpPost httpost1 = new HttpPost("https://admin.ccbill.com/htmlviewer/viewer.cgi");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();

		int itemPerPage = 10000;

		nvps.add(new BasicNameValuePair("enc", tag));
		nvps.add(new BasicNameValuePair("page", "1"));
		nvps.add(new BasicNameValuePair("itemsPerPage", String.valueOf(itemPerPage)));

		CloseableHttpResponse responsePost1 = null;
		try {
			do {
				httpost1.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
				responsePost1 = httpСlient.execute(httpost1);
			} while (responsePost1 == null);
			HttpEntity entityPost1 = responsePost1.getEntity();
			responseFile = EntityUtils.toString(entityPost1);
			if (entityPost1 != null) {
				EntityUtils.consume(entityPost1);
			}
		} catch (IOException e) {
			errors.append(e);
		}
		return responseFile;
	}

//	public String getResponseFile() {
//		return responseFile;
//	}

//	public void setResponseFile(String responseFile) {
//		this.responseFile = responseFile;
//	}
}
