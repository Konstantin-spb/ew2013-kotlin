package net.ew2013.downloaders.Ccbill.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by konstantin on 12.03.14.
 */
public class CcbillListOfAP {

    private List<CcbillAffiliateProgramInfo> ccbillListOfAffiliatePrograms = new ArrayList<CcbillAffiliateProgramInfo>();

    public List<CcbillAffiliateProgramInfo> getCcbillListOfAffiliatePrograms() {
        return ccbillListOfAffiliatePrograms;
    }

    public void addNewApInList(CcbillAffiliateProgramInfo ap) {

        ccbillListOfAffiliatePrograms.add(ap);
    }
}