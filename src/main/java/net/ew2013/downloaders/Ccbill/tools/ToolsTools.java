package net.ew2013.downloaders.Ccbill.tools;

import net.ew2013.downloaders.Ccbill.subReports.FindOtherDate;
import net.ew2013.downloaders.Ccbill.subReports.HttpViewerSubReports;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class ToolsTools {

	private String fileResponse;
	private String mainCcbillAffiliateId;
	private StringBuilder errors;
	private CloseableHttpClient httpClient;



	private CcbillListOfAP ccbillListOfAP;

	public CcbillListOfAP getCcbillListOfAP() {
		return ccbillListOfAP;
	}

	public ToolsTools (CloseableHttpClient httpСlient, String mainCcbillAffiliateId) {
		this.httpClient = httpСlient;
		errors = new StringBuilder("");
//		this.mainCcbillAffiliateId = mainCcbillAffiliateId;
	}

	public void getApList() {
		String enc = null;
		try {
			List<String > tagsInputForm;
			FindOtherDate findOtherDate = null;
			HttpViewerSubReports httpViewerSubReports = null;
			try {
				findOtherDate = new FindOtherDate();
				httpViewerSubReports = new HttpViewerSubReports(httpClient);
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			for (int i = 0;; i++) {
				try {
					fileResponse = sendGetRequest(httpClient, "https://admin.ccbill.com/affiliateadmin/tools.cgi");
					tagsInputForm = findOtherDate.findENCAndExtraHTMLintoPage(fileResponse);
					fileResponse = httpViewerSubReports.httpViewer(tagsInputForm);
					enc = findOtherDate.getFormParams(fileResponse);
					fileResponse = httpViewerSubReports.httpViewerItemsPerPage(enc, errors);
					toolsReportParser(fileResponse);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (enc.length() != 0 || i >= 5) {
//					System.out.println(("Получили список ПП"));
					break;
				}
//				PauseSleep.pause(2,5);
			}
		} catch (Exception e) {
			System.err.println("Страница с программами не получена");
			e.printStackTrace();
		} finally {
			fileResponse = null;
		}
	}

	private String sendGetRequest(CloseableHttpClient httpClient, String url) throws IOException {
		HttpGet httpget = new HttpGet(url);
		HttpResponse responseGet = httpClient.execute(httpget);
		HttpEntity entityGet = responseGet.getEntity();
		return EntityUtils.toString(entityGet);
	}

	private void toolsReportParser(String fileResponse) {
		Document doc = Jsoup.parse(fileResponse, "UTF-8");
		ccbillListOfAP = new CcbillListOfAP();
		Element table = doc.select("table[class=reportTable]").first();
		Iterator<Element> rows = table.select("tr[class~=row\\d]").iterator();
		Iterator<Element> tds;
		Element tr;
		Element td;
		List<Element> rowsInCollection = IteratorUtils.toList(rows);
		List<Element> cells;

		for (int i = 0; i < rowsInCollection.size() - 1; i++) {
			ccbillListOfAP.addNewApInList(new CcbillAffiliateProgramInfo());
			tr = rowsInCollection.get(i);
			tds = tr.select("td[class=cell]").iterator();
			cells = IteratorUtils.toList(tds);

			for (int j = 0; j < cells.size() - 1; j++) {
				td = cells.get(j);
				ccbillListOfAP.getCcbillListOfAffiliatePrograms().get(i).setaProgramOneElement(td.text());
//				System.out.println(td.text());
			}
		}
	}

	public String getFileResponse() {
		return fileResponse;
	}
}
