package net.ew2013.downloaders.Ccbill.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by konstantin on 11.03.14.
 */
public class CcbillAffiliateProgramInfo {

    private List<String> aProgram = new ArrayList<String>();

    public List<String> getaProgram() {
        return aProgram;
    }

    public void setaProgramOneElement(String s) {

        aProgram.add(s);
    }
}