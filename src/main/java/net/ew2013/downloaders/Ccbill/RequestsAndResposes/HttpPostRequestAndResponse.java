package net.ew2013.downloaders.Ccbill.RequestsAndResposes;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * Created by konstantin on 01.04.14.
 */
public class HttpPostRequestAndResponse {

	private static final Logger LOGGER = LoggerFactory.getLogger(HttpPostRequestAndResponse.class);

	private String sourceTextFile = null;
	private CloseableHttpClient httpСlient;
	private StringBuilder errors;

	public String getSourceTextFile() {
		return sourceTextFile;
	}

	private Header[] header;

	public Header[] getHeader() {
		return header;
	}

	public HttpPostRequestAndResponse(String reportUrl, List<NameValuePair> nameValuePairs,
									  CloseableHttpClient httpСlient) throws IOException {

		// this.errors = errors;
		this.httpСlient = httpСlient;
		HttpPost httpPost = new HttpPost(reportUrl);
		CloseableHttpResponse responsePost = null;
		httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
		responsePost = httpСlient.execute(httpPost);
		HttpEntity entityPost = responsePost.getEntity();
		sourceTextFile = EntityUtils.toString(entityPost);

		header = responsePost.getHeaders("Location");
		responsePost.close();
	}
}