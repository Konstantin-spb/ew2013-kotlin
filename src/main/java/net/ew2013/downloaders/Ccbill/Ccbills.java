package net.ew2013.downloaders.Ccbill;

import net.ew2013.downloaders.Ccbill.RequestsAndResposes.HttpPostRequestAndResponse;
import net.ew2013.downloaders.Ccbill.reports.CcbillReports;
import net.ew2013.downloaders.Ccbill.tools.CcbillAffiliateProgramInfo;
import net.ew2013.downloaders.Ccbill.tools.ToolsTools;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.ServiceCollectionRunsThreads;
import net.ew2013.repository.ServiceCollectionRunsThreadsRepository;
import net.ew2013.security.AES;
import net.ew2013.service.ServiceCollectionRunsThreadsService;
import net.ew2013.service.SiteFromAffProgramsService;
import net.ew2013.service.SiteService;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

@Component
@Scope(value = "prototype")
public class Ccbills {

    @Autowired
    SiteFromAffProgramsService siteFromAffProgramsService;
    @Autowired
    ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Autowired
    ApplicationContext context;
    @Autowired
    SiteService siteService;
    @Autowired
    ServiceCollectionRunsThreadsService serviceCollectionRunsThreadsService;
    @Autowired
    ServiceCollectionRunsThreadsRepository serviceCollectionRunsThreadsRepository;

    // static final Logger LOGGER = Logger.getLogger(Ccbills.class);
    private static final Logger LOGGER = LoggerFactory.getLogger(Ccbills.class);
    private CloseableHttpClient httpСlient;
    private LocalDate lastDate;
    private LocalDate endDate;

    private LoginInfo loginInfo;

    public void setParams(LoginInfo loginInfo) {

        httpСlient = HttpClients.createDefault();
        this.loginInfo = loginInfo;
        this.lastDate = ((loginInfo.getLastDate()) != null
                ? new java.sql.Date(loginInfo.getLastDate().getTime()).toLocalDate().minusDays(1)
                : new java.sql.Date(loginInfo.getStartDate().getTime()).toLocalDate());
        this.endDate = LocalDate.now();
//        LOGGER.info("Enddate" + endDate);
    }

    private boolean authorizationOnTheServer() throws IOException {

        String url = "https://admin.ccbill.com/loginAMM.cgi";
        boolean authorization = false;
        List<NameValuePair> nvps = new ArrayList<>();

        nvps.add(new BasicNameValuePair("reloginenc", ""));
        nvps.add(new BasicNameValuePair("bookmark", ""));
        nvps.add(new BasicNameValuePair("password", new AES().decrypt(loginInfo.getPassword(), loginInfo.getAppUser().getPassword())));
        nvps.add(new BasicNameValuePair("testLoginSubmit", "1"));
        nvps.add(new BasicNameValuePair("enc", ""));
        nvps.add(new BasicNameValuePair("login", "1"));
        nvps.add(new BasicNameValuePair("loginTypeSelect", "Affiliate"));
        nvps.add(new BasicNameValuePair("affiliateId", loginInfo.getExternalAffiliateLoginId()));// Adrenal1ne
        nvps.add(new BasicNameValuePair("username", loginInfo.getLogin()));

        Map<String, String> data = new Hashtable<>();

        data.put("testLoginSubmit", "1");
        data.put("enc", "");
        data.put("reloginenc", "");
        data.put("login", "1");
        data.put("bookmark", "");
        data.put("loginTypeSelect", "Affiliate");
        data.put("affiliateId", loginInfo.getExternalAffiliateLoginId());// Adrenal1ne
        data.put("username", loginInfo.getLogin());
        data.put("password", new AES().decrypt(loginInfo.getPassword(), loginInfo.getAppUser().getPassword()));

//        AffiliateProgram affiliateProgram = new AffiliateProgram();
//        Connection.Response response = affiliateProgram.tryToAutorization(loginInfo, data);

        HttpPostRequestAndResponse hprar = new HttpPostRequestAndResponse(url, nvps, httpСlient);

//        HttpPost httpPost = new HttpPost(url);
//        httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));

        String updatePassword = "updatePassword";
        Header[] header = null;
        header = hprar.getHeader();
        Document doc = Jsoup.parse(hprar.getSourceTextFile(), "UTF-8");

//        Document doc = response.parse();

        Element alertText = doc.select("div.alert_text").first();

//        Map<String, String> head = response.headers();

        if (hprar.getSourceTextFile().isEmpty() && header != null
                && header[0].toString().equals("Location: megamenus/ccbillHome.html") && alertText == null) {
//        if (head != null) {
            LOGGER.info("Ccbill -> " + loginInfo.getExternalAffiliateLoginId() + " Login is true");
            authorization = true;
        } else if (header.length > 0) {
            if (header[0].toString().contains(updatePassword)) {
                LOGGER.warn("Ccbill -> " + loginInfo.getExternalAffiliateLoginId() + " Change your password please");
            }
        } else if (alertText != null) {
            // Element alertText = doc.select("div.alert_text").first();
            LOGGER.warn("Ccbill -> " + loginInfo.getExternalAffiliateLoginId() + " Alert from HTTP page - " + alertText.text());
        } else {
            LOGGER.info("Пока непонятная ошибка");
        }
        return authorization;
    }

    @Async("ccbillThreadPoolTaskExecutor")
    public void run() {
        Thread.currentThread().setName(loginInfo.getAffiliateProgram().getType() + "_" + loginInfo.getExternalAffiliateLoginId() + "_" + loginInfo.getAppUser().getLoginName());
        ServiceCollectionRunsThreads serviceCollectionRunsThreads = serviceCollectionRunsThreadsService.setServiceCollectionRunsThreads(null, loginInfo);

        boolean setupLastDate = false;
        Date startPeriodData = new Date();
        Date finishPeriodDate;
//        List<Callable<Object>> tasks = new ArrayList<>();
//        List<Future<Object>> invokeAll = null;

        String siteAffiliateID;
//        String clientID;
        String programName;
        ToolsTools toolsTools;

//        for (int i = 0; i < 5; i++) {

        try {
            boolean isAutorizated = authorizationOnTheServer();
            if (isAutorizated) {
                toolsTools = new ToolsTools(httpСlient, loginInfo.getExternalAffiliateLoginId());
                toolsTools.getApList();
                List<CcbillAffiliateProgramInfo> listOfAffiliatePrograms = toolsTools.getCcbillListOfAP().getCcbillListOfAffiliatePrograms();
                for (CcbillAffiliateProgramInfo ap : listOfAffiliatePrograms) {
                    siteAffiliateID = ap.getaProgram().get(0);
                    String clientId = ap.getaProgram().get(4);
                    programName = ap.getaProgram().get(6);

                    CcbillReports ccbillReports = context.getBean(CcbillReports.class);
                    ccbillReports.setParams(loginInfo, siteService.setOneSite(siteAffiliateID, programName, loginInfo), clientId, httpСlient);
                    ccbillReports.call();
                    clientId = null;
                }
            } else {
//			LOGGER.warn("Ccbill -> " + affiliateId + " Login failed. Need user help");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            serviceCollectionRunsThreadsRepository.delete(serviceCollectionRunsThreads);
        }
        finishPeriodDate = new Date();
//        LOGGER.info(loginInfo.getAffiliateLoginId() + " - скачал статистику со всех сайтов CCBILL за  " + ((finishPeriodDate.getTime() - startPeriodData.getTime()) / 1000F) + " sec");
    }
}