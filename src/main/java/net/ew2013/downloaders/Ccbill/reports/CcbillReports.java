package net.ew2013.downloaders.Ccbill.reports;

import net.ew2013.downloaders.Ccbill.RequestsAndResposes.HttpPostRequestAndResponse;
import net.ew2013.downloaders.Ccbill.subReports.FindOtherDate;
import net.ew2013.downloaders.Ccbill.subReports.HttpViewerSubReports;
import net.ew2013.model.*;
import net.ew2013.repository.GeneralStatRepository;
import net.ew2013.repository.LastdateDownloadStatRepository;
import net.ew2013.repository.ServiceCollectionRunsThreadsRepository;
import net.ew2013.repository.StatusRepository;
import net.ew2013.service.GeneralStatService;
import net.ew2013.service.LastdateDownloadStatService;
import net.ew2013.service.ServiceCollectionRunsThreadsService;
import net.ew2013.service.StatusService;
import net.ew2013.utils.DataFromDb;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Scope(value = "prototype")
public class CcbillReports {

    // static final Logger LOGGER = Logger.getLogger(CcbillReports.class);
    private static final Logger LOGGER = LoggerFactory.getLogger(CcbillReports.class);
    private CloseableHttpClient httpClient;

    private LoginInfo loginInfo;
    private SiteFromAffProgram siteFromAffProgram;
    private List<NameValuePair> nameValuePairs;
    private Map<String, String> data = new HashMap<>();
    private LocalDate lastDate;
    private StringBuilder errors;

    @Autowired
    ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Autowired
    LastdateDownloadStatService lastdateDownloadStatService;
    @Autowired
    GeneralStatService generalStatService;
    @Autowired
    StatusService statusService;
    @Autowired
    GeneralStatRepository generalStatRepository;
    @Autowired
    LastdateDownloadStatRepository lastdateDownloadStatRepository;
    @Autowired
    StatusRepository statusRepository;
    @Autowired
    ServiceCollectionRunsThreadsService serviceCollectionRunsThreadsService;
    @Autowired
    ServiceCollectionRunsThreadsRepository serviceCollectionRunsThreadsRepository;


    public void setParams(LoginInfo loginInfo, SiteFromAffProgram siteFromAffPrograms, String clientId, CloseableHttpClient httpClient) {

        this.loginInfo = loginInfo;
        this.siteFromAffProgram = siteFromAffPrograms;
        this.httpClient = httpClient;
        //TODO will make one method to all classes
        LastdateDownloadStat lastdateDownloadStat = lastdateDownloadStatService.getLastdateForSite(siteFromAffPrograms, loginInfo);
//        this.lastDate = (lastdateDownloadStatService.getLastdateForSite(siteFromAffPrograms, loginInfo) != null
//                ? new java.sql.Date(lastdateDownloadStatService.getLastdateForSite(siteFromAffPrograms, loginInfo).getLastDate().getTime()).toLocalDate()
//                : new java.sql.Date(loginInfo.getStartDate().getTime()).toLocalDate());

        this.lastDate = new java.sql.Date(new DataFromDb().getLastDate(siteFromAffProgram, loginInfo).getTime()).toLocalDate().minusDays(1);

        data.put("referringAccount", siteFromAffPrograms.getExternalSiteId() + "||" + clientId);
        data.put("bShowNoSales", "on");
        data.put("startDate", lastDate + "+00:00");
        data.put("endDate", LocalDate.now() + "+23:59");
        data.put("dateTo", "1");
        data.put("apply", "Lookup");

        nameValuePairs = new ArrayList<>();
        //<option value='2565859||946907-0000' >2565859 - BimboCash Affiliate Program</option>
        nameValuePairs.add(new BasicNameValuePair("referringAccount", siteFromAffPrograms.getExternalSiteId() + "||" + clientId));
//        nameValuePairs.add(new BasicNameValuePair("referringAccount", "2565859" + "||" + "946907-0000"));
        nameValuePairs.add(new BasicNameValuePair("bShowNoSales", "on"));
        nameValuePairs.add(new BasicNameValuePair("startDate", lastDate + "+00:00"));
        nameValuePairs.add(new BasicNameValuePair("endDate", LocalDate.now() + "+23:59"));
        nameValuePairs.add(new BasicNameValuePair("dateTo", "1"));
        nameValuePairs.add(new BasicNameValuePair("apply", "Lookup"));
        errors = new StringBuilder("");
    }

    private boolean transactionsAffiliate() throws SQLException {

        HttpViewerSubReports httpViewer = new HttpViewerSubReports(httpClient);
        FindOtherDate findOtherDate = new FindOtherDate();
        // String[] tagsInputForm = new String[2];// First ENC, Second ExtraHTML
        String sourceTextFile;
        String url = "https://admin.ccbill.com/affiliateadmin/reports/transactions.cgi";
        boolean isGoodTranactionReport = false;
        HttpPostRequestAndResponse httpPostRequestAndResponse;
        List<String> tagInputForm;

        try {
            httpPostRequestAndResponse = new HttpPostRequestAndResponse(url, nameValuePairs, httpClient);
            sourceTextFile = httpPostRequestAndResponse.getSourceTextFile();
            // LOGGER.info(affiliateID + " SourceTextFile получен");
            tagInputForm = findOtherDate.findENCAndExtraHTMLintoPage(sourceTextFile);
            // LOGGER.info("1");
            if (tagInputForm.size() == 2) {
                // tagsInputForm = findOtherDate.getTagsInputForm();
                sourceTextFile = httpViewer.httpViewer(tagInputForm);
                // httpViewer.setResponseFile(null);
//				LOGGER.info("1 " + loginInfo.getExternalAffiliateLoginId() + " - теги для получения статистики найдены");
                Document doc = Jsoup.parse(sourceTextFile);
                Element reportTable = doc.select("table.reportTable").first();
                if (sourceTextFile != null && !sourceTextFile.isEmpty() && reportTable != null) {
//					LOGGER.info("2 " + loginInfo.getExternalAffiliateLoginId() + " - " + "SourceFile получен");
                    String enc = findOtherDate.findDateENC(sourceTextFile);
                    tagInputForm.clear();
                    if (reportTable.toString().contains("* There are no items to display in the current view")) {
                        isGoodTranactionReport = true;
                        // LOGGER.info("3.2 " + loginInfo.getExternalAffiliateLoginId() + " - " + "no
                        // stats");
                    } else if (enc != null && !enc.isEmpty()) {
                        sourceTextFile = httpViewer.httpViewerItemsPerPage(enc, errors);
                        try {
                            parseTransactionsAffiliateAndAddResultIntoDB(sourceTextFile);
                            isGoodTranactionReport = true;
//							LOGGER.info("3.1 " + loginInfo.getExternalAffiliateLoginId() + " - " + "статистика скачана");
                        } catch (Exception e) {
                            e.printStackTrace();
//							LOGGER.info(loginInfo.getExternalAffiliateLoginId() + "Должна быть повторная авторизация");
                        }
                    } else {
                        errors.append(loginInfo.getExternalAffiliateLoginId()).append(" - ").append("ERROR STATS");
//						LOGGER.info(loginInfo.getExternalAffiliateLoginId()
//								+ " Ебанутый ccbill не смог сформировать и отдать правильную страницу");
                    }
                }
            } else {
                LOGGER.info(loginInfo.getExternalAffiliateLoginId() + " - " + "нет значений в tagInputForm");
            }
        } catch (Exception e) {
            LOGGER.info(loginInfo.getExternalAffiliateLoginId() + "SuperException. Возможно проблема с получением ответа от сетвера.");
            errors.append(loginInfo.getExternalAffiliateLoginId()).append(" - SuperException \n").append(e);
            // e.printStackTrace();
        } finally {
            sourceTextFile = null;
            // httpPostRequestAndResponse = null;
            httpViewer = null;
            findOtherDate = null;
            // tagsInputForm = null;
        }
        return isGoodTranactionReport;
    }

    private void parseTransactionsAffiliateAndAddResultIntoDB(String sourceTextFile) throws SQLException {

        Document doc = Jsoup.parse(sourceTextFile, "UTF-8");

        Element table = doc.select("table[class=reportTable]").first();
//        LOGGER.info(doc.toString());
        Iterator<Element> rows = table.select("tr[class~=row\\d]").iterator();
        Iterator<Element> tds;
        Element tr;
        Element td;

        List<Element> rowsInCollection = IteratorUtils.toList(rows);
        DateFormat formatterIn = new SimpleDateFormat("dd MMM yyyy", Locale.forLanguageTag("en"));
        DateFormat formatterOut = new SimpleDateFormat("yyyy-MM-dd", Locale.forLanguageTag("en"));
        Date date = null;
        List<GeneralStat> generalStatList = new ArrayList<>();

        long trial = 0;
        long trialCounter = 0;
        long single = 0;
        long singleCounter = 0;
        long reccuring = 0;
        long reccuringCounter = 0;
        long rebill = 0;
        long rebillCounter = 0;
        long refund = 0;
        long refundCounter = 0;
        long chargeback = 0;
        long chargebackCounter = 0;
        long voidC = 0;
        long voidCCounter = 0;

        long totalCounter = 0;
        long rawClicks = 0;
        long uniqueClicks = 0;
        String clicks;
        String query;

        for (int i = 0; i < rowsInCollection.size() - 1; i++) {
            tr = rowsInCollection.get(i);
            tds = tr.select("td[class=cell]").iterator();
            String tempTd;
            String[] tempArray;
            for (int j = 0; tds.hasNext(); j++) {
                td = tds.next();
                tempTd = td.text().trim();
                switch (j) {
                    case 0:
                        try {
                            date = formatterIn.parse(tempTd);
                        } catch (ParseException e) {
                            // LOGGER.info(tempTd);
                            LOGGER.info("=========================================================================");
                            // LOGGER.info(sourceTextFile);
                            LOGGER.info("=========================================================================");
                            e.printStackTrace();
                        }
                        break;
                    case 1:
                        tempArray = tempTd.split("\\s");
                        tempArray[0] = tempArray[0].replaceAll("\\D", "");
                        trial = Integer.parseInt(tempArray[0]);
                        trialCounter = Integer.parseInt(tempArray[1]);
                        break;
                    case 2:
                        tempArray = tempTd.split("\\s");
                        tempArray[0] = tempArray[0].replaceAll("\\D", "");
                        single = Integer.parseInt(tempArray[0]);
                        singleCounter = Integer.parseInt(tempArray[1]);
                        break;
                    case 3:
                        tempArray = tempTd.split("\\s");
                        tempArray[0] = tempArray[0].replaceAll("\\D", "");
                        reccuring = Integer.parseInt(tempArray[0]);
                        reccuringCounter = Integer.parseInt(tempArray[1]);
                        break;
                    case 4:
                        tempArray = tempTd.split("\\s");
                        tempArray[0] = tempArray[0].replaceAll("\\D", "");
                        rebill = Integer.parseInt(tempArray[0]);
                        rebillCounter = Integer.parseInt(tempArray[1]);
                        break;
                    case 5:
                        tempArray = tempTd.split("\\s");
                        tempArray[0] = tempArray[0].replaceAll("\\D", "");
                        refund = Integer.parseInt(tempArray[0]);
                        refundCounter = Integer.parseInt(tempArray[1]);
                        break;
                    case 6:
                        tempArray = tempTd.split("\\s");
                        tempArray[0] = tempArray[0].replaceAll("\\D", "");
                        chargeback = Integer.parseInt(tempArray[0]);
                        chargebackCounter = Integer.parseInt(tempArray[1]);
                        break;
                    case 7:
                        tempArray = tempTd.split("\\s");
                        tempArray[0] = tempArray[0].replaceAll("\\D", "");
                        voidC = Integer.parseInt(tempArray[0]);
                        voidCCounter = Integer.parseInt(tempArray[1]);
                        break;
                    case 9:
                        tempArray = tempTd.split("\\s");
                        tempArray[0] = tempArray[0].replaceAll("\\D", "");
                        rawClicks = Integer.parseInt(tempArray[0]);
                        break;
                    case 10:
                        tempArray = tempTd.split("\\s");
                        tempArray[0] = tempArray[0].replaceAll("\\D", "");
                        try {
                            uniqueClicks = Integer.parseInt(tempArray[0]);
                        } catch (NumberFormatException e) {
                            // e.printStackTrace();
                            LOGGER.debug("Ccbill -> " + loginInfo.getExternalAffiliateLoginId() + " В таблице отсутствует колонка uniqueClicks");
                        }
                        break;
                    case 11:
                        clicks = tempTd;
                        break;
                }
                tempTd = null;
            }

//            String loginForQuery = null;
//            String affiliateIDForQuery = null;
            String dateForQuery = null;
//            String apNameForQuery = null;
//            String siteNameForQuery = null;
            String siteIdForQuery = null;
            long rawClicksForQuery = 0;
            long uniqueClicksForQuery = 0;
            long saleCounterForQuery = 0;
            long salesMoneyForQuery = 0;
            long rebillCounterForQuery = 0;
            long rebillMoneyForQuery = 0;
            long refundChargebackCounterForQuery = 0;
            long refundChargebackMoneyForQuery = 0;
            long totalMoneyForQuery = 0;

//            String[] stringsParam = new String[2];
//            int[] intParams = new int[10];

            dateForQuery = (formatterOut.format(date));
            rawClicksForQuery = rawClicks;
            uniqueClicksForQuery = uniqueClicks;
            saleCounterForQuery = trialCounter + singleCounter + reccuringCounter;
            salesMoneyForQuery = trial + single + reccuring;
            rebillCounterForQuery = rebillCounter;
            rebillMoneyForQuery = rebill;
            refundChargebackCounterForQuery = refundCounter + chargebackCounter + voidCCounter;
            refundChargebackMoneyForQuery = (refund + chargeback + voidC) * -1;
            totalMoneyForQuery = trial + single + reccuring + rebill - (refund + chargeback + voidC);
            if (totalMoneyForQuery != 0) {
                LOGGER.info(String.valueOf(totalMoneyForQuery));
            }

            // stringsParam[0] = loginForQuery;
//            stringsParam[0] = dateForQuery;
            // stringsParam[1] = dateForQuery;
//            stringsParam[1] = siteIdForQuery;

            if (rawClicksForQuery != 0 || uniqueClicksForQuery != 0 || saleCounterForQuery != 0 || salesMoneyForQuery != 0
                    || rebillCounterForQuery != 0 || rebillMoneyForQuery != 0 || refundChargebackCounterForQuery != 0 ||
                    refundChargebackMoneyForQuery != 0 || totalMoneyForQuery != 0) {
                GeneralStat generalStat = new GeneralStat();
                generalStat.setAppUser(loginInfo.getAppUser());
                generalStat.setAffiliateProgram(loginInfo.getAffiliateProgram());
                generalStat.setLoginInfo(loginInfo);
                generalStat.setSiteFromAffProgram(siteFromAffProgram);
                generalStat.setStatsDate(java.sql.Date.valueOf(dateForQuery));
                generalStat.setRawClick(rawClicksForQuery);
                generalStat.setUniqueClicks(uniqueClicksForQuery);
                generalStat.setSignupCount(saleCounterForQuery);
                generalStat.setSignupMoney(salesMoneyForQuery);
                generalStat.setRebillCount(rebillCounterForQuery);
                generalStat.setRebillMoney(rebillMoneyForQuery);
                generalStat.setRefundChargebackCount(refundChargebackCounterForQuery);
                generalStat.setRefundChargebackMoney(refundChargebackMoneyForQuery);
                generalStat.setTotalMoney(totalMoneyForQuery);

                generalStatList.add(generalStat);
//                generalStatsService.insertStatsRow(generalStat);
//                LOGGER.info(loginInfo.getAffiliateProgram().getName() + " -> Transactions -> " + totalMoneyForQuery);
            }
        }
//        new AffiliateProgram().saveAllStats(generalStatList);
        saveAllStats(generalStatList);
    }

    public boolean referredAffiliates() throws SQLException {
        String url = "https://admin.ccbill.com/affiliateadmin/reports/referredAffiliates.cgi";
        String postParam = "/affiliateadmin/reports";
        HttpPostRequestAndResponse httpPostRequestAndResponse;

        HttpViewerSubReports httpViewer = new HttpViewerSubReports(httpClient);
        FindOtherDate findOtherDate = new FindOtherDate();
        String sourceTextFile;
        boolean isGoodReferredReport = false;
        List<String> tagInputForm;
        List<String> encs = new ArrayList<>();

        try {
            httpPostRequestAndResponse = new HttpPostRequestAndResponse(url, nameValuePairs, httpClient);
            sourceTextFile = httpPostRequestAndResponse.getSourceTextFile();
//            LOGGER.info(siteFromAffProgram.getExternalSiteId() + " SourceTextFile получен");
            tagInputForm = findOtherDate.findENCAndExtraHTMLintoPage(sourceTextFile);
            if (tagInputForm.size() == 2) {
                sourceTextFile = httpViewer.httpViewer(tagInputForm);
                // httpViewer.setResponseFile(null);
//                LOGGER.info("1 " + loginInfo.getExternalAffiliateLoginId() + " - теги для получения статистики найдены");
                Document doc = Jsoup.parse(sourceTextFile);
                Element reportTable = doc.select("table.reportTable").first();
                Elements rows = reportTable.select("a.cellLink");

                if (rows != null && rows.size() > 1) {
                    Pattern p = Pattern.compile("'([\\d\\w]{50,})'");
                    Matcher m;
                    for (Element row : rows) {
                        m = p.matcher(row.toString());
                        if (m.find()) {
//                            System.out.println(m.group());
                            encs.add(m.group().replaceAll("'", ""));
                        }
                    }

                    for (String ecp : encs) {
                        try {
                            sourceTextFile = httpViewer.httpViewerItemsPerPage(ecp, errors);
                            if (sourceTextFile != null && !sourceTextFile.isEmpty()) {
//                                LOGGER.info("22 " + loginInfo.getExternalAffiliateLoginId() + " - " + "SourceFile получен");
//                                String enc = findOtherDate.findDataENCForReferredAffiliates(sourceTextFile);
                                tagInputForm.clear();

                                Elements rowsss = Jsoup.parse(sourceTextFile).select("tr[class~=row\\d]");

                                if (sourceTextFile.contains("* There are no items to display in the current view")) {
                                    isGoodReferredReport = true;
//                                    LOGGER.info("33.2 " + loginInfo.getExternalAffiliateLoginId() + " - " + "no stats");
                                } else if (rowsss != null && rowsss.size() > 1) {
                                    try {
                                        parseReferredAffiliates(sourceTextFile);
                                        isGoodReferredReport = true;
//                                        LOGGER.info("33.1 " + loginInfo.getExternalAffiliateLoginId() + " - " + "статистика скачана");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        LOGGER.info(loginInfo.getExternalAffiliateLoginId() + "Должна быть повторная авторизация");
                                    }
                                } else {
                                    errors.append(loginInfo.getExternalAffiliateLoginId()).append(" - ").append("ERROR STATS");
                                    LOGGER.info(loginInfo.getExternalAffiliateLoginId()
                                            + " Ебанутый ccbill не смог сформировать и отдать правильную страницу");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    LOGGER.info("");

                } else {

                    if (sourceTextFile != null && !sourceTextFile.isEmpty() && reportTable != null) {
//                        LOGGER.info("2 " + loginInfo.getExternalAffiliateLoginId() + " - " + "SourceFile получен");
                        String enc = findOtherDate.findDataENCForReferredAffiliates(sourceTextFile);
                        tagInputForm.clear();

                        if (reportTable.toString().contains("* There are no items to display in the current view")) {
                            isGoodReferredReport = true;
//                            LOGGER.info("3.2 " + loginInfo.getExternalAffiliateLoginId() + " - " + "no stats");
                        } else if (enc != null && !enc.isEmpty()) {
                            sourceTextFile = httpViewer.httpViewerItemsPerPage(enc, errors);
                            // sourceTextFile = httpViewer.getResponseFile();
                            try {
                                parseReferredAffiliates(sourceTextFile);
                                isGoodReferredReport = true;
                                // if (loginInfo.getExternalAffiliateLoginId().equals("2011046")) {
                                // LOGGER.info(sourceTextFile);
                                // }
//                                LOGGER.info("3.1 " + loginInfo.getExternalAffiliateLoginId() + " - " + "статистика скачана");
                            } catch (Exception e) {
                                e.printStackTrace();
                                LOGGER.info(loginInfo.getExternalAffiliateLoginId() + "Должна быть повторная авторизация");
                            }
                        } else {
                            errors.append(loginInfo.getExternalAffiliateLoginId()).append(" - ").append("ERROR STATS");
                            LOGGER.info(loginInfo.getExternalAffiliateLoginId()
                                    + " Ебанутый ccbill не смог сформировать и отдать правильную страницу");
                        }
                    }
                }
            } else {
                LOGGER.info(loginInfo.getExternalAffiliateLoginId() + " - " + "нет значений в tagInputForm");
            }
        } catch (Exception e) {
            LOGGER.info(loginInfo.getExternalAffiliateLoginId() + "SuperException. Возможно проблема с получением ответа от сервера.");
            errors.append(loginInfo.getExternalAffiliateLoginId()).append(" - SuperException \n").append(e);
            e.printStackTrace();
        } finally {
            sourceTextFile = null;
            httpViewer = null;
            findOtherDate = null;
        }
        return isGoodReferredReport;
    }

    private void parseReferredAffiliates(String sourceTextFile) throws SQLException {

        Document doc;
        doc = Jsoup.parse(sourceTextFile, "UTF-8");

//        String query = null;

        Element table = doc.select("table[class=reportTable]").first();
        Iterator<Element> rows = table.select("tr[class~=row\\d]").iterator();
        Iterator<Element> tds;
        Element tr;
        Element td;
        List<Element> rowsInCollection = IteratorUtils.toList(rows);
        DateFormat formatterIn = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.forLanguageTag("en"));
        DateFormat formatterOut = new SimpleDateFormat("yyyy-MM-dd", Locale.forLanguageTag("en"));
        Date date = null;
        long amount = 0;

        for (int i = 0; i < rowsInCollection.size() - 1; i++) {
            tr = rowsInCollection.get(i);
            tds = tr.select("td[class=cell]").iterator();
            String tempTd;

            LOGGER.info(tds.toString());

            String[] tempArray;
            for (int j = 0; tds.hasNext(); j++) {
                td = tds.next();
                tempTd = td.text().trim();
                switch (j) {
                    case 1:
                        try {
                            date = formatterIn.parse(tempTd);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 3:
                        try {
                            tempArray = tempTd.split("\\s");
                            tempArray[0] = tempArray[0].replaceAll("[$.]?", "");
                            amount = Integer.parseInt(tempArray[0]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
                LOGGER.info(tempTd);
                tempTd = null;
            }

            if (amount != 0) {
                java.sql.Date statsDate = java.sql.Date.valueOf(formatterOut.format(date));
//                GeneralStat generalStat = generalStatDao.findStatsDateByUserLoginSite(loginInfo, siteFromAffProgram, statsDate);
                GeneralStat generalStat = generalStatService.findStatsDateByLoginSiteAp(loginInfo, siteFromAffProgram, statsDate);
                if (generalStat == null) {
                    generalStat = new GeneralStat();
                    generalStat.setAppUser(loginInfo.getAppUser());
                    generalStat.setAffiliateProgram(loginInfo.getAffiliateProgram());
                    generalStat.setLoginInfo(loginInfo);
                    generalStat.setSiteFromAffProgram(siteFromAffProgram);
                    generalStat.setStatsDate(java.sql.Date.valueOf(formatterOut.format(date)));
                    generalStat.setTotalMoney(amount);
                } else {
                    generalStat.setTotalMoney(generalStat.getTotalMoney() + amount);
                }
                generalStatService.saveOrUpdate(generalStat);
//                generalStatDao.saveOrUpdate(generalStat);
//                LOGGER.info(loginInfo.getAffiliateProgram().getName() + " -> Transactions -> " + amount);
            }
        }
    }

    private void logout() {
        HttpGet httpget = new HttpGet("https://admin.ccbill.com/loginMM.cgi");
        try {
            httpClient.execute(httpget);
        } catch (IOException e) {

        }
    }

    @Async(value = "ccbillThreadPoolTaskExecutor")
    public void call() {
        Thread.currentThread().setName(loginInfo.getAffiliateProgram().getType() + "_" + loginInfo.getExternalAffiliateLoginId() + "_" + loginInfo.getAppUser().getLoginName() + "_" + siteFromAffProgram.getSiteName());
        ServiceCollectionRunsThreads serviceCollectionRunsThreads = serviceCollectionRunsThreadsService.setServiceCollectionRunsThreads(siteFromAffProgram, loginInfo);
        Date startPeriodData = new Date();
        Date finishPeriodDate;
        boolean isGoodTransactionReport;
        boolean isGoodReferredReport;
        try {
            if (lastDate.compareTo(LocalDate.now()) != 0) {
                LOGGER.info("Start " + siteFromAffProgram.getExternalSiteId());
                isGoodTransactionReport = transactionsAffiliate();
                isGoodReferredReport = referredAffiliates();
                if (isGoodTransactionReport && isGoodReferredReport) {
                    finishPeriodDate = new Date();
                    LOGGER.info(loginInfo.getExternalAffiliateLoginId() + " downloaded "
                            + ((finishPeriodDate.getTime() - startPeriodData.getTime()) / 1000F) + " sec");
                    lastDate = LocalDate.now();

                    lastdateDownloadStatService.saveLastDateWithStatus(loginInfo.getAppUser(), loginInfo, siteFromAffProgram, "DONE", new Date());
                    //TODO Сделать как сервис, нехуй просто так как методу здесь болтаться
                }
            } else {
                LOGGER.info(loginInfo.getExternalAffiliateLoginId() + " - LastDate и Today равны ... качать не нужно");
            }
            LOGGER.info("Stop " + siteFromAffProgram.getExternalSiteId());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            serviceCollectionRunsThreadsRepository.delete(serviceCollectionRunsThreads);
        }
    }

    void saveAllStats(List<GeneralStat> statList) {
        for (GeneralStat g : statList) {
            // generalStatsService.insertStatsRow(g);
            GeneralStat generalStat = generalStatRepository.findByStatsDateAndAppUserAndSiteFromAffProgramAndLoginInfoAndAffiliateProgram(g.getStatsDate(), g.getAppUser(), g.getSiteFromAffProgram(), g.getLoginInfo(), g.getAffiliateProgram());
            if (generalStat == null) {
                generalStatRepository.save(g);
            }
        }
    }

    private void saveLastDate(LastdateDownloadStat lastdateDownloadStat) {
        LastdateDownloadStat ld = lastdateDownloadStatRepository.getLastdateForSite(siteFromAffProgram, loginInfo);

        if (ld != null) {
            ld.setLastDate(new Date());
            lastdateDownloadStat = ld;
        }
        lastdateDownloadStatRepository.save(lastdateDownloadStat);
    }
}