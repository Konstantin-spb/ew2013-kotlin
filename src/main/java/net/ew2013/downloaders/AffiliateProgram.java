package net.ew2013.downloaders;

import net.ew2013.model.GeneralStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.repository.GeneralStatRepository;
import net.ew2013.security.AES;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Component
@Scope(value = "prototype")
public class AffiliateProgram {

    @Autowired
    GeneralStatRepository generalStatRepository;

    private LoginInfo loginInfo;
    private Map<String, String> dataForLoginRequest;

    public AffiliateProgram() {
    }

    public AffiliateProgram(LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }

    public AffiliateProgram(LoginInfo loginInfo, Map<String, String> dataForLoginRequest) {
        this.loginInfo = loginInfo;
        this.dataForLoginRequest = dataForLoginRequest;
    }

    public Connection.Response tryToAutorization() {
        Connection connection = null;
        Connection.Response response = null;
//        String userField = loginInfo.getAffiliateProgram().getFormfieldUser();
//        String passwordField = loginInfo.getAffiliateProgram().getFormfieldPassword();
        String login = loginInfo.getLogin();
        String password = new AES().decrypt(loginInfo.getPassword(), loginInfo.getAppUser().getPassword());
        String url = loginInfo.getAffiliateProgram().getProtocol().getProtocolName()
                + "://"
                + loginInfo.getAffiliateProgram().getDomain().getDomainName()
                + "/"
                + loginInfo.getAffiliateProgram().getUri();
        try {
            connection = Jsoup.connect(url)
                    .userAgent("Ew2013-collect-stats-program")
                    .timeout(50 * 1000)
                    .method(Connection.Method.POST)
//                    .data(userField, login)
//                    .data(passwordField, password)
                    .validateTLSCertificates(false)
                    .followRedirects(true);
            if (dataForLoginRequest != null) {
                for (Map.Entry<String, String> d : dataForLoginRequest.entrySet()) {
                    connection.data(d.getKey(), d.getValue());
                }
            }

            response = connection.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

//    public Connection.Response tryToAutorization(Protocol p, Domain d, String uri, LoginInfo loginInfo, Map<String, String> datas) {

    @Transactional
    public void saveAllStats(List<GeneralStat> statList) {
        for (GeneralStat g : statList) {
            GeneralStat generalStat = generalStatRepository.findByStatsDateAndAppUserAndSiteFromAffProgramAndLoginInfoAndAffiliateProgram(g.getStatsDate(), g.getAppUser(), g.getSiteFromAffProgram(), g.getLoginInfo(), g.getAffiliateProgram());
            if (generalStat == null) {
                generalStatRepository.save(g);
            }
        }
    }
}
