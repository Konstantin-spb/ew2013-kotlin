package net.ew2013.downloaders;

import java.util.concurrent.ThreadFactory;

/**
 * Created by konstantin on 21.09.14.
 */
public class DaemonThreadFactory implements ThreadFactory {

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
//        t.setDaemon(true);
        t.setPriority(Thread.MIN_PRIORITY);
        return t;
    }
}
