package net.ew2013.downloaders;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.*;
import java.util.List;

/**
 * Created by konstantin on 06.10.16.
 */
public class PostRequest {

    private String sourceTextFile;

    public PostRequest() {
    }

    public Header[] sendRequest(CloseableHttpClient httpClient, List<NameValuePair> nvps, String urlForRequest) throws IOException {
        CloseableHttpResponse responsePost = null;

        int timeoutSeconds = 50;
        int CONNECTION_TIMEOUT_MS = timeoutSeconds * 1000; // Timeout in millis.
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
                .setConnectTimeout(CONNECTION_TIMEOUT_MS)
                .setSocketTimeout(CONNECTION_TIMEOUT_MS)
                .build();

        HttpPost httpPost;
        httpPost = new HttpPost(urlForRequest);
        httpPost.setConfig(requestConfig);
        httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
        responsePost = httpClient.execute(httpPost);
        HttpEntity entityPost = responsePost.getEntity();
        sourceTextFile = getStringFromInputStream(entityPost.getContent());
        Header[] headerLocation = responsePost.getHeaders("Location");
        responsePost.close();
        return headerLocation;
    }

    // convert InputStream to String
    private String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }

    public String getSourceTextFile() {
        return sourceTextFile;
    }
}
