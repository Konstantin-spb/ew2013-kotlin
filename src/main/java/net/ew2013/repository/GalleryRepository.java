package net.ew2013.repository;

import net.ew2013.model.AffiliateProgram;
import net.ew2013.model.Gallery;
import net.ew2013.model.LinkToContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

/**
 * Created by konstantin on 04.04.17.
 */
public interface GalleryRepository extends JpaRepository<Gallery, Long> {
	Gallery findByUri(String uri);

	Set<Gallery> findByAffiliateProgram(AffiliateProgram affiliateProgram);

	List<Gallery> findAllByAffiliateProgramOrderByLinkToContents(AffiliateProgram affiliateProgram);

	@Query(value = "SELECT g.id, g.description, g.sitename_id,g.affiliate_program_id,g.title,g.protocol_id,g.domain_id,g.uri,g.date FROM gallery g LEFT JOIN gallery_link_to_content gltc ON g.id = gltc.gallery_id LEFT JOIN link_to_content ltc ON gltc.link_to_content_id = ltc.id WHERE ltc.content_type_id ISNULL ;", nativeQuery = true)
	Set<Gallery> fingGalleryWithoutPicsOrVideo();

	@Query(value = "SELECT p.protocol_name,pr.protocol_name,d.domain_name,dm.domain_name,g.title,g.description,g.uri,ltc.image_link_uri,ap.name,t.tagname FROM gallery g " +
            "LEFT JOIN gallery_link_to_content gltc ON g.id = gltc.gallery_id " +
            "LEFT JOIN link_to_content ltc ON gltc.link_to_content_id = ltc.id " +
            "LEFT JOIN protocol p ON g.protocol_id = p.id LEFT JOIN protocol pr ON ltc.protocol_id = pr.id " +
            "LEFT JOIN domain d ON g.domain_id = d.id " +
            "LEFT JOIN domain dm ON ltc.domain_id = dm.id " +
            "LEFT JOIN gallery_tag gt on g.id = gt.gallery_id " +
            "LEFT JOIN tag t on gt.tag_id = t.id " +
            "LEFT JOIN affiliate_program ap ON g.affiliate_program_id = ap.id " +
            "ORDER BY g.date DESC, g.uri, ltc.content_type_id", nativeQuery = true)
	void getGalleries();
}
