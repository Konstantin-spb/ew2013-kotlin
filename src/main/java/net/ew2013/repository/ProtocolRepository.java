package net.ew2013.repository;

import net.ew2013.model.Protocol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by konstantin on 04.04.17.
 */
public interface ProtocolRepository extends JpaRepository<Protocol, Long> {
    @Query("select p from Protocol p where p.protocolName = :name")
    Protocol findByName(@Param("name") String name);
}
