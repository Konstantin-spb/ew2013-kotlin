package net.ew2013.repository;

import net.ew2013.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by konstantin on 04.04.17.
 */
public interface StatusRepository extends JpaRepository<Status, Long> {
    @Query("select s from Status s where s.status = :name")
    Status findByName(@Param("name") String name);
}
