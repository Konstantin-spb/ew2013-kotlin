package net.ew2013.repository;

import net.ew2013.model.AffiliateProgram;
import net.ew2013.model.AppUser;
import net.ew2013.model.LoginInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by konstantin on 04.04.17.
 */
public interface LoginInfoRepository extends JpaRepository<LoginInfo, Long> {
    List<LoginInfo> findByAppUserOrderByAffiliateProgram(AppUser user);
    LoginInfo findByAppUserAndAffiliateProgramAndLogin(AppUser user, AffiliateProgram ap, String login);

    LoginInfo findByIdAndAppUser(long id, AppUser appUser);

    List<LoginInfo> findAllByAppUser(AppUser appUser);
}
