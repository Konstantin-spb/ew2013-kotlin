package net.ew2013.repository;

import net.ew2013.model.ServiceCollectionRunsThreads;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceCollectionRunsThreadsRepository extends JpaRepository<ServiceCollectionRunsThreads, Long> {
}
