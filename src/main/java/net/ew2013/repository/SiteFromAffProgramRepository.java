package net.ew2013.repository;

import net.ew2013.model.AffiliateProgram;
import net.ew2013.model.SiteFromAffProgram;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by konstantin on 04.04.17.
 */
public interface SiteFromAffProgramRepository extends JpaRepository<SiteFromAffProgram, Integer> {

//    SiteFromAffProgram findByExternalSiteIdAfAndAffiliateProgram(String externalSiteId, AffiliateProgram affiliateProgram);//method with error
        SiteFromAffProgram findByAffiliateProgramAndExternalSiteId(AffiliateProgram af, String exId);
}
