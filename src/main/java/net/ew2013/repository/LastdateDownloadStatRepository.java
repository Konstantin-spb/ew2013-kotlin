package net.ew2013.repository;

import net.ew2013.model.AppUser;
import net.ew2013.model.LastdateDownloadStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by konstantin on 04.04.17.
 */
public interface LastdateDownloadStatRepository extends JpaRepository<LastdateDownloadStat, Long> {
    //TODO make good query
    @Query("select l from LastdateDownloadStat l where l.siteFromAffProgram = :site and l.loginInfo = :login")
    LastdateDownloadStat getLastdateForSite(@Param("site") SiteFromAffProgram site, @Param("login") LoginInfo loginInfo);

    List<LastdateDownloadStat> findAllByLoginInfo(LoginInfo loginInfo);

    List<LastdateDownloadStat> findAllByAppUser(AppUser appUser);
}
