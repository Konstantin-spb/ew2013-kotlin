package net.ew2013.repository;

import net.ew2013.model.AffiliateProgram;
import net.ew2013.model.PornModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PornModelRepository extends JpaRepository<PornModel, Long> {

    PornModel findByName(String name);
    PornModel findByNameAndAffiliateProgram(String name, AffiliateProgram ap);
}
