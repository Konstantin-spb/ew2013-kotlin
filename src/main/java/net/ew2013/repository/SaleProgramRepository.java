package net.ew2013.repository;

import net.ew2013.model.SaleProgram;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by konstantin on 04.04.17.
 */
public interface SaleProgramRepository extends JpaRepository<SaleProgram, Long> {
}
