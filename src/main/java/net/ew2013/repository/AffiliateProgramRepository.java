package net.ew2013.repository;

import net.ew2013.model.AffiliateProgram;
import net.ew2013.model.Domain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;

/**
 * Created by konstantin on 04.04.17.
 */
public interface AffiliateProgramRepository extends JpaRepository<AffiliateProgram, Long> {
    @Query("select a from AffiliateProgram a where a.name = :name")
    AffiliateProgram findByName(@Param("name") String name);

    @Query("select a from AffiliateProgram a where a.domain = :domain")
    AffiliateProgram findByDomain(@Param("domain") Domain domain);

    List<AffiliateProgram> findAllByOrderByNameAsc();
}
