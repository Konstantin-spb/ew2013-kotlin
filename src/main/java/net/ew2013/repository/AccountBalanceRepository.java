package net.ew2013.repository;

import net.ew2013.model.AccountBalance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by konstantin on 08.05.17.
 */
public interface AccountBalanceRepository extends JpaRepository<AccountBalance, Integer> {

}
