package net.ew2013.repository;

import net.ew2013.model.PersistentLogins;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by konstantin on 04.04.17.
 */
public interface PersistentLoginsRepository extends JpaRepository<PersistentLogins, Long> {
    @Query("select p from PersistentLogins p where p.username = :name")
    PersistentLogins findByName(@Param("name") String name);
}
