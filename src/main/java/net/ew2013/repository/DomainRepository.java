package net.ew2013.repository;

import net.ew2013.model.Domain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by konstantin on 04.04.17.
 */
public interface DomainRepository extends JpaRepository<Domain, Long> {
    @Query("select d from Domain d where d.domainName = :domainName")
    Domain findByName(@Param("domainName") String domainName);

//    Domain findByDomainNameContains(String domainName);
    Domain findByDomainName(String domainName);
}