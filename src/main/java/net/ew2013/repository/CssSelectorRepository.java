package net.ew2013.repository;

import net.ew2013.model.CssSelector;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CssSelectorRepository extends JpaRepository<CssSelector, Long> {
}
