package net.ew2013.repository;

import net.ew2013.model.AffiliateProgram;
import net.ew2013.model.AppUser;
import net.ew2013.model.GeneralStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by konstantin on 04.04.17.
 */
public interface GeneralStatRepository extends JpaRepository<GeneralStat, Long> {
	GeneralStat findByLoginInfoAndAffiliateProgramAndSiteFromAffProgramAndStatsDate(LoginInfo login, AffiliateProgram ap,
                                                                                    SiteFromAffProgram site, Date statDate);// For Ccbill queries

	GeneralStat findByStatsDateAndAppUserAndSiteFromAffProgramAndLoginInfoAndAffiliateProgram(Date d, AppUser u, SiteFromAffProgram site, LoginInfo l, AffiliateProgram ap);

	@Query(value = "SELECT" +
			"  log.login," +
			"  ap.name," +
			"  site.site_name," +
			"  SUM(stats.raw_click)               r," +
			"  SUM(stats.unique_clicks)           u," +
			"  SUM(stats.signup_count)            sc," +
			"  SUM(stats.signup_money)            sm," +
			"  SUM(stats.rebill_count)            rebc," +
			"  SUM(stats.rebill_money)            rebm," +
			"  SUM(stats.refund_chargeback_count) refc," +
			"  SUM(stats.refund_chargeback_money) refm," +
			"  SUM(stats.total_money)             tm," +
			"  s.status," +
			"  laststat.last_date" +
			" FROM login_info log" +
			"  LEFT JOIN affiliate_program ap ON log.affiliate_program_id = ap.id" +
			"  LEFT JOIN site_from_aff_program site ON ap.id = site.affiliate_program_id" +
			"   LEFT JOIN lastdate_download_stat laststat ON log.id = laststat.login_id AND site.id = laststat.site_id" +
			"   LEFT JOIN status s ON laststat.status_id = s.id" +
			"  LEFT OUTER JOIN general_stat stats" +
			"    ON log.id = stats.logininfo_id" +
			"       AND site.id = stats.site_from_aps_id" +
			"       AND stats.stats_date >= :startDate" +
			"       AND stats.stats_date <= :endDate" +
			" WHERE site.site_name NOTNULL AND log.app_user_id = :appUserId" +
			" GROUP BY LOG.login, ap.name, site.site_name,s.status,laststat.last_date" +
			" ORDER BY ap.name, log.login, site_name;", nativeQuery = true)
	List<Object[]> getStatsByDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("appUserId") long appUserId);
}
