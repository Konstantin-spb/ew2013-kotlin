package net.ew2013.repository;

import net.ew2013.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Integer> {
    Tag findByTagname(String tag);
}
