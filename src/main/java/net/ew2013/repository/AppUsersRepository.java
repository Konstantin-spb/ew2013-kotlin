package net.ew2013.repository;

import net.ew2013.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by konstantin on 04.04.17.
 */
public interface AppUsersRepository extends JpaRepository<AppUser, Long> {
    @Query("select s from AppUser s where s.loginName = :loginName")
    AppUser findByName(@Param("loginName") String loginName);

    List<AppUser> findAllByEmail(String email);
}
