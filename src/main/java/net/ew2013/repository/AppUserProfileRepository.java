package net.ew2013.repository;

import net.ew2013.model.AppUserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by konstantin on 04.04.17.
 */
public interface AppUserProfileRepository extends JpaRepository<AppUserProfile, Long> {
    @Query("select u from AppUserProfile u where u.type = :typeName")
    AppUserProfile findByType(@Param("typeName") String typeName);
}
