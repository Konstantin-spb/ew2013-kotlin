package net.ew2013.repository;

import net.ew2013.model.LinkToContent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LinkToContentRepository extends JpaRepository<LinkToContent, Long> {

    LinkToContent findByImageLinkUri(String uri);
}
