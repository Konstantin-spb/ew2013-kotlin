package net.ew2013.repository;

import net.ew2013.model.ContentType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContentTypeRepository  extends JpaRepository<ContentType, Long> {
}
