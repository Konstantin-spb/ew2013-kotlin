package net.ew2013.galleries;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by konstantin on 18.05.17.
 */
public class GalleryFromSite {
    private String apName;
    private String siteName;
    private List<String> tags = new ArrayList<>();
    private String mainDomain;
    private String uri;
    private String title;
    private String desc;
    private List<String> models = new ArrayList<>();
    private List<String> ImagesLinks = new ArrayList<>();
    private String mp4Link;

    public String getApName() {
        return apName;
    }

    public void setApName(String apName) {
        this.apName = apName;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getMainDomain() {
        return mainDomain;
    }

    public void setMainDomain(String mainDomain) {
        this.mainDomain = mainDomain;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<String> getModels() {
        return models;
    }

    public void setModels(List<String> models) {
        this.models = models;
    }

    public List<String> getImagesLinks() {
        return ImagesLinks;
    }

    public void setImagesLinks(List<String> imagesLinks) {
        ImagesLinks = imagesLinks;
    }

    public String getMp4Link() {
        return mp4Link;
    }

    public void setMp4Link(String mp4Link) {
        this.mp4Link = mp4Link;
    }
}
