package net.ew2013.galleries;

import net.ew2013.model.*;
import net.ew2013.repository.*;
import net.ew2013.service.AffiliateProgramService;
import net.ew2013.service.ProtocolService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by konstantin on 15.05.17.
 */
@Component
@Scope(value = "prototype")
public class NastyDollarsGalleries {

    @Autowired
    AffiliateProgramService affiliateProgramService;
    @Autowired
    ProtocolService protocolService;
    @Autowired
    ContentTypeRepository contentTypeRepository;
    @Autowired
    LinkToContentRepository linkToContentRepository;
    @Autowired
    DomainRepository domainRepository;
    @Autowired
    PornModelRepository pornModelRepository;
    @Autowired
    GalleryRepository galleryRepository;
    @Autowired
    TagRepository tagRepository;


    private Proxy proxy = new Proxy(                                      //
            Proxy.Type.HTTP,                                      //
            InetSocketAddress.createUnresolved("61.130.97.212", 8099) //
    );
    //    private String protocol = "http";
    private String mainDomain = "www.rk.com";
    private String firstPartOfUri = "/tour/videos";
    //    private List<GalleryFromSite> galleryList;
//    private AffiliateProgram affiliateProgram;


    private ConcurrentLinkedQueue countUnicGalleries = new ConcurrentLinkedQueue();

    private ConcurrentLinkedQueue countAllGalleries = new ConcurrentLinkedQueue();

    //    ConcurrentSkipListMap concurrentSkipListMap = new ConcurrentSkipListMap();
    private List<String[]> pageWithGalleriesLinks = new ArrayList<>();

    public List<String[]> getPageWithGalleriesLinks() {
        return pageWithGalleriesLinks;
    }

    //    @Async
    public Elements getGalleryLinks() throws IOException {
        Protocol protocol = protocolService.findOne(1L);
//        Domain domain = domainRepository.findByName(mainDomain);
        AffiliateProgram affiliateProgram = affiliateProgramService.findByName("NastyDollars");

        String startLink = protocol.getProtocolName() + "://" + mainDomain + firstPartOfUri + "/all-sites/all-categories/all-time/recent/";
        Document doc = Jsoup.connect(startLink)
//                .proxy(proxy)
                .timeout(50 * 1000)
                .get();
        //Получаем список сайтов
//        Elements sites = doc.select("select[data-list=collection] > option");
        Elements tags = doc.select("div[data-filter-type=tags] > div.filter-cols > a");//div class="filter-content collapse" data-filter-type="tags"
        doc = null;
        return tags;
    }

    public Elements getLinksBySiteAndNiches(Element e) throws IOException {
        Protocol protocol = protocolService.findOne(1L);
//        Domain domain = domainRepository.findByName(mainDomain);
        Document doc;
        //Получаем первую страницу сайта по всем категориям
        doc = Jsoup.connect(protocol.getProtocolName() + "://" + mainDomain + e.attr("data-url"))
//                .proxy(proxy)
                .timeout(50 * 1000)
                .get();
        //Получаем ссылки на категории по сайту
        Elements nameAndLinksOneSite = doc.select("div.filter-cols > a");

        return nameAndLinksOneSite;
    }

    //    @Async("threadPoolTaskExecutor")
    public List<String[]> getPagesLinksBySiteAndNiches(Element element, int threadNumber) throws IOException {
//        Thread.currentThread().setName("pageThread - " + threadNumber);
        Protocol protocol = protocolService.findOne(1L);
//        Domain domain = domainRepository.findByName(mainDomain);
        String tagName = element.text();
        Document doc = Jsoup.connect(protocol.getProtocolName() + "://" + mainDomain + element.attr("href"))
//                .proxy(proxy)
                .timeout(50 * 1000)
                .get();

        Element aTag = doc.select("ul.pagination > li > a").last();//получаем количество страниц
        int p = 1;
        if (aTag != null) {
            String[] pieses = aTag.attr("href").split("recent");
            p = Integer.parseInt(pieses[pieses.length - 1].replaceAll("\\D", ""));
            pieses = null;
        }
//        System.out.println(" ---> Page numder:" + p + "### TAG - (" + tagName + ")" + " - " + protocol.getProtocolName() + "://" + mainDomain + element.attr("href"));
        for (int i = 1; i <= p; i++) {
//            System.out.println(protocol.getProtocolName() + "://" + mainDomain + element.attr("href") + i);
//            countUnicGalleries.add(protocol.getProtocolName() + "://" + mainDomain + element.attr("href") + "/" + i);
//            concurrentSkipListMap.put(protocol.getProtocolName() + "://" + mainDomain + element.attr("href") + "/" + i, tagName);
            String[] galleryAndTag = new String[2];
            galleryAndTag[0] = element.attr("href") + i;
            galleryAndTag[1] = tagName;
            pageWithGalleriesLinks.add(galleryAndTag);
        }

        aTag = null;
        doc = null;
        System.out.println(Thread.currentThread().getName() + " - Работу закончил - " + pageWithGalleriesLinks.size());
        return pageWithGalleriesLinks;
    }

    @Async("threadPoolTaskExecutor")
    @Transactional
    public void getGalleryLinksBySiteAndNiches(String linkToPageWinthGalleries, String tagName) throws IOException {
        AffiliateProgram affiliateProgram = affiliateProgramService.findByName("NastyDollars");
        Protocol protocol = protocolService.findOne(1L);
        Domain domain = domainRepository.findByName(mainDomain);
        countAllGalleries.add(1);
        String galleryUri = "";
        Document doc = Jsoup.connect(protocol.getProtocolName() + "://" + mainDomain + linkToPageWinthGalleries)
//                .proxy(proxy)
                .timeout(50 * 1000)
                .get();
        Elements linksToGalleries = doc.select("div.card-info");
        for (int j = 0; j < linksToGalleries.size(); j++) {
            doc = null;
            String uri = linksToGalleries.get(j).select("h3 > a").attr("href");
            String galleryDate = linksToGalleries.get(j).select("div.card-info__meta > span").text();
            Tag tag;
            Set<Tag> tags;
//            System.out.println(uri + " - " + galleryDate);
            if (!uri.equals("/tour/join/")) {
                Gallery gallery = getNewOrExistsGallery(protocol, domain, affiliateProgram, uri);

                if (!galleryDate.isEmpty()) {
                    gallery.setDate(dateFormat(galleryDate));
                }
                tag = getNewOrExistsTag(tagName);
                tags = gallery.getTags();
                if (tags == null) {
                    tags = new HashSet<>();
                }
                tags.add(tag);
                gallery.setTags(tags);
                galleryRepository.save(gallery);
                galleryUri = null;
                tag = null;
                tags = null;
                gallery = null;
                galleryUri = "";
            }
            uri = null;
        }
        linksToGalleries = null;
        System.out.println("All gals - " + countAllGalleries.size() + "### Unic gals - " + countUnicGalleries.size());
    }

    @Async("threadPoolTaskExecutor")
    @Transactional
    public void updateGalleries(Gallery gallery) throws IOException {
        Protocol protocol = protocolService.findOne(1L);
        Domain domain = domainRepository.findByName("www.rk.com");
        AffiliateProgram affiliateProgram = affiliateProgramService.findByName("NastyDollars");

        String link = gallery.getProtocol().getProtocolName() + "://" + gallery.getDomain().getDomainName() + "/" + gallery.getUri();
        Document doc = Jsoup.connect(link)
//                .proxy(proxy)
                .timeout(50 * 1000)
                .get();
        countUnicGalleries.add(2);
        Element divClassTrailerDescTxt = doc.select("div#trailer-desc-txt").first();
        String description = null;
        String title = doc.select("h2.section_title").text();
        if (divClassTrailerDescTxt == null) {
            return;
        }
        Element desc = divClassTrailerDescTxt.select("div#trailer-desc-txt > p").first();
        if (desc != null) {
            description = desc.text();
        }
        Elements models = divClassTrailerDescTxt.select("h4 > a");


        Set<PornModel> pornModels = new HashSet<>();
        Set<LinkToContent> linkToContents = new HashSet<>();
        if (!description.isEmpty()) {
            gallery.setDescription(description);
        }
        if (!title.isEmpty()) {
            gallery.setTitle(title);
        }

        for (Element model : models) {
            //TODO проверить на наличие модели в базе
            PornModel pornModel = getNewOrExistsPornModel(model.text(), affiliateProgram);
            pornModel.setUriToModelPage(model.attr("href"));
            pornModel.setProtocol(protocol);
            pornModel.setDomain(domain);
            pornModelRepository.save(pornModel);
//            pornModelRepository.flush();
            pornModels.add(pornModel);
        }
        gallery.setPornModels(pornModels);

        ContentType contentType = contentTypeRepository.findOne(1L);
        for (Element im :
                doc.select("a.card-thumb__img")) {
            String linkToImage = im.attr("href");
            String uri = findWithRegExp(linkToImage, "http://.+?.com(.+.jpg)");
            String domainName = findWithRegExp(linkToImage, "http://(.+?.com)");
            Domain imageDomain = getNewOrExixtsDomain(domainName);
            LinkToContent linkToContent = getNewOrExistsContentLink(uri, gallery.getDomain(), gallery.getProtocol(), contentType);
            if (linkToContent.getGalleries().size() <= 0) {
                linkToContent.setContentType(contentType);
                linkToContent.setDomain(imageDomain);

                linkToContent.setProtocol(protocol);
                linkToContent.setImageLinkUri(uri.trim());

                linkToContentRepository.save(linkToContent);
                linkToContents.add(linkToContent);

//                System.out.println(domainName + uri);
            } else {
                System.out.println(uri + " - jpg есть в базе.");
            }
        }
        contentType = null;

        String mp4Link = findWithRegExp(doc.select("div.player-video").first().toString(), "(http:.+?mp4)").replaceAll("\\\\", "");

        if (!mp4Link.isEmpty()) {
            String uri = findWithRegExp(mp4Link, "http://.+?.com(.+)");
            contentType = contentTypeRepository.findOne(2L);
            LinkToContent linkToContent = getNewOrExistsContentLink(uri, gallery.getDomain(), gallery.getProtocol(), contentType);
            if (linkToContent.getImageLinkUri() == null) {
                linkToContent.setContentType(contentType);
                String domainName = findWithRegExp(mp4Link, "http://(.+?.com)");
//                linkToContent.setProtocol(gallery.getProtocol());
                linkToContent.setProtocol(protocol);
                linkToContent.setContentType(contentType);
                linkToContent.setImageLinkUri(uri.trim());
                Domain imageDomain = getNewOrExixtsDomain(domainName);
                linkToContent.setDomain(imageDomain);
                linkToContent.setImageLinkUri(uri);
                linkToContentRepository.save(linkToContent);
                linkToContents.add(linkToContent);
//                System.out.println(domainName + uri);
            } else {
                System.out.println(uri + " - mp4 есть в базе.");
            }
        } else {
            mp4Link = "MP4 NOT FOUND";
        }
        gallery.setLinkToContents(linkToContents);
        galleryRepository.save(gallery);
//        System.out.println(j + " | " + mp4Link + " | " + tagName + " | " + apName + " | " + siteName + " | " + mainDomain + linksToGalleries.get(j).attr("href") + " | " + linksToGalleries.get(j).text() + " | " + desc.text());
        linkToContents = null;
        contentType = null;
        System.out.println("### Unic gals - " + countUnicGalleries.size());
    }

//    @Async("threadPoolTaskExecutor")
    @Transactional
    public void updateModel(PornModel pm) throws IOException {
        Protocol protocol = pm.getProtocol();
        Domain domain = pm.getDomain();
        String uri = pm.getUriToModelPage();
        Document doc = Jsoup.connect(protocol.getProtocolName() + "://" + domain.getDomainName() + "/" + uri)
                .timeout(50 * 1000)
                .get();

//        String birthplace = doc.select("dl.model-bio__data > dd").text();
//        if (!birthplace.isEmpty()) {
//            pm.setBirthplace(birthplace);
//        }

        String desc = doc.select("div.biotext > p").text();
        if (!desc.isEmpty()) {
            pm.setDescription(desc);
        }
//        Element temp = doc.select("img.data-bind").first();
        String temp = doc.select("div.model-picture__thumb > img[data-bind]").toString();
        String linkToPortfolioImage = findWithRegExp(temp.toString(), "(http://.+?.jpg)");
        if (!linkToPortfolioImage.isEmpty()) {
            pm.setLinkToPortfolioImage(linkToPortfolioImage);
        }
        System.out.println(" ---> " + linkToPortfolioImage);
//        System.out.println(desc);
        pornModelRepository.save(pm);
//        pornModelRepository.flush();
    }

//    public void getLinks() throws IOException {
//
//        AffiliateProgram affiliateProgram = affiliateProgramService.findByName("NastyDollars");
//        Domain domain = domainRepository.findByName("www.rk.com");
//        if (domain == null) {
//            domain.setDomainName("www.rk.com");
//            domainRepository.saveAndFlush(domain);
//        }
//        Protocol protocol = protocolService.findOne(1L);
//        String startLink = protocol.getProtocolName() + "://" + mainDomain + firstPartOfUri + "/all-sites/all-categories/all-time/recent/";
//
//        Date startPeriodData = new Date();
//        Date finishPeriodDate;
//        String apName = "NastyDollars";
//
//        Document doc = Jsoup.connect(startLink).get();
//        //Получаем список сайтов
//        Elements sites = doc.select("select[data-list=collection] > option");
//
//        doc = null;
//
//        for (Element e : sites) {
//            //Получаем первую страницу сайта по всем категориям
//            doc = Jsoup.connect(protocol.getProtocolName() + "://" + mainDomain + e.attr("data-url")).get();
//            String siteName = e.text();
//
//            if (e.attr("data-keyword").equalsIgnoreCase("all sites")) {
//                continue;
//            }
//            //Получаем ссылки на категории по сайту
//            Elements nameAndLinks = doc.select("div.filter-cols > a");
//
//            for (Element element : nameAndLinks) {
//                String tagName = element.text();
//                doc = Jsoup.connect(protocol.getProtocolName() + "://" + mainDomain + element.attr("href")).get();
//
//                Element aTag = doc.select("ul.pagination > li > a").last();//получаем количество страниц
//                int p = 1;
//                if (aTag != null) {
//                    String href = aTag.attr("href");
//                    String[] pieses = href.split("recent");
//                    p = Integer.parseInt(pieses[pieses.length - 1].replaceAll("\\D", ""));
//                }
//                System.out.println(siteName + " ---> " + p + " TAG - (" + tagName + ")" + " - " + protocol.getProtocolName() + "://" + mainDomain + element.attr("href"));
//
//                doc = null;
//
//                for (int i = 1; i <= p; i++) {
//                    System.out.println("==================PAGE NuMBER - " + i + " start - tag ->" + tagName);
//                    doc = Jsoup.connect(protocol.getProtocolName() + "://" + mainDomain + element.attr("href") + "/" + i).get();
//                    Elements linksToGalleries = doc.select("div.card-info > h3 > a");
//                    for (int j = 0; j < linksToGalleries.size(); j++) {
//                        doc = null;
//                        doc = Jsoup.connect(protocol.getProtocolName() + "://" + mainDomain + linksToGalleries.get(j).attr("href")).get();
//                        Gallery gallery = galleryRepository.findByUri(linksToGalleries.get(j).attr("href"));
//
////                        boolean galleryExist = false;
//                        Tag tag = getNewOrExistsTag(tagName);
//                        Set<Gallery> galleries = new HashSet<>();
//                        Set<PornModel> pornModels = new HashSet<>();
//                        Set<Tag> tags = new HashSet<>();
//                        Set<LinkToContent> linkToContents = new HashSet<>();
//                        String description = "";
//                        if (gallery != null) {
//                            tags.add(tag);
//                            gallery.setTags(tags);
//                            tags = null;
//                            tag = null;
//                            break;
//                        }
//
//                        Element divClassTrailerDescTxt = doc.select("div#trailer-desc-txt").first();
//                        if (divClassTrailerDescTxt == null) {
//                            continue;
//                        }
//                        Element desc = divClassTrailerDescTxt.select("div#trailer-desc-txt > p").first();
//                        if (desc != null) {
//                            description = desc.text();
//                        }
//                        Elements models = divClassTrailerDescTxt.select("h4 > a");
//
//                        gallery = new Gallery();
//                        gallery.setAffiliateProgram(affiliateProgram);
//                        gallery.setDomain(domain);
//                        gallery.setProtocol(protocol);
//                        gallery.setUri(linksToGalleries.get(j).attr("href"));
//                        gallery.setDescription(description);
//                        gallery.setTitle(linksToGalleries.get(j).text());
//                        gallery.setTags(tags);
//                        tag = null;
//
//                        for (Element model : models) {
//                            //TODO проверить на наличие модели в базе
//                            PornModel pornModel = getNewOrExistsPornModel(model.text());
//                            pornModels.add(pornModel);
//                        }
//                        gallery.setPornModels(pornModels);
//
//                        ContentType contentType = contentTypeRepository.findOne(1L);
//                        for (Element im :
//                                doc.select("a.card-thumb__img")) {
//                            String linkToImage = im.attr("href");
//                            String uri = findWithRegExp(linkToImage, "http://.+?.com(.+.jpg)");
//                            String domainName = findWithRegExp(linkToImage, "http://(.+?.com)");
//                            Domain imageDomain = getNewOrExixtsDomain(domainName);
//                            LinkToContent linkToContent = getNewOrExistsContentLink(uri, domain, protocol, contentType);
//                            if (linkToContent.getGalleries().size() <= 0) {
//                                linkToContent.setContentType(contentType);
////                                Domain imageDomain = getNewOrExixtsDomain(domainName);
//                                linkToContent.setDomain(imageDomain);
//                                linkToContent.setImageLinkUri(uri);
//                                linkToContentRepository.saveAndFlush(linkToContent);
//                                linkToContents.add(linkToContent);
//                                System.out.println(domainName + uri);
//                            } else {
//                                System.out.println(uri + " - URI есть в базе.");
//                            }
//                        }
//                        contentType = null;
//
//                        String mp4Link = findWithRegExp(doc.select("div.player-video").first().toString(), "(http:.+?mp4)").replaceAll("\\\\", "");
//
//                        if (!mp4Link.isEmpty()) {
//                            String uri = findWithRegExp(mp4Link, "http://.+?.com(.+)");
//                            contentType = contentTypeRepository.findOne(2L);
//                            LinkToContent linkToContent = getNewOrExistsContentLink(uri, domain, protocol, contentType);
//                            if (linkToContent == null) {
//                                linkToContent.setContentType(contentType);
//                                String domainName = findWithRegExp(mp4Link, "http://(.+?.com)");
//                                linkToContent.setProtocol(protocol);
//
//                                Domain imageDomain = getNewOrExixtsDomain(domainName);
//                                linkToContent.setDomain(imageDomain);
//                                linkToContent.setImageLinkUri(uri);
//                                linkToContentRepository.saveAndFlush(linkToContent);
//                                linkToContents.add(linkToContent);
//                                System.out.println(domainName + uri);
//                            } else {
//                                System.out.println(uri + " - URI есть в базе.");
//                            }
//                        } else {
//                            mp4Link = "MP4 NOT FOUND";
//                        }
//                        gallery.setLinkToContents(linkToContents);
//                        galleryRepository.saveAndFlush(gallery);
//                        System.out.println(j + " | " + mp4Link + " | " + tagName + " | " + apName + " | " + siteName + " | " + mainDomain + linksToGalleries.get(j).attr("href") + " | " + linksToGalleries.get(j).text() + " | " + desc.text());
//                        linkToContents = null;
//                        contentType = null;
//                    }
//                    System.out.println("==================PAGE NuMBER - " + i + " stoptag ->" + tagName);
//                }
//            }
//            doc = null;
//        }
//        finishPeriodDate = new Date();
//        System.out.println(((finishPeriodDate.getTime() - startPeriodData.getTime()) / 1000F));
//    }

    private String findWithRegExp(String inputString, String pattern) {
        //вопрос делает ленивый поиск в данном случае https://learn.javascript.ru/regexp-greedy-and-lazy
        String answer = "";
        Pattern ptrn = Pattern.compile(pattern);
        Matcher matcher = ptrn.matcher(inputString);
        if (matcher.find()) {
            answer = matcher.group(1);
        }
        return answer;
    }

    private Tag getNewOrExistsTag(String tagName) {
        Tag tag = tagRepository.findByTagname(tagName);
        if (tag == null) {
            tag = new Tag();
            tag.setTagname(tagName.trim());
            tagRepository.save(tag);
        }
        return tag;
    }

    private synchronized PornModel getNewOrExistsPornModel(String pornModelName, AffiliateProgram ap) {
        System.out.println(pornModelName);
        PornModel pornModel = pornModelRepository.findByNameAndAffiliateProgram(pornModelName, ap);
        if (pornModel == null) {
            pornModel = new PornModel();
            pornModel.setAffiliateProgram(ap);
            pornModel.setName(pornModelName.trim());
        }
        return pornModel;
    }

    synchronized LinkToContent getNewOrExistsContentLink(String uri, Domain domain, Protocol protocol, ContentType contentType) {
        LinkToContent linkToContent = linkToContentRepository.findByImageLinkUri(uri);
        if (linkToContent == null) {
            linkToContent = new LinkToContent();
        }
        return linkToContent;
    }

    Domain getNewOrExixtsDomain(String domainName) {
        Domain domain = domainRepository.findByName(domainName);
        if (domain == null) {
            domain = new Domain();
            domain.setDomainName(domainName);
            domainRepository.save(domain);
        }
        return domain;
    }

    Gallery getNewOrExistsGallery(Protocol p, Domain d, AffiliateProgram ap, String uri) {
        Gallery gallery = galleryRepository.findByUri(uri);
        if (gallery == null) {
            countUnicGalleries.add(2);
            gallery = new Gallery();
            gallery.setProtocol(p);
            gallery.setDomain(d);
            gallery.setAffiliateProgram(ap);
            gallery.setUri(uri);
//            galleryRepository.saveAndFlush(gallery);
        }
        return gallery;
    }

    Date dateFormat(String date) {
        Date ourDate = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM d,yyyy", Locale.ENGLISH);

        try {
            ourDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        System.out.println("Constructor 4: " + dateFormat.format(ourDate));
        return ourDate;
    }

    public ConcurrentLinkedQueue getCountUnicGalleries() {
        return countUnicGalleries;
    }

    public ConcurrentLinkedQueue getCountAllGalleries() {
        return countAllGalleries;
    }
}
