package net.ew2013.converter;

import net.ew2013.model.AppUserProfile;
import net.ew2013.repository.AppUserProfileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * A converter class used in views to map id's to actual userProfile objects.
 */
@Component
public class RoleToUserProfileConverter implements Converter<Object, AppUserProfile> {

    static final Logger logger = LoggerFactory.getLogger(RoleToUserProfileConverter.class);

    @Autowired
    AppUserProfileRepository appUserProfileRepository;

    /**
     * Gets AppUserProfile by Id
     *
     * @see Converter#convert(Object)
     */
    public AppUserProfile convert(Object element) {
        Long id = Long.parseLong((String) element);
        AppUserProfile profile = appUserProfileRepository.findOne(id);
        logger.info("Profile : {}", profile);
        return profile;
    }
}