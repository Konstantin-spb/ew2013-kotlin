package net.ew2013.utils;

import net.ew2013.model.LastdateDownloadStat;
import net.ew2013.model.LoginInfo;
import net.ew2013.model.SiteFromAffProgram;

import java.util.Date;

/**
 * Created by konstantin on 31.03.17.
 */
public class DataFromDb {

    public Date getLastDate(SiteFromAffProgram siteFromAffProgram, LoginInfo loginInfo) {
        Date lastDate = null;

        if (siteFromAffProgram.getLastdateDownloadStats() != null && siteFromAffProgram.getLastdateDownloadStats().size() == 1) {
            for (LastdateDownloadStat lastdateDownloadStat : siteFromAffProgram.getLastdateDownloadStats()) {
                lastDate = lastdateDownloadStat.getLastDate();
            }
        } else {
            lastDate = loginInfo.getStartDate();
        }
        return lastDate;
    }
}
