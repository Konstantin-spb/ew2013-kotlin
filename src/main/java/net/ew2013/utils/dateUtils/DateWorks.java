package net.ew2013.utils.dateUtils;

import org.joda.time.DateTime;

import java.time.LocalDate;

/**
 * Created by konstantin on 28.07.15.
 */
public class DateWorks {

    public static LocalDate getLastDayOfMonth() {
        DateTime dt = new DateTime();
        int maxDay = dt.dayOfMonth().getMaximumValue();

        return LocalDate.of(dt.getYear(), dt.getMonthOfYear(), maxDay);
    }
}
