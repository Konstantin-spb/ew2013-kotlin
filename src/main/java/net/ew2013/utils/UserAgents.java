package net.ew2013.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by konstantin on 14.10.15.
 */
public class UserAgents {

    private List<String> fileLines = new ArrayList<String>();
    private Random random = new Random();

    // в конструкторе считываем строки из файла и ложим их в наш ArrayList
    public UserAgents()
    {
        try {
            fileLines = FileUtils.readLines(new File("/home/konstantin/IdeaProjects/ew2013/userAgents.txt"));
        } catch (IOException e) {
            e.printStackTrace();  //Вах вах вах... Ошибка.
//            System.exit(0); //Нет пути! Всё тлен. Мы должны уйти.
        }
    }

    //ну а дальше всё просто.
    public String getRandomLine()
    {
        return fileLines.get(random.nextInt(fileLines.size()+1));
    }

}
