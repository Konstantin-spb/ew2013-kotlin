package net.ew2013.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by konstantin on 13.10.15.
 */
public class PauseSleep {
    private static final Logger LOGGER = LoggerFactory.getLogger(PauseSleep.class);

    public static void pause(int min, int max) {
        try {
            long pauseTime = (long) ((min + Math.random() * (max - min) + 1) * 1000);
//            LOGGER.info("Pause " + pauseTime / 1000 + " sec.");
            Thread.sleep(pauseTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}