package net.ew2013.exceptions;

/**
 * Created by konstantin on 10.08.16.
 */

import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;
import java.sql.SQLException;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(SQLException.class)
    public void handlerIOException() {
        System.err.println("SQLException here");
    }
}
