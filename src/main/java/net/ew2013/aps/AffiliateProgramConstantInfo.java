package net.ew2013.aps;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class AffiliateProgramConstantInfo {

    @Min(0)
    private long id;
    private long affiliateProgramId;
    private String affiliateProgramName;
    private String startDate;
    private String login;
    private String affiliateId;//for ccbill

    @NotNull
//    @Min(1)
    private String password;

    public long getAffiliateProgramId() {
        return affiliateProgramId;
    }

    public void setAffiliateProgramId(long affiliateProgramId) {
        this.affiliateProgramId = affiliateProgramId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAffiliateProgramName() {
        return affiliateProgramName;
    }

    public void setAffiliateProgramName(String affiliateProgramName) {
        this.affiliateProgramName = affiliateProgramName;
    }

    public String getAffiliateId() {
        return affiliateId;
    }

    public void setAffiliateId(String affiliateId) {
        this.affiliateId = affiliateId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
