package net.ew2013.controller;

import net.ew2013.downloaders.galleries.GalleriesDumperNats41;
import net.ew2013.galleries.NastyDollarsGalleries;
import net.ew2013.model.*;
import net.ew2013.repository.GalleryRepository;
import net.ew2013.repository.PornModelRepository;
import net.ew2013.service.AffiliateProgramService;
import net.ew2013.service.LoginInfoService;
import net.ew2013.service.UsersService;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by konstantin on 10.03.17.
 */
@Controller
//@Scope(value = "prototype")
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    GalleriesDumperNats41 dumperNats41;
    @Autowired
    AffiliateProgramService affiliateProgramService;
    @Autowired
    LoginInfoService loginInfoService;
    @Autowired
    @Qualifier("threadPoolTaskExecutor")
    ThreadPoolTaskExecutor executor;
    @Autowired
    UsersService userService;

    /**
     * This method will list all existing users.
     */
    @RequestMapping(value = {"/list" }, method = RequestMethod.GET)
    public String listUsers(ModelMap model) {

        List<AppUser> users = userService.findAll();
        model.addAttribute("users", users);
        model.addAttribute("loggedinuser", userService.getPrincipal());
        return "/admin/userslist";
    }

    @RequestMapping("/panel")
    public String showAdminPanel(Model model) {
//        List<AffiliateProgram> programList = affiliateProgramService.findAll();
//        model.addAttribute("programList", programList);
//        model.addAttribute("programList", affiliateProgramService.getAfProgramWithSalePrograms());
        model.addAttribute("executorCounter",executor.getActiveCount());
        return "/admin/panel";
    }

//    @Async
    @RequestMapping(value = "/get-programs-info", method = RequestMethod.GET)
    public String getProgramInfo() {

        List<LoginInfo> programList = loginInfoService.findAll();
        for (LoginInfo l: programList) {
            dumperNats41.setParams(l);
            dumperNats41.getSiteOrProgramInfo("link_program");
        }

        System.out.println("That's all");
        return "redirect:/admin/panel";
    }

//    @Async
    @RequestMapping(value = "/get-site-info", method = RequestMethod.GET)
    public String getSiteInfo() {

        List<LoginInfo> programList = loginInfoService.findAll();
        for (LoginInfo l: programList) {
            dumperNats41.setParams(l);
            dumperNats41.getSiteOrProgramInfo("link_site");
        }

        System.out.println("That's all");
        return "redirect:/admin/panel";
    }
}
