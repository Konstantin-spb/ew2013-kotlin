package net.ew2013.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.ew2013.aps.AffiliateProgramConstantInfo;
import net.ew2013.model.*;
import net.ew2013.repository.*;
import net.ew2013.security.AES;
import net.ew2013.service.*;
import net.ew2013.sqlQueries.SqlQueries;
import net.ew2013.sqlQueries.wrapSqlQueries.ApLoginBalance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;

/**
 * Created by konstantin on 04.08.16.
 */
@Controller
@Scope(value = "prototype")
@RequestMapping("/users")
public class UserController {

    @Autowired
    UsersService usersService;
    @Autowired
    LoginInfoService loginInfoService;
    @Autowired
    DownloadService downloadService;
    @Autowired
    GeneralStatService generalStatService;
    @Autowired
    AffiliateProgramRepository affiliateProgramRepository;
    @Autowired
    LoginInfoRepository loginInfoRepository;
    @Autowired
    AffiliateProgramService affiliateProgramService;
    @Autowired
    AppUsersRepository appUsersRepository;
    @Autowired
    GeneralStatRepository generalStatRepository;
    @Autowired
    AutorizationHistoryRepository autorizationHistoryRepository;
    @Autowired
    SqlQueries sqlQueries;
    @Autowired
    ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @RequestMapping(value = "/{currentuser}")
    public String userStartPage(@PathVariable String currentuser, Model model) {

        threadPoolTaskExecutor.getThreadPoolExecutor().purge();
        for (String s: AppController.lisfOfThreads) {
            System.out.println(s);
        }
//        System.out.println();
        AppUser user = usersService.findByName(usersService.getPrincipal());
        if ( currentuser.equals(user.getLoginName())) {
            List<ApLoginBalance> apLoginBalanceList = sqlQueries.getApLoginBalance(user.getId());

            model.addAttribute("myUser", user);
            model.addAttribute("apLoginBalance", apLoginBalanceList);
            AutorizationHistory autorizationHistory = new AutorizationHistory();
            autorizationHistory.setAppUser(user);
            autorizationHistory.setDateLogin(new java.util.Date());
            autorizationHistoryRepository.save(autorizationHistory);
            return "/user/personalpage";
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/{currentuser}/add-new-affiliate-program-into-your-list", method = RequestMethod.GET)
    public String addNewAp(Model model) {

//        List<AffiliateProgram> affiliatePrograms = affiliateProgramService.findAll();
        List<AffiliateProgram> affiliatePrograms = affiliateProgramRepository.findAllByOrderByNameAsc();
        model.addAttribute("myUser", usersService.findByName(usersService.getPrincipal()));
        model.addAttribute("affiliateProgramList", affiliatePrograms);
        model.addAttribute(new AffiliateProgramConstantInfo());
        return "/user/add-or-edit-affiliate-program";
    }

    @RequestMapping(value = "/{currentuser}/{affiliateProgram}/{login}/edit", method = RequestMethod.GET)
    public String editLoginInfo(@PathVariable String login, @PathVariable String affiliateProgram, Model model) {

        List<AffiliateProgram> affiliatePrograms = affiliateProgramService.findAll();
        LoginInfo loginInfo = loginInfoService.getReamLoginFromDb(login, affiliateProgram);
        AffiliateProgramConstantInfo affiliateProgramConstantInfo = new AffiliateProgramConstantInfo();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = formatter.format(loginInfo.getStartDate());

        affiliateProgramConstantInfo.setStartDate(String.valueOf(formattedDate));
        affiliateProgramConstantInfo.setLogin(loginInfo.getLogin());
        affiliateProgramConstantInfo.setAffiliateProgramName(loginInfo.getAffiliateProgram().getName());
        affiliateProgramConstantInfo.setAffiliateId(loginInfo.getExternalAffiliateLoginId());
        affiliateProgramConstantInfo.setAffiliateProgramId(loginInfo.getAffiliateProgram().getId());
        model.addAttribute(affiliateProgramConstantInfo);
        model.addAttribute("affiliateProgramList", affiliatePrograms);

        return "/user/add-or-edit-affiliate-program";
    }

    @Transactional
    @RequestMapping(value = "/{currentuser}/{affiliateProgram}/{login}/delete/{loginId}", method = RequestMethod.GET)
    public String deleteApAndLogin(@PathVariable String login, @PathVariable String affiliateProgram, @PathVariable Long loginId) {
        AppUser user = usersService.findByName(usersService.getPrincipal());
        LoginInfo loginInfo = loginInfoRepository.findByIdAndAppUser(loginId, user);
        try {
            loginInfoRepository.delete(loginInfo);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Такой партнерки не существует");
        }

        return "redirect:/users/" + usersService.getPrincipal();
    }

    @Transactional
    @RequestMapping(value = {"/{currentuser}/add-new-affiliate-program-into-your-list",
            "/{login}/{affiliateProgram}/{login}/edit"}, method = RequestMethod.POST)
    public String addOrderFromForm(@Valid AffiliateProgramConstantInfo affiliateProgramConstantInfo,
                                   BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            List<AffiliateProgram> affiliatePrograms = affiliateProgramService.findAll();

            model.addAttribute("affiliateProgramList", affiliatePrograms);
            model.addAttribute(new AffiliateProgramConstantInfo());
            return "/user/add-or-edit-affiliate-program";
        }

        AppUser user = usersService.findByName(usersService.getPrincipal());
        LoginInfo loginInfo = loginInfoService.getReamLoginFromDb(affiliateProgramConstantInfo.getLogin(),
                affiliateProgramConstantInfo.getAffiliateProgramName());

        if (loginInfo == null) {
            loginInfo = new LoginInfo();
            loginInfo.setAppUser(user);
            loginInfo.setLastDate(Date.valueOf(affiliateProgramConstantInfo.getStartDate()));
            loginInfo.setLogin(affiliateProgramConstantInfo.getLogin());
            loginInfo.setExternalAffiliateLoginId(affiliateProgramConstantInfo.getAffiliateId());
            AffiliateProgram affiliateProgram = affiliateProgramRepository.findOne(
                    affiliateProgramConstantInfo.getId());
            loginInfo.setAffiliateProgram(affiliateProgram);
        }

        loginInfo.setPassword(new AES().encrypt(affiliateProgramConstantInfo.getPassword(), user.getPassword()));
        loginInfo.setStartDate(Date.valueOf(affiliateProgramConstantInfo.getStartDate()));

        // TODO обработать первичный ключ
        loginInfoService.saveOrUpdate(loginInfo);

        return "redirect:/users/" + usersService.getPrincipal();
    }

    @RequestMapping(value = "/{currentuser}/{affiliateProgram}/{login}/{logId}/download")
    // @RequestMapping(value = "/{currentuser}/download")
    public String downloadStatsForOneAffiliateProgram(@PathVariable String logId) {

        downloadService.downloadStatsOneAp(Long.parseLong(logId));
        // downloadService.downloadStatsOneAp(apId, loginId);
        return "redirect:/users/" + usersService.getPrincipal();
    }

    @RequestMapping(value = "/{currentuser}/download-all-stats")
    public String downloadAllStats() {
        downloadService.downloadStatsForAllProgram();
        return "redirect:/users/" + usersService.getPrincipal();
    }

    @RequestMapping(value = "/{currentuser}/add-ap-from-file", method = RequestMethod.POST)
    public String addAffiliateProgramsFromFile(@RequestParam("file") MultipartFile file) {
//        File file = new File("/home/konstantin/IdeaProjects/ew2013/mainAffiliatePrograms.txt");
        try {
            affiliateProgramService.addAffiliateProgramsFromExternalFile(file.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/users/" + usersService.getPrincipal();
    }

    @RequestMapping(value = "/{currentuser}/show-all-stats", method = RequestMethod.GET)
    public String getStatsPage() {
        return "redirect:/users/" + usersService.getPrincipal();
    }

    @RequestMapping(value = "/{currentuser}/show-all-stats", method = RequestMethod.POST)
    // @Async
    public ModelAndView getAllStatsByUser(@RequestParam("startDate") String startDate,
                                          @RequestParam("endDate") String endDate) throws ParseException {
        java.util.Date startPeriodData = new java.util.Date();
        java.util.Date finishPeriodDate = null;
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        ModelMap model = new ModelMap();
        AppUser user = usersService.findByName(usersService.getPrincipal());
        String apJson = null;

        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            apJson = gson.toJson(generalStatRepository.getStatsByDate(format.parse(startDate), format.parse(endDate), user.getId())).replaceAll("null", "0").replaceAll("\\n", "");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        finishPeriodDate = new java.util.Date();
        System.out.println((finishPeriodDate.getTime() - startPeriodData.getTime()) / 1000F);

        model.addAttribute("apJsonFromJava", apJson);
        model.addAttribute("myUser", user);
        return new ModelAndView("/user/all-stats", model);
    }

    @RequestMapping(value = "/{currentuser}/uploadLoginFile", method = RequestMethod.POST)
    public String submit(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        try {
            loginInfoService.addLoginFromFile(file.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/users/" + usersService.getPrincipal();
    }
}