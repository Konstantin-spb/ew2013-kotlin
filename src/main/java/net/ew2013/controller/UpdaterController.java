package net.ew2013.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import net.ew2013.model.Tag;
import net.ew2013.repository.AffiliateProgramRepository;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.ew2013.galleries.NastyDollarsGalleries;
import net.ew2013.model.AffiliateProgram;
import net.ew2013.model.Gallery;
import net.ew2013.model.LinkToContent;
import net.ew2013.model.PornModel;
import net.ew2013.repository.GalleryRepository;
import net.ew2013.repository.PornModelRepository;
import net.ew2013.service.AffiliateProgramService;

@Controller
@RequestMapping(value = "/updater")
public class UpdaterController {
	@Autowired
	NastyDollarsGalleries nastyDollarsGalleries;
	@Autowired
	GalleryRepository galleryRepository;
	@Autowired
	PornModelRepository pornModelRepository;
	@Autowired
	AffiliateProgramService affiliateProgramService;
	@Autowired
    AffiliateProgramRepository affiliateProgramRepository;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getGalleriesDump(Model model) {
	    Date startDate = new Date();
	    Date dateFromDb;
	    Date sbCreateDate;
	    StringBuilder stringBuilder = new StringBuilder();
		List<Gallery> galleryList = galleryRepository.findAll();
//		galleryList = galleryRepository.findAllByAffiliateProgramOrderByLinkToContents(affiliateProgramRepository.findOne(146L));
		dateFromDb = new Date();
        System.out.println("Select from DB: " + ((dateFromDb.getTime() - startDate.getTime()) / 1000F) + " sec");
		model.addAttribute("galleriesList", galleryList);

		return "/user/galleriesDumper";
	}

	@RequestMapping(value = "/getGalleries")
	public String getGalleries() throws IOException {

		Date startPeriodData = new Date();
		Date finishPeriodDate;
		for (Element e : nastyDollarsGalleries.getGalleryLinks()) {
			int threadNumber = 0;
			// for (Element link: links) {
			nastyDollarsGalleries.getPagesLinksBySiteAndNiches(e, threadNumber);
			threadNumber++;
		}

//		System.out.println();
		for (String[] st : nastyDollarsGalleries.getPageWithGalleriesLinks()) {
			nastyDollarsGalleries.getGalleryLinksBySiteAndNiches(st[0], st[1]);
		}
		finishPeriodDate = new Date();
		System.out.println(((finishPeriodDate.getTime() - startPeriodData.getTime()) / 1000F) + " sec");
		// nastyDollarsGalleries.getGalleryLinksBySiteAndNiches(nastyDollarsGalleries.getGalleryLinks());

		return "redirect:/users/";
	}

	@RequestMapping(value = "/updateGalleries")
	public String updateGalleries() {

		AffiliateProgram affiliateProgram = affiliateProgramService.findByName("NastyDollars");
		Set<Gallery> galleryList = galleryRepository.findByAffiliateProgram(affiliateProgram);

		for (Gallery g : galleryList) {
			try {
				nastyDollarsGalleries.updateGalleries(g);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return "redirect:/users/";
	}

	@RequestMapping(value = "updateSomeGalleries")
	public String updateSomeGalleries() {
		AffiliateProgram affiliateProgram = affiliateProgramService.findByName("NastyDollars");
		Set<Gallery> galleryList =  galleryRepository.fingGalleryWithoutPicsOrVideo();

		for (Gallery g : galleryList) {
			try {
				nastyDollarsGalleries.updateGalleries(g);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return"redirect:/users/";
	}

	@RequestMapping(value = "/updateModels")
	public String updateModels() throws IOException {

		List<PornModel> pornModels = pornModelRepository.findAll();
		for (PornModel pm : pornModels) {
			nastyDollarsGalleries.updateModel(pm);
		}
		return "redirect:/users/";
	}
}
